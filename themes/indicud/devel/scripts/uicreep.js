var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
document.documentElement.className += isTouchDevice ? ' touch' : ' no-touch';

(function ($, undefined) {
    initStuff();
    // initSwiper();
    initHeader();
    // initHero();
    // initAccordion();
    initSideNav();
    initSlideToggle();
    initCourseListing();
    initMobNav();
    // initFooTable();
    initScrollTo();

    $(window).on('load', function () {
        setTimeout(function () {
            initWhoTalking();
            initSideBar();
        }, 300);
    });

    function initScrollTo() {
        $('.scroll-to').each(function () {
            var $this = $(this);

            $this.on('click', function (e) {
                e.preventDefault();

                var $target = $this.data('scrollTarget');

                if ($target == null) {
                    $target = $($this.attr('data-target'));
                    $this.data('scrollTarget', $target);
                }

                if ($target.length) {
                    $('html, body').animate({
                        scrollTop: $target.offset().top
                    }, 300);
                }
            });
        });
    }

    /* function initFooTable() {
     $('.js-footable').footable({});
     } */

    function initMobNav() {
        var $body = $('body'),
            $header = $('.main-header:not(.we-megamenu)'),
            $nav = $('#nav'),
            $toggle = $('.main-header:not(.we-megamenu) .navbar-toggle'),
            $pull_right = $('.pull-right'),
            $subNavOpener;

        $toggle.on('click', function () {
            $nav_collapse = $('.nav-collapse'),
            $nav_collapse.removeClass("back-opened");
            $pull_right.removeClass("back-opened");

            if (!$header.hasClass('nav-opened')) {
                $body.css('overflow', 'hidden');
                $header.addClass('nav-opened');
            }
            else {
                $body.css('overflow', '');
                $header.removeClass('nav-opened');
            }
        });

        rspHelper.addMQ({
            '(max-width: 767px)': {
                on: function () {
                    var $subNav = $nav.find('.sub-nav-wrap');

                    if (!$subNavOpener) {
                        $subNavOpener = $subNav.prev();
                    }

                    $subNavOpener.each(function () {
                        var $this = $(this),
                            $parent = $this.parent(),
                            $back = $parent.find('.back .btn');

                        $this.on('click.mobNav', function (e) {
                            e.preventDefault();
                            $parent.addClass('sub-nav-opened');
                        });

                        $back.on('click.mobNav', function (e) {
                            e.preventDefault();
                            $parent.removeClass('sub-nav-opened');
                        });
                    });
                },
                off: function () {
                    $subNavOpener.off('.mobNav');
                    $subNavOpener.parent().find('.back .btn').off('.mobNav');
                }
            }
        });
    }

    function initCourseListing() {
        var $el = $('.preview-list'),
            $items = $el.find('.preview');

        if ($items.length) {
            rspHelper.addMQ({
                '(min-width: 769px)': {
                    on: function () {
                        $el.sameHeight({
                            elements: '.preview',
                            flexible: true,
                            multiLine: true
                        });
                    },
                    off: function () {

                    }
                }
            });
        }
    }

    function initSlideToggle() {
        $('[data-slide-toggle]').on('click', function () {
            var $this = $(this),
                $elems = $this.data('slideElems');

            if (!$elems) {
                $elems = $($this.attr('data-slide-toggle'));
                $this.data('slideElems', $elems);
            }

            $elems.stop().slideToggle();
            $this.toggleClass('active');
        });
    }

    function initSideBar() {
        var $sidebar = $('.sidebar'),
            pad = $('body').css('direction') === 'rtl' ? 'paddingLeft' : 'paddingRight';

        if ($sidebar.length) {

            var $win = $(window),
                $container = $('.main > .container'),
                sidebarInitialTop, sidebarHeight,
                containerBottom, maxSidebarTop;

            rspHelper.addMQ({
                '(max-width: 767px)': {
                    on: function () {
                        $sidebar.openClose();
                    },
                    off: function () {
                        $sidebar.data('OpenClose').destroy();
                    }
                },
                '(min-width: 768px)': {
                    on: function () {
                        // setSidebarDim();
                        // measure();
                        // check();
                        /* $win.on('scroll.sidebar', check);
                        $win.on('resize.sidebar orientationchange.sidebar', function () {
                            setSidebarDim();
                            measure();
                            check();
                        }); */
                    },
                    off: function () {
                        $win.off('.sidebar');
                        $sidebar.css({
                            position: '',
                            top: '',
                            width: '',
                            paddingLeft: '',
                            paddingRight: ''
                        });
                    }
                }
            });

            function setSidebarDim() {
                var w = $container.innerWidth(),
                    props = {
                        width: w * 0.289
                    };

                props[pad] = w * 0.051;

                $sidebar.css(props);
            }

            function measure() {
                sidebarInitialTop = $container.offset().top;
                sidebarHeight = $sidebar.innerHeight();
                containerBottom = $container.offset().top + $container.innerHeight();
                maxSidebarTop = containerBottom - sidebarHeight - sidebarInitialTop;
            }

            function check() {
                var winTop = $win.scrollTop(),
                    sidebarPosition,
                    sidebarState,
                    sidebarTop;

                if (winTop > sidebarInitialTop) {
                    sidebarTop = 0;
                    sidebarPosition = 'fixed';
                    sidebarState = 'sticky';
                }
                else {
                    sidebarPosition = '';
                    sidebarTop = '';
                    sidebarState = 'static';
                }

                if (typeof sidebarTop === 'number' && (winTop - sidebarInitialTop) > maxSidebarTop) {
                    sidebarPosition = '';
                    sidebarTop = maxSidebarTop;
                    sidebarState = 'stuck';
                }

                $sidebar.css({
                    top: sidebarTop,
                    position: sidebarPosition
                });

                $sidebar.attr('data-state', sidebarState);
            }
        }
    }

    function initSideNav() {
        var $el = $('.side-nav')

        /* use class menu link attributes classes that are added on the span or a tag */
        var items = $(".side-nav .menu-item--expanded");

        items.each(function () {
            var item = $(this);

            var aclass = item.find("a");
            if (aclass.hasClass("opened")) {
                item.addClass(aclass.attr("class"));
            }

            var sclass = item.find("span");
            if (sclass.hasClass("opened")) {
                item.addClass(sclass.attr("class"));
            }

        });


        /* this works in conjunction with jquery.accordion.js */
        /* custom classes added for opened.first */
        $('.sidebar .clickable-opener').on("click", function () {
            if ($(this).hasClass("opened")) {
                $(this).removeClass("opened");
            } else {
                $('.sidebar .clickable-opener').removeClass("opened");
                $(this).addClass("opened");
            }
        });

        if ($el.length) {
            $el.slideAccordion();
        }
    }

    /* function initAccordion() {
        $('.accordion').slideAccordion();
    } */

    /* function initHero() {
     var $el = $('.hero');

     if ($el.length) {
     rspHelper.addMQ({
     '(min-width: 768px)': {
     on: function () {
     $el.css('background-image', 'url(' + $el.attr('data-bg') + ')');
     }
     },
     '(max-width: 767px)': {
     on: function () {
     $el.css('background-image', 'url(' + $el.attr('data-sm-bg') + ')');
     }
     }
     });
     }
     } */

    /* function initSwiper() {
     new Swiper(".swiper-container", {
     direction: "horizontal",
     loop: !0,
     pagination: ".swiper-pagination",
     nextButton: ".swiper-button-next",
     prevButton: ".swiper-button-prev",
     scrollbarHide: !0,
     paginationClickable: true
     });
     } */

    function initHeader() {

        if ($('.main-header:not(.ignore)').length > 0) {
                var $body = $('body');
                // console.log(document.querySelector('.main-header'));
                var hr = new Headroom(document.querySelector('.main-header'), {
                    offset: 250,
                    onPin: function () {
                        $body.addClass('sticky-header');
                    },
                    onUnpin: function () {
                        $body.removeClass('sticky-header');
                    }
                });
                hr.init();
            }
    }

    function initWhoTalking() {
        var $el = $('.home-whoistalking__inner');

        if ($el.length) {
            rspHelper.addMQ({
                '(min-width: 768px)': {
                    on: function () {
                        $el.masonry({
                            itemSelector: '.social-box',
                            percentPosition: true
                        });
                    },
                    off: function () {
                        $el.masonry('destroy');
                    }
                }
            });
        }
    }

    function initStuff() {

        BrowserDetection();

        var $el = $('article iframe');

        if ($el.length) {
            rspHelper.addMQ({
                '(max-width: 767px)': {
                    on: function () {
                        $el.attr({"width": "100%", "height": "250px"});

                        if ($("body").hasClass("page-node-type-files")) {
                            $(".page-node-type-files article iframe").attr({"width": "100%", "height": "450px"});
                        }
                                            },
                    off: function () {
                        $el.attr({"width": "770px", "height": "433px"});

                        if ($("body").hasClass("page-node-type-files")) {

                            $(".page-node-type-files article iframe").attr({"width": "100%", "height": "750px"});

                        }
                    }
                }
            });
        }
    }

    function BrowserDetection() {

        // console.log(navigator);

        //Check if browser is IE
        if (navigator.userAgent.search("MSIE") >= 0) {
            $("body").addClass("MSIE");
        }
        //Check if browser is MSIE Edge
        if (navigator.userAgent.search("Edge") >= 0) {
            $("body").addClass("MSIE-Edge");
        }
        //Check if browser is Chrome
        else if (navigator.userAgent.search("Chrome") >= 0) {
            $("body").addClass("Chrome");
        }
        //Check if browser is Firefox
        else if (navigator.userAgent.search("Firefox") >= 0) {
            $("body").addClass("Firefox");
        }
        //Check if browser is Safari
        else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") <= 0) {
            $("body").addClass("Safari");
        }
        //Check if browser is Opera
        else if (navigator.userAgent.search("Opera") >= 0) {
            $("body").addClass("Opera");
        }
        //Check if browser is Opera
        else {
            $("body").addClass("UnknownBrowser");
        }
    }


})(jQuery);
