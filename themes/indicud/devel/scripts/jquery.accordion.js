/*
 * jQuery Accordion plugin
 */
;
(function ($) {
    $.fn.slideAccordion = function (opt) {
        // default options
        var options = $.extend({
            addClassBeforeAnimation: false,
            allowClickWhenExpanded: false,
            activeClass: 'active',
            opener: '[data-opener]',
            slider: '[data-slide]',
            animSpeed: 300,
            collapsible: true,
            event: 'click'
        }, opt);

        return this.each(function () {
            // options
            var accordion = $(this);
            var items = accordion.find(':has(' + options.slider + ')');

            items.each(function () {

                var item = $(this);
                var opener = item.find(options.opener);
                var slider = item.find(options.slider);


                /* ++g++ ignore special classes */
                if (item.find('a').hasClass('default')) {
                    slider.hide();
                } else {

                     opener.on(options.event, function (e) {
                        if (!slider.is(':animated')) {
                            if (item.hasClass(options.activeClass)) {
                                if (options.allowClickWhenExpanded) {
                                    return;
                                } else if (options.collapsible) {
                                    slider.slideUp(options.animSpeed, function () {
                                        item.removeClass(options.activeClass);
                                    });
                                }
                            } else {
                                // show active
                                var levelItems = item.siblings('.' + options.activeClass);
                                var sliderElements = levelItems.find(options.slider);
                                item.addClass(options.activeClass);
                                slider.slideDown(options.animSpeed);

                                // collapse others
                                sliderElements.slideUp(options.animSpeed, function () {
                                    levelItems.removeClass(options.activeClass);
                                });
                            }
                        }
                        e.preventDefault();
                    });

                    /* ++g++ */
                    /* this works in conjunction with uicreep.js/initSideNav */
                    /* custom classes added for opened.first */
                    if (item.hasClass("opened") && item.hasClass("first"))
                    {
                        item.addClass("active");
                        item.find(".clickable-opener").addClass('opened');
                    }

                    if (item.hasClass(options.activeClass))
                        slider.show();
                    else
                        slider.hide();

                }


            });
        });
    };
}(jQuery));