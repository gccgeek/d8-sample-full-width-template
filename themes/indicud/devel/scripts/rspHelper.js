/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas, David Knight. Dual MIT/BSD license */

window.matchMedia || (window.matchMedia = function() {
	'use strict';

	// For browsers that support matchMedium api such as IE 9 and webkit
	var styleMedia = (window.styleMedia || window.media);

	return function(media) {
		return {
			matches: styleMedia.matchMedium(media || 'all'),
			media: media || 'all'
		};
	};
}());

var rspHelper = (function () {
	var isListening = false,
		timeoutID = null,
		queries = {};

	function handleChange(e) {
		clearTimeout(timeoutID);

		timeoutID = setTimeout(function () {
			var execHandlers = {
				on: [],
				off: []
			};

			for(var queryStr in queries) {
				if(queries[queryStr] && queries.hasOwnProperty(queryStr)) {
					var query = queries[queryStr],
						matches = window.matchMedia(queryStr).matches,
						handlers = query.handlers || [],
						handler, action;

					if(matches !== query.matches) {
						query.matches = matches;
						action = matches ? 'on' : 'off';

						handlers.forEach(function (handler) {
							execHandlers[action].push(handler[action]);
						});
					}
				}
			}

			execHandlers.off.forEach(function (fn) {
				fn();
			});

			execHandlers.on.forEach(function (fn) {
				fn();
			});
		}, 100);
	}

	function addMQ(queryObj) {
		if(!isListening) {
			isListening = true;
			window.addEventListener('resize', handleChange, true);
		}

		for(var queryStr in queryObj) {
			if(queryObj[queryStr] && queryObj.hasOwnProperty(queryStr)) {
				var query = queries[queryStr] || (queries[queryStr] = {handlers: [], matches: false}),
					handlers = query.handlers,
					index;

				index = handlers.push({
					on: queryObj[queryStr].on || function () {},
					off: queryObj[queryStr].off || function () {}
				});

				if(window.matchMedia(queryStr).matches) {
					query.matches = true;
					handlers[index - 1].on();
				}
			}
		}
	}

	return {
		addMQ: addMQ
	};
}());