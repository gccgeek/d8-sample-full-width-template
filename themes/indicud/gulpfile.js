var gulp = require('gulp'),
	sass = require('gulp-ruby-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	plumber = require('gulp-plumber'),
	browserSync = require('browser-sync').create(),
	minifyCss = require("gulp-minify-css"),
	rename = require("gulp-rename");

var cfg = {
	stylesPath: 'devel/styles',
	stylesPathCustom: 'devel/styles-custom',
	stylesPathRtl: 'devel/styles-rtl',
	stylesPathRtlCustom: 'devel/styles-custom-rtl',
	scriptsPath: 'devel/scripts'
};

gulp.task('compile-styles', function () {
	return sass(cfg.stylesPath + '/uicreep.scss', {
			style: 'compressed', // expanded, compressed
			loadPath: [
				cfg.stylesPath
			],
        sourcemap: false
		})
		.on('error', sass.logError)
		.pipe(sourcemaps.write('maps', {
        			includeContent: false,
        			sourceRoot: cfg.stylesPath
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(minifyCss())
		.pipe(gulp.dest('css'))
		.pipe(browserSync.stream({
			match: '**/*.css'
		}));
});

gulp.task('cud-compile-styles', function () {
	return sass(cfg.stylesPathCustom + '/main.scss', {
		style: 'compressed', // expanded, compressed
		loadPath: [
			cfg.stylesPathCustom
		],
		sourcemap: false
	})
		.on('error', sass.logError)
		.pipe(sourcemaps.write('maps', {
        			includeContent: false,
        			sourceRoot: cfg.stylesPathCustom
				 }))
		.pipe(rename({
			suffix: '.min'
		}))
	    .pipe(minifyCss())
		.pipe(gulp.dest('css'))
		.pipe(browserSync.stream({
			match: '**/*.css'
		}));
});

gulp.task('compile-styles-rtl', function () {
	return sass(cfg.stylesPathRtl + '/uicreep-rtl.scss', {
			style: 'compressed', // expanded, compressed
			loadPath: [
				cfg.stylesPathRtl
			],
			sourcemap: false
		})
		.on('error', sass.logError)
		.pipe(sourcemaps.write('maps', {
		 	includeContent: false,
		 	sourceRoot: cfg.stylesPathRtl
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(minifyCss())
		.pipe(gulp.dest('css'))
		.pipe(browserSync.stream({
			match: '**/*.css'
		}));
});

gulp.task('cud-compile-styles-rtl', function () {
	return sass(cfg.stylesPathRtlCustom + '/main-rtl.scss', {
			style: 'compressed', // expanded, compressed
			loadPath: [
				cfg.stylesPathRtlCustom
			],
			sourcemap: false
		})
		.on('error', sass.logError)
		.pipe(sourcemaps.write('maps', {
		 	includeContent: false,
		 	sourceRoot: cfg.stylesPathRtlCustom
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(minifyCss())
		.pipe(gulp.dest('css'))
		.pipe(browserSync.stream({
			match: '**/*.css'
		}));
});


gulp.task('bs-reload', function () {
	browserSync.reload();
});

gulp.task('bs-init', function () {
	browserSync.init({
		server: '.',
		directory: true
	});
});

gulp.task('uglify-scripts', function () {
	return gulp.src(['js/uicreep.js'])
		.pipe(plumber({
			errorHandler: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))
		.pipe(uglify())
		.pipe(gulp.dest('js'));
});

gulp.task('concat-scripts', function() {
	return gulp.src([cfg.scriptsPath + '/libs/*.js', cfg.scriptsPath + '/!(uicreep).js', cfg.scriptsPath + '/uicreep.js'])
		.pipe(concat('uicreep.js'))
		.pipe(gulp.dest('js'));
});

gulp.task('watch', function () {
	gulp.watch(cfg.stylesPath + '/**/*.scss', ['compile-styles']);
	gulp.watch(cfg.stylesPathCustom + '/**/*.scss', ['cud-compile-styles']);
	gulp.watch(cfg.stylesPathRtl + '/**/*.scss', ['compile-styles-rtl']);
	gulp.watch(cfg.stylesPathRtlCustom + '/**/*.scss', ['cud-compile-styles-rtl']);
	gulp.watch(cfg.scriptsPath + '/**/*.js', ['concat-scripts', 'bs-reload']);
	gulp.watch('templates/*.html', ['bs-reload']);
});

gulp.task('default', ['bs-init', 'watch']);