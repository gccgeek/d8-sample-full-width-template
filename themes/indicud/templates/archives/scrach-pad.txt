#block-views-block-promotion-secondary-promotion-scroll {
  height: 100%;
  width: 100%;
  margin: 0 auto;
  position: relative;
  overflow: hidden;
  z-index: 1;

  .skin-default {
    display: block;
    font-size: 0;
    white-space: nowrap;
    width: 100% !important;
    height: 450px;

    .views-slideshow-controls-top {
      .views-slideshow-controls-text-previous-processed,
      .views-slideshow-controls-text-next-processed {
        position: absolute;
        top: 50%;
        width: 27px;
        height: 44px;
        margin-top: -22px;
        z-index: 10;
        cursor: pointer;
        -moz-background-size: 27px 44px;
        -webkit-background-size: 27px 44px;
        background-size: 27px 44px;
        background-position: center;
        background-repeat: no-repeat;
        display: inline-block;
        left: 10px;
        right: auto;
      }

      .views-slideshow-controls-text-previous-processed {
        left: 10px;
        right: auto;
      }

      .views-slideshow-controls-text-next-processed {
        right: 10px;
        left: auto;
      }

      .views-slideshow-controls-text-previous-processed {
        background: url(../media/icons/arrow-left.png) no-repeat;
        height: 60px;
        width: 70px;

      }

      .views-slideshow-controls-text-next-processed {
        background: url(../media/icons/arrow-right.png) 100% 0 no-repeat;
        height: 60px;
        width: 70px;
        cursor: pointer;

      }
    }

    .views_slideshow_slide {
      width: 100% !important;
      text-align: center;
      font-size: 18px;
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;

      .views-row {
        white-space: normal;
        display: inline-block;
        vertical-align: top;
        width: 100% !important;

        .col-md-12 {
          position: inherit;
        }

        .cover-wrap {
          position: absolute;
          height: 450px;
        }
        .cover-wrap:before {
          content: '';
          width: 100% !important;
          height: 100vh;
          background: #000;
          opacity: 0.5;
          position: absolute;
          left: 0;
          z-index: 1;
        }

        .container {
          position: relative;
          top: 50%;
          text-align: left;
          width: 100% !important;
          padding-top: 45px;
          padding-bottom: 45px;
          color: #fff;
          z-index: 2;

          .title,
          h2 {
            text-transform: uppercase;
            font-size: 1.8em;
            padding-bottom: 35px;
            position: relative;
            font-family: KozGoPro-Bold-AlphaNum, arial, helvetica, sans-serif;
          }

          .title:before,
          h2:before {
            content: '';
            width: 100px;
            height: 2px;
            background: #d8212c;
            display: block;
            margin: 20px 0;
            position: absolute;
            left: 0;
            bottom: 0;
          }

          .title:after,
          h2:after {
            display: inline-block;
            font: normal normal normal 14px/1 FontAwesome;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            content: "";
            position: absolute;
            left: 120px;
            bottom: 14px;
            color: #d8212c;
          }

          .edit_node {
            background: #fff;
            width: 26px !important;
            height: 26px !important;
            display: inline-block;
            border-radius: 25px;
            position: absolute;
            top: 0;
            right: 0;

            a {
              background-image: url(/core/themes/stable/images/core/icons/bebebe/pencil.svg);
              background-position: center center;
              background-repeat: no-repeat;
              background-size: 16px 16px;
              height: 26px !important;
              width: 26px !important;
              text-indent: -9999px;
              display: inline-block;
              color: #ccc;
              border-radius: 25px;

            }
          }
          ul {
            list-style: none;
            margin: 0;
            padding: 0;
            li {
              float: left;
              margin-bottom: 15px;
              padding-left: 0 !important;
              a {
                display: block;
                padding-top: 10px;
                padding-bottom: 10px;
              }
              a:before {
                top: 34%;
              }
            }
          }
        }

        /* secondary scroll */
        .views_slideshow_main {
          .views_slideshow_slide .views-row {
            /* 4 divisions */
            width: 25% !important;

            .col-md-4 {
              width: 100%;
              padding:0;

              .image {

                padding:5px;

                img {
                  width: 100%;
                  padding: 5px;
                }
              }

              .details {

                padding:5px;

                h5 {
                  font: 12px/15px "KozGoPro-Light-AlphaNum", arial, helvetica, sans-serif;
                }
              }
            }
          }
        }
      }
    }
  }
}

.swiper-pagination-bullet {
  border-top-right-radius: 0;
  border-top-left-radius: 0;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
  width: 13px;
  height: 13px;
}

.video-showcase {
  position: relative;
  padding: 0;
  max-height: 275px;
  overflow: hidden;
  z-index: 0;

  .url {
    display: none;
  }

  .video-launcher {
    display: block;
    cursor: pointer;
    min-height:343px;
    width:222px;
    height:343px;
    background:url(../media/default/front-testimonial-background.png) top center no-repeat;

    .image {
      display: block;
      width: 225px;
      margin: 0 auto;
    }

    .details {
      width: 225px;
      margin: 0 auto;
    }
  }

  #target-vcontainer {
    text-align: center;
    width: 100%;
    display: inline-block;
    padding: 0;
    margin: 0;
    min-height: 350px;
    -webkit-transition: top 0.1s;
    -moz-transition:  top 0.1s;
    -ms-transition:  top 0.1s;
    -o-transition:  top 0.1s;
    transition:  top 0.1s;
    background: rgba(0,0,0,0.9);

    i {
      position: absolute;
      top: 0;
      right: 0;
      padding: 15px;
      font-size: 30px;
      z-index:11;
      color:$red_1;
      opacity:0.5;
    }

    i:hover {
      opacity:1;
    }

    iframe {
      margin: 0 auto;
      display: block;
      padding: 0;
      width: 450px;
    }
  }

  #target-vcontainer.show {
    top:0;
    position:absolute;
    z-index:10;
  }

}

.video-showcase:hover:before {
  top: 0;
  max-height: 100%;
}

.video-showcase:before {
  top: 0;
  content: ' ';
  width: 100%;
  height: 0;
  background: red;
  position: absolute;
  z-index: 5;
  -webkit-transition: height 0.3s ease-in-out;
  -moz-transition: height 0.3s ease-in-out;
  -ms-transition: height 0.3s ease-in-out;
  -o-transition: height 0.3s ease-in-out;
  transition: height 0.3s ease-in-out;

}

@import "cud-carousel-media";




        /*Drupal.behaviors.jQueryCycleAdjustSliderHeights = {
         attach: function (context, settings) {

         var self = Drupal.behaviors.jQueryCycleAdjustSliderHeights;

         $.each($(".views_slideshow_cycle_main", context), function (idx, value) {

         var cycle = $('.views_slideshow_cycle_teaser_section', context);

         $('.views_slideshow_slide', context).each(function (i) {
         $(this, context).css({width: cycle.width(), height: cycle.height()});
         });
         });
         }
         }*/


        $('.slick-slider').on('click', '.slick-slide', function () {
            alert("test");
        });


/* php

$service = \Drupal::service('entity.query');

        $entity_manager = \Drupal::service('entity.manager')->getStorage('node');

        $query = $service->get('node');

        $query->condition('type', 'program')
            ->condition('field_department.entity.nid', $schoolId, '=')
            ->condition('status', 1);

        $current_node = \Drupal::routeMatch()->getParameter('node');
        if ($current_node) {
            $query->condition('nid', $current_node->nid->value, '<>');
        }

        $result = $query->execute(); */