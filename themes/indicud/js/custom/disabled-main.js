  function stopVideo() {
    var audio = jQuery('.player-audio');
    if (audio.length > 0) {
      audio.find('i.fa').removeClass('fa-volume-up').addClass('fa-volume-off');
    }

    if (player) 
    {
      player.mute();
      player.pauseVideo();
      player.stopVideo();  
    }
  }


  (function ($, Drupal, drupalSettings) {

    /* case insensitive contains */
    $.expr[":"].contains = $.expr.createPseudo(function (arg) {
      return function (elem) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
      };
    });

    $(document).bind('cbox_open', function () {
      $('body').css({
        overflow: 'hidden'
      });
    }).bind('cbox_closed', function () {
      $('body').css({
        overflow: ''
      });
    });


    $(document).ready(function () {
 
      $('.paroller').paroller();
 
      // /gem-education
      // similar functionality
      $(".tooltip .close").click(function () {
        $(".tooltip-program-content").hide();
      });

      // just check slider first of everything
      // in parallel of checking with 
      // on ready / still checking api
      var hero_slick = jQuery('.primary-hero .slick-slider').slick('getSlick');
      if (typeof hero_slick.slickPause === "function") {
        // no video for the current slide
        // don't pause slick slider
        if (player) {
          if (typeof player.getVideoData === "function") {
            if (player.getVideoData()["video_id"] != null) {
              hero_slick.slickPause();
            }
          }
        }
      }

      $('#carousel').on('click', function () {
        var scrollOffset = 0;
        if ($(".cud-carousel .carousel").length) {
          scrollOffset = parseInt($(".cud-carousel .carousel").offset().top);
        } else if ($(".main-content").length) {
          scrollOffset = parseInt($(".main-content").offset().top);
        }

        $('html, body').animate({
          scrollTop: scrollOffset
        }, 500);
      });


      /* webform invisible elements label animation */
      $('.webform-submission-form input, .webform-submission-form textarea, .webform-submission-form select').each(function () {
        if ($(this).val() != '') {
          $(this).parents('.js-form-type-textfield,.js-form-type-email,.js-form-type-tel,.js-form-type-textarea, .js-form-type-select').find('label').addClass('show animated slideInUp');
        }
      });

      $('.webform-submission-form input, .webform-submission-form textarea, .webform-submission-form select').on('keyup change', function () {
        $(this).parents('.js-form-type-textfield,.js-form-type-email,.js-form-type-tel,.js-form-type-textarea, .js-form-type-select').find('label').addClass('show animated slideInUp');
      });

      /* ensure that webform tel - international telephone javascript error will be respected before actual submission */
      $('form.webform-submission-form').submit(function () {
        if ($("input[type='tel']").hasClass('error')) {
          $("input[type='tel']").val('');
        }
      });

      $('.search-wrap button[type="submit"]').on('click', function (e) {
        e.preventDefault();

        if ($('.search-wrap .search-input').val() != '' && $('.search-wrap .search-input').val().length > 3) {
          $(this).bind('click');

          var $form = $(this).closest('form');

          $form.submit();
        }
      });

      $('.cud-our-chat-button').on('click', function (e) {
        e.preventDefault();

        var chat_div = $('#ChatDiv');
        if (chat_div.is(":visible")) {
          chat_div.hide();
        } else {
          chat_div.show();
        }

      });

      Drupal.behaviors.jQueryLiveChatButtonToggle = {
        attach: function (context, settings) {

          var self = Drupal.behaviors.jQueryLiveChatButtonToggle;

          $(".cud-our-chat-button", context).on('click', function (event) {
            event.preventDefault();

            var chat_div = $('#ChatDiv', context);

            if (chat_div.is(":visible")) {
              chat_div.hide();
            } else {
              chat_div.show();
            }

          });
        }
      };

      rspHelper.addMQ({

        '(max-width: 991px)': {
          on: function () {

            /* remove use-ajax for small screens */
            $("a.use-ajax").each(function(){
              $(this).off();
              $(this).removeClass("use-ajax").addClass("use-ajax-removal-processed");
            });
            
            var sh = $(".mobile.same-height");
            sh.sameHeight({
              elements: '.same-height-target',
              flexible: true,
              multiLine: true,
              biggestHeight: true
            });

            $('table.responsive').basictable({
              breakpoint: 991
            });
          },

          off: function () {
            /* if use-ajax class was removed purposely; add use-ajax for big screens */
            $("a.use-ajax-removal-processed").each(function(){
              $(this).on();
              $(this).addClass('use-ajax');
            });
          }
        },
        '(min-width: 550px) and (max-width: 767px)': {

          on: function () {

            popup(550, 309);
          },

          off: function () {
            $.colorbox.remove();
          }
        },
        '(min-width: 768px) and (max-width: 991px)': {

          on: function () {

            popup(750, 422);
          },

          off: function () {
            $.colorbox.remove();
          }
        },
        '(min-width: 992px)': {

          on: function () {

            /* same height */
            $(".course-info > .row").sameHeight({
              elements: 'li',
              flexible: true,
              multiLine: true,
              biggestHeight: true
            });

            var sh = $(".same-height");
            sh.sameHeight({
              elements: '.same-height-target',
              flexible: true,
              multiLine: true,
              biggestHeight: true
            });

            sh.sameHeight({
              elements: '.header',
              flexible: true,
              multiLine: true,
              biggestHeight: true
            });

            sh.sameHeight({
              elements: '.blazy .grid__content',
              flexible: true,
              multiLine: true,
              biggestHeight: true
            });

            sh.sameHeight({
              elements: '.content > .title',
              flexible: true,
              multiLine: true,
              biggestHeight: true
            });

            sh.sameHeight({
              elements: '.content > .body',
              flexible: true,
              multiLine: true,
              biggestHeight: true
            });

            // drupal's after ajax
            // loadings
            // checks
            Drupal.behaviors.afterAjax = {
              attach: function (context, settings) {

                var same_height = $(context).find('.same-height');
                same_height.sameHeight({
                  elements: '.same-height-target',
                  flexible: true,
                  multiLine: true,
                  biggestHeight: true
                });

                same_height.sameHeight({
                  elements: '.header',
                  flexible: true,
                  multiLine: true,
                  biggestHeight: true
                });

                same_height.sameHeight({
                  elements: '.blazy .grid__content',
                  flexible: true,
                  multiLine: true,
                  biggestHeight: true
                });

                same_height.sameHeight({
                  elements: '.content > .title',
                  flexible: true,
                  multiLine: true,
                  biggestHeight: true
                });

                same_height.sameHeight({
                  elements: '.content > .body',
                  flexible: true,
                  multiLine: true,
                  biggestHeight: true
                });
              }
            };

          },

          off: function () {}
        },
        '(min-width: 992px) and (max-width: 1279px)': {

          on: function () {

            popup(990, 557);
          },

          off: function () {
            $.colorbox.remove();
          }
        },
        '(min-width: 1280px)': {

          on: function () {

            popup(1200, 675);

          },

          off: function () {
            $.colorbox.remove();
          }
        },
        '(max-width: 549px)': {

          on: function () {

            popup("100%", 250);

          },

          off: function () {
            $.colorbox.remove();
          }
        },
        '(min-width: 768px)': {

          on: function () {
            /* search button */
            if (!$('html').hasClass("touch")) {
              $('.search-wrap').hover(function () {
                $('.tb-megamenu.tb-megamenu-main').css({
                  'visibility': 'hidden'
                });
              }, function () {
                $('.tb-megamenu.tb-megamenu-main').css({
                  'visibility': 'visible'
                });
              });
            }
          },

          off: function () {}
        },
        '(max-width: 767px)': {

          on: function () {

            // remove: use-ajax on mobiles
            $("a").each(function () {
              if ($(this).hasClass("use-ajax")) {
                $(this).removeClass("use-ajax");
              }
            });
          },

          off: function () {}
        }
      });


      init_if_touch_device();

      init_education_program();

      init_navigation();

      handle_social_misc();

      // handle_testimonial();

      handler_whatsapp();

      $('.lefsid').hoverIntent({
        over: function () {
          $(this).addClass('open');
        },
        timeout: 250,
        out: function () {

          $(this).removeClass("open");
        }
      });

      $(".simpletabs a").click(function (event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
      });


    });

    $(window).scroll(function () {
      var _this = $(this);
      rspHelper.addMQ({
        "(max-width: 991px)": {
          on: function () {
            if (_this.scrollTop() > 150)
              $(".fixed").addClass("fixed-container");
            else
              $(".fixed").removeClass("fixed-container");
          }
        }
      });
    });

    $(window).on('load', function () {

      rspHelper.addMQ({
        '(min-width: 992px)': {
          on: function () {
            setTimeout(function () {
              var ne_height = $(".news-events .news .icontainer").height();
              $(".news-events .events .icontainer").height(ne_height);
            }, 3000);
          },
          off: function () {

          }
        }
      });

      /* fcad - landing pages */
      $(".path-landing-page #webform-submission-fcad-form .webform-email-confirm, .path-landing-page #webform-submission-landing-psychology-form .webform-email-confirm, .path-landing-page #webform-submission-landing-engineering-form .webform-email-confirm, .path-landing-page #webform-submission-landing-business-form .webform-email-confirm, .webform-submission-form .webform-email-confirm, .ui-dialog .webform-email-confirm").attr("placeholder", "Confirm Email");

      setTimeout(function () {
        $('.path-landing-page #hero').css({
          "cursor": "pointer"
        });
      }, 500);
      $(".path-landing-page").on("click", ".hero", function (e) {
        if (player) {
          player.loadVideoById('RpJab5cEGKs', 0, 'medium');
          player.loadPlaylist('RpJab5cEGKs', 0, 0, 'medium');
          player.setLoop(true);
        }
        $(".path-landing-page .video-container").removeClass("hidden").css({
          "visibility": "visible"
        });
      });

      handler_animate_summary();

      handler_toggle_custom_node_edit_pencil();

      // handle_discover_programs_height();

      handler_reactive_slick_slider_hero();

      setTimeout(function () {
        handle_video_front_page_promotion();
      }, 1250);

      handler_social_box_photos();

      /* adjust main content min-height */
      if ($('.sidebar').outerHeight() > $('.main-content').outerHeight()) {
        $('.main-content').css({
          'min-height': $('.sidebar').outerHeight()
        });
      }


    });

    var resizeId;
    $(window).resize(function () {
      clearTimeout(resizeId);
      resizeId = setTimeout(doneResizing, 100);
    });

    function doneResizing() {
      rspHelper.addMQ({
        '(min-width: 992px)': {
          on: function () {
            var ne_height = $(".news-events .news .icontainer").height();
            $(".news-events .events .icontainer").height(ne_height);
          },
          off: function () {

          }
        }
      });

      // handle_discover_programs_height();
    }


    /* ---------- handlers ----------  */
    function popup(width, height) {
      width = !!width ? width : '100%';
      height = !!height ? height : 'auto';

      Drupal.behaviors.jQuerySlickFrontPage = {
        attach: function (context, settings) {

          var self = Drupal.behaviors.jQuerySlickFrontPage;

          $(".popup", context).on('click', function (event) {
            
            event.preventDefault();

            var loc = null;
        
            if ($(this).attr('href') != "") {
              loc = $(this).attr('href');
            }
    
            if ($(this).data('href') != "") {
              loc = $(this).data('href');
            }
    
            var is_loc = loc.includes("youtube");
            var is_id = loc.includes("?");

            if (is_id && is_loc) {
              /* works only if ? is included; ?{id} format */
              var id = loc.split("?")[1].split("&")[0].split("=")[1];

              event.preventDefault();

              // if on frontpage and the hero is 
              // playing a video; pause it temporarily
              if ($(".hero").hasClass('video')) {
                $(".hero.video video").trigger('pause');
                if (player) {
                  stopVideo();
                }
              }

              $.colorbox({
                href: "https://www.youtube.com/embed/" + id + "?enablejsapi=1&autoplay=1&rel=0&modestbranding=1&autohide=1&showinfo=0&controls=1",
                width: width,
                scrolling: false,
                iframe: true,
                height: height,
                onClosed: function () {
                  $(".hero.video video").trigger('play');
                }
              });
            } else {
              /* use normal colorbox */
              $.colorbox({
                href: loc,
                width: width,
                scrolling: false,
                iframe: true,
                height: height,
                onClosed: function () {
                  $(".hero.video video").trigger('play');
                }
              });
            }
          });

        }
      }

      $(".popup").click(function (event) {

        event.preventDefault();

        var loc = null;
        
        if ($(this).attr('href') != undefined || $(this).attr('href') != null) {
          loc = $(this).attr('href');
        }

        if ($(this).data('href') != undefined || $(this).data('href') != null) {
          loc = $(this).data('href');
        }
        
        var is_loc = loc.includes("youtube");
        var is_id = loc.includes("?");

        if (is_id && is_loc) {
          /* works only if ? is included; ?{id} format */
          var id = loc.split("?")[1].split("&")[0].split("=")[1];

          /* if on frontpage and the hero is 
           playing a video; pause it temporarily */
          if ($(".hero").hasClass('video')) {
            $(".hero.video video").trigger('pause');
            if (player) {
              stopVideo();
            }
          }

          $.colorbox({
            href: "https://www.youtube.com/embed/" + id + "?enablejsapi=1&autoplay=1&rel=0&modestbranding=1&autohide=1&showinfo=0&controls=1",
            width: width,
            scrolling: false,
            iframe: true,
            height: height,
            onClosed: function () {
              $(".hero.video video").trigger('play');
            }
          });
        } else {
          /* use normal colorbox */
          $.colorbox({
            href: loc,
            width: width,
            scrolling: false,
            iframe: true,
            height: height,
            onClosed: function () {
              $(".hero.video video").trigger('play');
            }
          });
        }

      });
    }

    function init_navigation() {
      /*
       navigation

       30/5/2016
       disabled tb-megamenu-touch.js default open and/or closing

       */

      $(document).delegate('.prepended-processed', 'click', function () {
        if ($(this).parents('.level-1').hasClass('open')) {
          $(".pull-right, .tb-megamenu-main .nav-collapse").removeClass("back-opened");
          $(this).parents('.level-1').removeClass('open');
        }
      });


      /* non-touch */
      rspHelper.addMQ({
        '(max-width: 767px)': {

          on: function () {

            if (!$('html').hasClass("touch")) {
              $(".tb-megamenu-item.level-1.mega.dropdown").each(function () {
                /* add back if not existing */
                if (!$(this).find('.tb-megamenu-submenu.dropdown-menu').find('.prepended').length > 0) {
                  $dropdown = $(this).find('.tb-megamenu-submenu.dropdown-menu');

                  /* prepend the parent link */
                  var parent_href = $(this).find("a.dropdown-toggle").attr("href");
                  var parent_title = $(this).find("a.dropdown-toggle").text();
                  $dropdown_inner = $dropdown.find('> .mega-dropdown-inner');
                  $dropdown_inner.prepend('<a class="prepended" href="' + parent_href + '"><h3>' + parent_title + '</h3></a>');

                  /* prepend the back link */
                  $dropdown.prepend("<div class='prepended'><div class='back visible-xs'><button class='prepended-processed btn btn-primary btn_ico-left btn_transparent'><i class='fa fa-long-arrow-left'>&nbsp;</i>back</button></div>");
                }
              });

              $(".tb-megamenu-item.level-1.mega.dropdown").hoverIntent({
                over: function () {
                  $(this).addClass("open");
                  $(this).find("a.dropdown-toggle").addClass("tb-megamenu-clicked");

                  $(".pull-right, .tb-megamenu-main .nav-collapse").addClass("back-opened");
                },
                timeout: 250,
                out: function () {
                  $(this).removeClass("open");
                  $(this).find("a.dropdown-toggle").removeClass("tb-megamenu-clicked");

                  $(".pull-right, .tb-megamenu-main .nav-collapse").removeClass("back-opened");
                }
              });
            }
          },

          off: function () {
            if (!$('html').hasClass("touch")) {
              $(".dropdown-menu .prepended").each(function () {
                $(this).remove();
              });
            }
          }
        }

      });

      /* touch */
      rspHelper.addMQ({
        '(max-width: 767px)': {

          on: function () {

            if ($('html').hasClass("touch")) {

              $(".tb-megamenu-item.level-1.mega.dropdown").each(function () {
                /* add back if not existing */

                var parent_href = $(this).find("a.dropdown-toggle").attr("href");
                var parent_title = $(this).find("a.dropdown-toggle").text();
                $(this).find("a.dropdown-toggle").attr("href", "#");

                if (!$(this).find('.tb-megamenu-submenu.dropdown-menu').find('.prepended').length > 0) {
                  $dropdown = $(this).find('.tb-megamenu-submenu.dropdown-menu');


                  /* prepend the parent link */
                  $dropdown_inner = $dropdown.find('> .mega-dropdown-inner');
                  $dropdown_inner.prepend('<a class="prepended" href="' + parent_href + '"><h3>' + parent_title + '</h3></a>');

                  /* prepend the back link */
                  $dropdown.prepend("<div class='prepended'><div class='back visible-xs'><button class='prepended-processed btn btn-primary btn_ico-left btn_transparent'><i class='fa fa-long-arrow-left'>&nbsp;</i>back</button></div>");
                }
              });


              $(".tb-megamenu-item.level-1.mega.dropdown").click(function (e) {

                if (!$(this).hasClass('open')) {
                  $(this).addClass("open");
                  $(this).find("a.dropdown-toggle").addClass("tb-megamenu-clicked");
                }


                $(".pull-right, .tb-megamenu-main .nav-collapse").addClass("back-opened");

                $("#main").addClass('touch-enabled-css');

              });

              /* only touch events will work */
              /* $(".tb-megamenu-item.level-1.mega.dropdown").bind('touchstart', function (e) {
               $(".tb-megamenu-item.level-1.mega.dropdown").removeClass("open");
               $(".tb-megamenu-item.level-1.mega.dropdown").find("a.dropdown-toggle").removeClass("tb-megamenu-clicked");
               $(this).find("a.dropdown-toggle").attr("href", "#");

               $(this).addClass("open");
               $(this).find("a.dropdown-toggle").addClass("tb-megamenu-clicked");

               $("#main").addClass('touch-enabled-css');
               });

               $(".tb-megamenu-item.level-1.mega.dropdown").bind('touchend', function () {
               $("#main").removeClass('touch-enabled-css');
               $(this).removeClass("open");
               $(this).find("a.dropdown-toggle").removeClass("tb-megamenu-clicked");
               }); */
            }

          },
          off: function () {

            if ($('html').hasClass("touch")) {

              $(".pull-right, .tb-megamenu-main .nav-collapse").removeClass("back-opened");

              $(".dropdown-menu .prepended").each(function () {
                $(this).remove();
              });
            }
          }
        }
      });

      rspHelper.addMQ({
        '(min-width: 768px)': {
          on: function () {

            if ($('html').hasClass("touch")) {
              /* only touch events will work */
              $(".tb-megamenu-item.level-1.mega.dropdown > a").bind('touchstart', function (e) {

                if ($(".portal-wrap").hasClass('open')) {
                  $(".portal-wrap").removeClass("open");
                }

                if ($(this).parent().hasClass('open')) {
                  $(this).parent().addClass("processed");

                  $(".tb-megamenu-item.level-1.mega.dropdown").each(function () {
                    if (!$(this).parent().hasClass('processed')) {
                      $(this).removeClass("open");
                    }
                  });
                } else {
                  $(".tb-megamenu-item.level-1.mega.dropdown").each(function () {
                    $(this).removeClass("open");
                  });
                }

                // opened - next click, run
                if ($(this).parent().hasClass('processed')) {

                  $(".lefsid").removeClass("open");

                } else {

                  // just open
                  e.preventDefault();

                  /* $(".tb-megamenu-item.level-1.mega.dropdown").each(function () {
                   $(this).removeClass("open");
                   $(this).find("a.dropdown-toggle").removeClass("tb-megamenu-clicked");
                   }); */

                  $(this).parent().addClass("open");
                  $(this).parent().find("a.dropdown-toggle").addClass("tb-megamenu-clicked");

                  $("#main").addClass('touch-enabled-css');
                  $(".main-header:not(.we-megamenu)").addClass("forced-hover");
                }
              });


              /* contact dropdown */
              $(".lefsid").bind('touchstart', function (e) {

                $(".tb-megamenu-item.level-1.mega.dropdown").each(function () {
                  $(this).removeClass("open");
                  $(this).find("a.dropdown-toggle").removeClass("tb-megamenu-clicked");
                });

                if (!$(this).hasClass("open")) {
                  e.preventDefault();
                  $(this).addClass("open");
                } else {
                  $(this).removeClass("open");
                }
              });

              $(".portal-wrap").bind('touchstart', function (e) {

                if (!$(this).hasClass("open")) {
                  e.preventDefault();
                  $(this).addClass("open");
                  $('.main-header:not(.we-megamenu)').addClass("forced-hover");
                }

                $(".tb-megamenu-item.level-1.mega.dropdown").each(function () {
                  $(this).removeClass("open");
                  $(this).find("a.dropdown-toggle").removeClass("tb-megamenu-clicked");
                });
                $(".lefsid").removeClass("open");
              });
            }

          },
          off: function () {
            if ($('html').hasClass("touch")) {
              /* $(".tb-megamenu-item.level-1.mega.dropdown").each(function () {
               $(this).removeClass("open");
               $(this).find("a.dropdown-toggle").removeClass("tb-megamenu-clicked");
               }); */

              $(".lefsid").removeClass("open");
            }
          }
        }
      });

      if (!$('html').hasClass("touch")) {} else {
        $("ul.subprogram").parent().parent().addClass("with-subprograms");
      }
    }

    function init_if_touch_device() {
      drupalSettings.isTouchDevice = function () {
        return "ontouchstart" in window;
      }

      if (drupalSettings.isTouchDevice()) {
        Drupal.behaviors.jQueryMobileSlideShowTouchAdvance = {
          attach: function (context, settings) {
            var self = Drupal.behaviors.jQueryMobileSlideShowTouchAdvance;
            jQuery.each(jQuery(".views_slideshow_cycle_main.viewsSlideshowCycle-processed"), function (idx, value) {
              value.addEventListener("touchstart", self.handleTouchStart);
              jQuery(value).addClass("views-slideshow-mobile-processed");
            })
            jQuery(self).bind("swipe", self.handleSwipe);
          },
          detach: function () {},
          original: {
            x: 0,
            y: 0
          },
          changed: {
            x: 0,
            y: 0
          },
          direction: {
            x: "",
            y: ""
          },
          fired: false,
          ratio: 25,
          handleTouchStart: function (evt) {
            var self = Drupal.behaviors.jQueryMobileSlideShowTouchAdvance;
            if (evt.touches) {
              if (evt.targetTouches.length != 1) {
                return false;
              } // don't respond to gestures
              self.original = {
                x: evt.touches[0].clientX,
                y: evt.touches[0].clientY
              }
              self.target = jQuery(this).attr("id").replace("views_slideshow_cycle_main_", "");
              Drupal.viewsSlideshow.action({
                "action": "pause",
                "slideshowID": self.target
              });
              evt.target.addEventListener("touchmove", self.handleTouchMove);
              evt.target.addEventListener("touchend", self.handleTouchEnd);
            }
          },
          handleTouchMove: function (evt) {
            var self = Drupal.behaviors.jQueryMobileSlideShowTouchAdvance;
            self.changed = {
              x: (evt.touches.length) ? evt.touches[0].clientX : evt.changedTouches[0].clientX,
              y: (evt.touches.length) ? evt.touches[0].clientY : evt.changedTouches[0].clientY
            };
            h = parseInt(self.original.x - self.changed.x),
              v = parseInt(self.original.y - self.changed.y);
            if (h !== 0) {
              self.direction.x = (h < self.ratio) ? "right" : "left";
            }
            if (v !== 0) {
              self.direction.y = (v < self.ratio) ? "up" : "down";
            }

            if (h > self.ratio || h < self.ratio * -1) {
              jQuery(self).trigger("swipe");
            }
          },
          handleTouchEnd: function (evt) {
            var self = Drupal.behaviors.jQueryMobileSlideShowTouchAdvance;
            evt.target.removeEventListener("touchmove", self.handleTouchMove);
            evt.target.removeEventListener("touchend", self.handleTouchEnd);
            self.fired = false;
          },
          handleSwipe: function (evt) {
            var self = Drupal.behaviors.jQueryMobileSlideShowTouchAdvance;
            if (evt != undefined && self.fired == false) {
              Drupal.viewsSlideshow.action({
                "action": (self.direction.x == "left") ? "nextSlide" : "previousSlide",
                "slideshowID": self.target
              });
              self.fired = true; //only fire advance once per touch
            }
          }
        }


        // contact details
        var ictr_leftsid_clicked = 0;
        $(".lefsid").on("click", function (e) {
          e.preventDefault();
          $(this).unbind("click");

          ictr_leftsid_clicked++;

          if (ictr_leftsid_clicked >= 2) {
            $(this).bind("click");
          }

        });
      }
    }

    function init_education_program() {
      var $el = $('.home-section .home-program');

      if ($el.length) {
        var $items = $el.find('ul#programs > li'),
          $links = $el.find('ul#programs > li > a'),
          $popup = $el.find('.cud-popup'),
          $active = null,
          activeCls = 'active';

        rspHelper.addMQ({
          '(min-width: 768px)': {
            on: function () {
              $links.on('click.homeProgram', function (e) {

                /* ++g++ make sure the accordion is tweaked for the special classes */
                if ($(this).hasClass('default')) {
                  // ignore default behavior
                  hide();
                } else {
                  e.preventDefault();

                  var $this = $(this),
                    isActive = $this.is($active);

                  hide();
                  !isActive && show($this);
                }
              });

              function hide() {
                if ($active) {
                  $active.removeClass(activeCls);
                  $active.parent('li').find('.cud-popup').hide();
                  $active = null;
                }
              }

              function show($el) {
                $el.addClass(activeCls);
                $el.parent('li').find('.cud-popup').fadeIn();
                $active = $el;
              }
            },
            off: function () {
              $links.off('.homeProgram').removeClass(activeCls);
              $popup.hide();
              $active = null;
            }
          },
          '(max-width: 767px)': {
            on: function () {
              $el.insertAfter('.home-section');

              /* ++g++ make sure the accordion is tweaked for the special classes */
              $el.find('ul#programs').slideAccordion();

              /* fixed the dynamic calculation of tbmegamenu */
              // check tb-megamenu-frontend.js
              // $(".tb-megamenu-main .nav-collapse").removeClass("collapse");
            },
            off: function () {
              $el.find('[data-opener]').off('click').removeClass('active');
              $el.find('[data-slide]').hide().removeClass('active');
              $('.home-section').append($el);

              /* fixed the dynamic calculation of tbmegamenu */
              // check tb-megamenu-frontend.js
              // $(".tb-megamenu-main .nav-collapse").addClass("collapse");
            }
          }
        });
      }
    }

    /*
     * Social wall/boxes photos can do a slider (provided the api got more than one (1) image); in replacement of static image stacks
     */
    function handler_social_box_photos() {

      setTimeout(function () {
        $('.social-box .Photos').slick({
          infinite: true,
          autoplay: true,
          autoplaySpeed: 2000,
          slidesToShow: 1,
          speed: 300,
          adaptiveHeight: false,
        });

        $('.social-box .Photos').on('init', function () {
          $(this).find('.slick-slide').css({
            'display': 'block'
          });
        });

        $('.social-box .Photos').on('setPosition', function () {
          $(this).find('.slick-slide').height('auto');
          var slickTrack = $(this).find('.slick-track');
          var slickTrackHeight = $(slickTrack).height();
          $(this).find('.slick-slide').css({
            'height': slickTrackHeight + 'px',
            'display': 'block'
          });
        });
      }, 100);
    }

    /*
     * Hero image sliders extended functions
     */

    /* mouse move timer on primary hero for slick arrows */
    $primary_hero_elapse_counter = 0;
    $("#main-dynamic-banner-provider").mousemove(function (e) {
      $primary_hero_elapse_counter = 0;
      $(".primary-hero .slick__arrow").animate({
        "opacity": 1,
        "visibility": "visible"
      });
    });

    $primary_hero_timer = window.setInterval(function () {
      if ($primary_hero_elapse_counter == 15) {
        $(".primary-hero .slick__arrow").animate({
          "opacity": 0,
          "visibility": "hidden"
        });
        $primary_hero_elapse_counter = 0;
      }
      $primary_hero_elapse_counter++;
    }, 1000);



    

    /*
     * Primary promotion slider extended functions
     */
    function handler_reactive_slick_slider_hero_primary_promotion() {
      // set the position of the primary promotion
      // only exists in the front page.
      var primary_promotion_slick = $('.primary-promotion-main-scroll .slick-initialized').slick('getSlick');

      // primary_hero_slick
      var primary_hero_slick = $('.primary-hero .slick-initialized').slick('getSlick');

      if (typeof primary_promotion_slick != 'undefined') {

        if (typeof primary_hero_slick != 'undefined') {
          $('.primary-promotion-main-scroll').hover(function () {
            primary_hero_slick.slickPause();
          }, function () {
            primary_hero_slick.slickPlay();
          });

        }

        /* $(primary_slick.$slides).each(function (i, elem) {
         $(elem).css({'visibility': 'visible'})
         .parents().css({'visibility': 'visible'});
         }); */

        if (typeof primary_promotion_slick.slickSetOption === "function") {

          primary_promotion_slick.slickSetOption('speed', 1000);

          // primary_slick.slickSetOption('autoplaySpeed', 7000);
          primary_promotion_slick.slickSetOption('autoplaySpeed', 10000);
        }

        $('.primary-promotion-main-scroll').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
          if (nextSlide == 0) {
            if (typeof primary_promotion_slick.slickSetOption === "function") {
              slick.slickSetOption('speed', 1000);
              // slick.slickSetOption('autoplaySpeed', 5000);
              // slick.slickSetOption('autoplaySpeed', 7000);
              slick.slickSetOption('autoplaySpeed', 10000);
            }
          } else {
            if (typeof primary_promotion_slick.slickSetOption === "function") {
              slick.slickSetOption('speed', 1000);
              slick.slickSetOption('autoplaySpeed', 5000);
            }

          }
        });

        // auto play
        // enable to make the
        // promotional slider
        // animating
        // removed: use the slick UI from changing
        //          slick template
        //          from auto-play and non auto-play
        // if (typeof primary_slick.slickSetOption === "function") {
        // primary_slick.slickSetOption('autoplay', true);
        // console.log(primary_slick);
        // }

        // reInit will hopefully
        // reinitialize the primary promotional
        /// sliding box over the hero images.
        if (typeof primary_promotion_slick.reinit === "function") {
          primary_promotion_slick.reinit();
        }

        /* $('.primary-promotion-main-scroll .slick .slick__arrow').css({'display': 'block'}); */

        if (typeof primary_promotion_slick.setPosition === "function") {
          primary_promotion_slick.setPosition();

        }

      }
    }

    function handler_whatsapp() {

      $("a#whatsapp").attr("href", "https://web.whatsapp.com");
      var mq = window.matchMedia("(max-width: 991px)");
      if (mq.matches) {
        mq.addListener(WidthChange);
        WidthChange(mq);
      }
    }

    function WidthChange(mq) {

      var isMobile = {
        Android: function () {
          return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
          return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
          return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
          return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
          return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
      };


      if (mq.matches) {
        if (isMobile.Android()) {

          if ($(".page-node-22294").length > 0) {
            // farsi
            // TODO: to be replaced
            $("a#whatsapp").attr("href", "intent://send/0585867849#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end");
          } else {
            // generic
            $("a#whatsapp").attr("href", "intent://send/0585867849#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end");
          }
        } else {

          if ($('html').hasClass('touch')) {
            if ($(".page-node-22294").length) {
              // farsi
              // TODO: to be replaced
              $("a#whatsapp").attr("href", "tel:+971585867849");
            } else {
              $("a#whatsapp").attr("href", "tel:+971585867849");
            }
          } else {
            $("a#whatsapp").attr("href", "https://web.whatsapp.com");
          }
        }
      };

    }

    /**
     * @deprecated
     */
    function handler_filtering_schools_programs() {


      /* schools and program listing */
      $('#filter-programs').on('keyup', function () {


        var value = $(this).val();

        if (value == '') {
          $('.program-list ul li > .views-field-title a').closest('li').show();
        } else {
          $('.program-list ul li > .views-field-title a').closest('li').show();
          $('.program-list ul li > .views-field-title a:not(:contains(' + value + '))').closest('li').hide();
          $('.program-list ul li > li .views-field-title a:contains(' + value + ')').closest('li').show();
        }


        /* check the schools children, if all is hidden then hide the school name */
        $(".schools .school").each(function () {

          $(this).removeClass('hide');

          /* hide the school if empty */
          var count_total_li = 0,
            count_total_li_visible = 0,
            got_at_least_one_visible = 0;

          $(this).find('.program-list .u-list > li').each(function () {
            count_total_li++;

            if ($(this).is(':visible')) {
              count_total_li_visible++;
              got_at_least_one_visible = 1;
            }
          });

          if (!got_at_least_one_visible) {
            $(this).addClass('hide');
          }


          /* hide the school, if the != to school value */
          var selected_val = $("#filter-schools").val();

          var $school = $(this),
            school_id;

          school_id = $school.find('#school-id').text();

          if (school_id != '') {

            if (selected_val == school_id || selected_val == 0) {

              if (selected_val == 0) {
                if (!got_at_least_one_visible) {
                  $school.addClass('hide');
                }
              } else {
                $school.removeClass('hide');
              }

            } else {
              $school.addClass('hide');
            }
          }

        });


      });

      $("#filter-schools").change(function () {


        if ($('#filter-programs').val() != '') {
          $('#filter-programs').trigger('keyup');
        }

        var selected_val = $(this).val();


        /* check the schools children, if all is hidden then hide the school name */
        $(".schools .school").each(function () {

          $(this).removeClass('hide');

          /* hide the school if empty */
          var count_total_li = 0,
            count_total_li_visible = 0,
            got_at_least_one_visible = 0;

          $(this).find('.program-list .u-list > li').each(function () {
            count_total_li++;

            if ($(this).is(':visible')) {
              count_total_li_visible++;
              got_at_least_one_visible = 1;
            }
          });

          if (!got_at_least_one_visible) {
            $(this).addClass('hide');
          }


          var $school = $(this),
            school_id;

          school_id = $school.find('#school-id').text();

          if (school_id != '') {

            if (selected_val == school_id || selected_val == 0) {

              if (selected_val == 0) {
                if (!got_at_least_one_visible) {
                  $school.addClass('hide');
                }
              } else {

                if (!got_at_least_one_visible) {
                  $school.addClass('hide');
                } else {
                  $school.removeClass('hide');
                }

              }

            } else {
              $school.addClass('hide');
            }
          }

          got_at_least_one_visible = 0;

        });


        $(".schools .school").css({
          'border-bottom': '1px solid #c2c2c2'
        });
        $(".schools .school:visible:last").css({
          'border-bottom': '0'
        });

      });
    }

    function handler_animate_summary() {
      $('.animate-summary').hover(function () {

        var $target = $(this).find('.summary');

        $target.animate({
          scrollTop: $target.prop('scrollHeight')
        }, "slow");

      }, function () {
        $target.animate({
          scrollTop: 0
        }, "fast");
      });
    }

    function handler_toggle_custom_node_edit_pencil() {
      //TODO:// edit pencil icons must be toggled like the rest
    }

    function handle_discover_programs_height() {

      // title
      var highest = 0;
      $(".body--block .programs .wrapper-preview .preview__title").css({
        "height": "auto"
      });

      $('.body--block .programs .wrapper-preview .preview__title').each(function () {
        $(this).find('li').each(function () {

          if ($(this).height() > highest) {
            highest = $(this).height();
          }
        });

      });

      $('.body--block .programs .wrapper-preview .preview__title').css({
        'height': (highest + 15) + 'px'
      });

      // wrapper
      $(".body--block .programs .wrapper-preview").css({
        "height": "auto"
      });

      $(".body--block .programs .wrapper-preview").each(function () {
        $(this).css({
          "height": $(this).height()
        });
      });

      var parent = $(".programs .preview").closest(".body--block");
      $(parent).css({
        "overflow": "initial"
      });
    }


    function handle_social_misc() {

      if ($('.social-item video').length) {

        var attr = $('.social-item video').attr('height');

        if (typeof attr !== typeof undefined && attr !== false) {
          $('.social-item video').removeAttr("height");
        }

        var attr = $('.social-item video').attr('width');

        if (typeof attr !== typeof undefined && attr !== false) {
          $('.social-item video').removeAttr("width");
        }
      }
    }

    function handle_video_front_page_promotion() {

      if ($('.video-showcase .slick-track .slick__slide').length) {

        $('.features-wrapper ul.features a img').hover(function () {
          $(this).addClass("animated pulse");
        }, function () {
          $(this).removeClass("animated pulse");
        });
      }
    }

    /* function handle_testimonial() {
      rspHelper.addMQ({
        '(max-width: 767px)': {
          on: function () {
            $(".testimonials .testimonial").on("click", function () {
              if ($(this).hasClass("active")) {
                $(this).removeClass("active");
              } else {
                $(this).addClass('active');
              }
            });
          },
          off: function () {
            $(".testimonials .testimonial").removeClass('active');
          }
        }
      });
    } */

  })(jQuery, Drupal, drupalSettings);
