(function ($, Drupal, drupalSettings) {

  $(document).ready(function () {

    if ($("#exchange-rate").length) {
      $exchange_str = $("#exchange-rate").html();
      $exchange_num = $exchange_str.replace(/[^\d\.\-]/g, "");
      $exchange = parseFloat($exchange_num);    
        $(".usd").each(function() {        
          $aed_str = $(this).siblings(".aed").html();
          $aed_num = $aed_str.replace(/[^\d\.\-]/g, "");
          $aed = parseFloat($aed_num);        
          $(this).removeClass("d-none");
          $(this).find(".converted").html((($aed / $exchange).toFixed(2)).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });
    }

    // confirm email
    // placeholder
    if ($("input[name='email_with_confirm[mail_2]']").length) {
      $("input[name='email_with_confirm[mail_2]']").attr("placeholder", "Confirm Email");
    }
    /* $('.collapseInnerElementContent').overflowing('.collapseOuter', function(overflowed) { 
      // console.log('This element is being overflowed', overflowed);
    }); */

    $(window).scroll(function() {     
        if ($('header.main-header').length) {
          if ($(document).scrollTop() > $('header.main-header').height()) {
             if ($('.custom-affix').length) {
              $width = $('.custom-affix').width();
              $height = $('.custom-affix').innerHeight() - $('header.main-header').innerHeight();
              $('.custom-affix').addClass('affix');
              $('.custom-affix').css({'width':$width, 'z-index':'1', 'background':'white','top': '0'});
              $('.custom-affix-sibling').css({'margin-top': $height});
            }
          } else {
            if ($('.custom-affix').length) {
              $('.custom-affix').removeClass('affix');
              $('.custom-affix-sibling').css({'margin-top': 'auto'});
              $('.custom-affix').css({'width':'auto', 'height': 'auto'});
            } 
          }
        }
    });

    $(window).resize(function(){
      $(window).trigger("scroll");
    });

    // example, new: /open-house
    $('.auto-popup').click();

  });

})(jQuery, Drupal, drupalSettings);
