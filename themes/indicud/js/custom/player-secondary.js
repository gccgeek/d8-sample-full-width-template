(function ($, Drupal, drupalSettings) {

  $(document).ready(function () {

    $(".front-location-video .card").hover(
      function () {
        if ($(this).find(".player2").hasClass("clicked") == true) {
          $(".front-location-video .card .card-img-overlay .card-text i").removeClass("fa-play-circle").addClass("fa-times-square");
          $(".front-location-video .card .card-img-overlay").css({
            "z-index": "0"
          }).show();
        }
      },
      function () {
        if ($(this).find(".player2").hasClass("clicked") == true) {
          $(".front-location-video .card .card-img-overlay .card-text i").addClass("fa-play-circle").removeClass("fa-times-square");
          $(".front-location-video .card .card-img-overlay").css({
            "z-index": "-1"
          }).show();
        }
      }
    );



    $(".player2").on("click", function () {

      if ($(this).hasClass("clicked") == false) {
        $(this).addClass("clicked");
        $height = $(".front-location-video .card .video-image").height();
        $width = $(".front-location-video .card .video-image").width();

        /* aspectRatio = ( 1920 / 1080 );
        $height = ( newWidth / aspectRatio );
        $width = ( newHeight * aspectRatio ); */

        $(".front-location-video .card .video").removeClass("d-none").show();
        $(".front-location-video .card .video-image").hide();
        $(".front-location-video .card .card-img-overlay").hide().css({
          "z-index": "-1"
        });

        if (typeof window.onYouTubeIframeAPIReady !== 'undefined') {

          player2 = new YT.Player('player2', {
            height: $height,
            width: $width,
            playerVars: {
              autoplay: 1,
              controls: 0,
              disablekb: 1,
              loop: 1,
              showinfo: 0,
              rel: 0
            },
            frameBorder: 0,
            videoId: $(".player2").data("id"),
            events: {
              'onReady': function (event) {
                player2.playVideo();

                if (typeof player != null) {
                  player.mute();
                  player.pauseVideo();
                  player.stopVideo();
                }
              },
              'onStateChange': function (event) {
                if (event.data == YT.PlayerState.BUFFERING) {
                  if (typeof player != null) {
                    player.mute();
                    player.pauseVideo();
                    player.stopVideo();
                  }
                }

                if (event.data === YT.PlayerState.ENDED) {
                  $("#player2").removeClass("d-none");
                  if (typeof player2 != null) {
                    player2.mute();
                    player2.pauseVideo();
                    player2.stopVideo();
                  }
                }

                if (event.data === YT.PlayerState.PLAYING) {
                  if (typeof player != null) {
                    player.mute();
                    player.pauseVideo();
                    player.stopVideo();

                    var audio = $('.player-audio');

                    if (audio.length > 0) {
                      audio.find('i.fa').removeClass('fa-volume-up').addClass('fa-volume-off');
                      audio.removeClass('on');
                    }
                  }
                }
              }
            }
          });
        }
      } else {
        $(this).removeClass("clicked");
        if (typeof player2 != null) {
          player2.mute();
          player2.pauseVideo();
          player2.stopVideo();
          player2.destroy();
        }
        $(".front-location-video .card .video").addClass("d-none").hide();
          $(".front-location-video .card .video-image").show();
          $(".front-location-video .card .card-img-overlay").css({
            "z-index": "0"
          }).show();
          $(".front-location-video .card .card-img-overlay .card-text i").removeClass("fa-times-square").addClass("fa-play-circle");
      }


    });
  });

})(jQuery, Drupal, drupalSettings);
