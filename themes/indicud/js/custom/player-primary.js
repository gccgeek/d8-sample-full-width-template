   // load API
   var tag = document.createElement('script');
   tag.src = "https://www.youtube.com/iframe_api";
   var firstScriptTag = document.getElementsByTagName('script')[0];
   firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

   // define player
   var player;

   function onYouTubeIframeAPIReady() {
     player = new YT.Player('yt-main-player', {
       playerVars: {
         autoplay: 1,
         controls: 0,
         disablekb: 1,
         loop: 1,
         showinfo: 0,
         rel: 0,
         theme: 'dark'
       },
       height: '100%',
       width: '100%'
     });
   }

   (function ($, Drupal, drupalSettings) {

     $(document).ready(function () {
       var $id = null;

       var main = $("#main");

       if ($(".slide__media").length > 0) {
         $(".slide__media").each(function (idx, value) {
           if ($(value).data("youtubeid") != null) {
             if ($id !== null) {
               return $id;
             }
             $id = $(value).data("youtubeid");
           }
         });
       }

       var $slick = $('.primary-hero .slick-initialized').slick('getSlick');

       // there's a video
       // and this is a slick carousel
       if ($id && $slick) {
         // pause slick
         // to check the video position on the list
         if (typeof $slick.slickPause === "function") {
           $slick.slickPause();
         }

         // somewhere in the code
         loadPlayer($id, main, $slick);

         $('.player-audio').click(function () {
           var audio = $(this).find("i");
           if (player && (audio.length > 0)) {
             if (audio.hasClass('fa-volume-up')) {
               audio.removeClass('fa-volume-up').addClass('fa-volume-off');
               player.mute();
             } else {
               audio.removeClass('fa-volume-down').addClass('fa-volume-up');
               player.unMute();
             }
           }
         });
       }


       /*
        * Hero image sliders extended functions
        */

       /* mouse move timer on primary hero for slick arrows */
       $primary_hero_elapse_counter = 0;
       $(".primary-hero").mousemove(function (e) {
         $primary_hero_elapse_counter = 0;
         $(this).find(".slick__arrow").animate({
           "opacity": 1,
           "visibility": "visible"
         });
       });

       $primary_hero_timer = window.setInterval(function () {
         if ($primary_hero_elapse_counter == 15) {
           $(this).find(".slick__arrow").animate({
             "opacity": 0,
             "visibility": "hidden"
           });
           $primary_hero_elapse_counter = 0;
         }
         $primary_hero_elapse_counter++;
       }, 1000);


     }); // document ready

     $(window).on('load', function () {
       rspHelper.addMQ({
         '(max-width: 767px)': {
           on: function () {

             console.log($(".primary-hero .unslick .slide__media.video__play").length);
             console.log($(".primary-hero .unslick .slide__media.video__play").data("youtubeid"));

             if (($(".primary-hero .unslick .slide__media.video__play").length > 0) || ($(".primary-hero .unslick .slide__media.video__play").data("youtubeid"))) {
               if ($(".primary-hero .player-audio").hasClass("invisible")) {
                 $(".player-audio").addClass("mobile-visible");
               }
               if ($(".primary-hero .video-yt-main-player").hasClass("invisible")) {
                 $(".video-yt-main-player").addClass("mobile-visible");
               }
             }
           },
           off: function () {
             if (($(".primary-hero .unslick .slide__media.video__play").length > 0) || ($(".primary-hero .unslick .slide__media.video__play").data("youtubeid"))) {

               if ($(".primary-hero .player-audio").hasClass("mobile-visible")) {
                 $(".player-audio").removeClass("mobile-invisible");
               }
               if ($(".primary-hero .video-yt-main-player").hasClass("mobile-invisible")) {
                 $(".video-yt-main-player").removeClass("mobile-invisible");
               }
             }
           }
         }
       });
     });

     // Ref: https://stackoverflow.com/questions/18139277/loading-youtube-api-in-jquery
     function loadPlayer($id, $main, $slick) {

       if ((typeof player !== "undefined")) {
         player.addEventListener('onReady', function (event) {

           handler_reactive_slick_slider_hero($main, event.target, $slick);

           customOnReady(event);
         });
         player.addEventListener('onStateChange', function (event) {
           customOnStateChange(event);
         });
         player.addEventListener('onError', function (event) {
           customOnError(event);
         });

       } else {
         setTimeout(loadPlayer, 100, $id, $main, $slick);
       }
     }

     function customOnReady(event) {
       // console.log("---------------------------------------------------------------------------- ready");
       if (!$(".player-audio").hasClass("invisible")) {
         $(".player-audio").addClass("invisible");
       }
       var audio = $(".player-audio i");
       audio.removeClass('fa-volume-up fa-volume-down').addClass('fa-volume-up');
       // player.mute();
     }

     function customOnStateChange(event) {
       // console.log("---------------------------------------------------------------------------- state change");
       var isMaybeMobile = false;

       var audio = $(".player-audio i");
       var active_overlay_programs = $("ul#programs li a.active");

       var hero_slick_slide_medias = $('.primary-hero .slide__media');
       var hero_slick = $('.primary-hero .slick-initialized').slick('getSlick');

       rspHelper.addMQ({
         '(max-width: 991px)': {
           on: function () {
             isMaybeMobile = true;
           },
           off: function () {
             isMaybeMobile = false;
           }
         }
       });

       if (event.data > -1) {
         if (isMaybeMobile) {
           // event.target.stopVideo();
         }
       }

       if (audio.hasClass("fa-volume-off")) {
         event.target.mute();
       }
       if (audio.hasClass("fa-volume-up")) {
         event.target.unMute();
       }

       if ((event.data === -1) || (event.data === YT.PlayerState.CUED)) {
         // console.log("Data: " + (event.data == -1 ? "Unstarted" : "Started"));
       }


       if (event.data === YT.PlayerState.PLAYING) {
         // console.log("Data: Playing");
         // $url = event.target.getVideoUrl();
         // var $id = $url.substring($url.lastIndexOf("v=") + 2, $url.length);
         // // console.log("Playing ID: " + $id);

         if ($(".player-audio").hasClass("invisible")) {

           $('.player-audio').removeClass("invisible");
         }
         if ($(".video-yt-main-player").hasClass("invisible")) {
           $(".video-yt-main-player").removeClass("invisible");
         }
         if (typeof hero_slick.slickPause === "function") {
           hero_slick.slickPause();
         }
       }

       if (event.data === YT.PlayerState.ENDED) {
         // console.log("Data: Ended");

         if ($(".primary-hero .unslick .slide__media").length == 1) {
           // console.log("unslick");
           event.target.playVideo();
         } else {
           hero_slick_slide_medias.removeClass("video__play");
           if (!$(".video-yt-main-player").hasClass("invisible")) {
             $(".video-yt-main-player").addClass("invisible");
           }
           if (!$(".player-audio").hasClass("invisible")) {
             $(".player-audio").addClass("invisible");
           }
           if (typeof hero_slick.slickPlay === "function") {
             hero_slick.slickNext();
             hero_slick.slickPlay();
           }
         }
       }
     }

     function customOnError(event) {
       // console.log("---------------------------------------------------------------------------- error");
       if (!$(".player-audio").hasClass("invisible")) {
         $(".player-audio").addClass("invisible");
       }
       if (!$(".video-yt-main-player").hasClass("invisible")) {
         $(".video-yt-main-player").addClass("invisible");
       }

       if (event.target !== null) {
         var $id = null;

         if ($(".slide__media").length > 0) {
           $(".slide__media").each(function (idx, value) {
             if ($(value).data("youtubeid") != null) {
               $id = $(value).data("youtubeid");
             }
           });

           event.target.loadVideoById($id, 0);
           event.target.loadPlaylist($id, 0, 0);
         }
         event.target.stopVideo();
         event.target.mute();

         var audio = $(".player-audio i");
         if (audio.hasClass('fa-volume-up')) {
           audio.removeClass('fa-volume-up').addClass('fa-volume-off');
         }

       }
     }

     function handler_reactive_slick_slider_hero(main, YTPlayer, slick_slider) {

       var $target = main;
       var $player = YTPlayer;
       var $slick = slick_slider;

       if ($slick) {

         // handle first child video
         $firstSlide = $(".primary-hero .slide--0 .slide__content .slide__media")

         if ($firstSlide.data("youtubeid") !== "" && $firstSlide.data("youtubeid") !== undefined) {
           $firstSlide.removeClass("video__play");

           $player.loadVideoById($firstSlide.data("youtubeid"), 0);
           $player.loadPlaylist($firstSlide.data("youtubeid"), 0, 0);

           if (typeof $slick.slickPause === "function") {
             $slick.slickPause();
           }
         } else {
           // slick got 1 element
           // unslick
           $firstSlide = $(".primary-hero .unslick .slide__media");

           if ($firstSlide.data("youtubeid") !== "" && $firstSlide.data("youtubeid") !== undefined) {
             $firstSlide.addClass("video__play");

             $player.loadVideoById($firstSlide.data("youtubeid"), 0);
             $player.loadPlaylist($firstSlide.data("youtubeid"), 0, 0);
           }
         }

         // handle events
         $(".primary-hero").on('beforeChange', function (event, slick, currentSlide, nextSlide) {
           $nextSlide = $(".primary-hero  .slide--" + nextSlide + " .slide__content .slide__media");
           if ($nextSlide.data("youtubeid") !== "") {
             $nextSlide.addClass("video__play");
           } else {
             if (!$(".player-audio").hasClass("invisible")) {
               $(".player-audio").addClass("invisible");
             }
             if (!$(".video-yt-main-player").hasClass("invisible")) {
               $(".video-yt-main-player").addClass("invisible");
             }
             $nextSlide.removeClass("video__play");
           }
         });

         $(".primary-hero").on('afterChange', function (event, slick, currentSlide) {

           $currentSlide = $(".primary-hero  .slide--" + currentSlide + " .slide__content .slide__media");

           if ($currentSlide.data("youtubeid") !== "") {
             $player.loadVideoById($currentSlide.data("youtubeid"), 0);
             $player.loadPlaylist($currentSlide.data("youtubeid"), 0, 0);
           } else {
             if (!$(".player-audio").hasClass("invisible")) {
               $(".player-audio").addClass("invisible");
             }
             if (!$(".video-yt-main-player").hasClass("invisible")) {
               $(".video-yt-main-player").addClass("invisible");
             }
             $player.stopVideo();
           }
         });

         /* override handler for primary hero arrows */
         $(".primary-hero .slick__arrow .slick-next").on('click', function () {
           if (typeof $slick.slickNext === "function") {
             if ($player) {
               $player.pauseVideo();
             }
             $slick.slickNext();
             $slick.slickPlay();
           }
         });

         $(".primary-hero .slick__arrow .slick-prev").on('click', function () {
           if (typeof $slick.slickPrev === "function") {
             if ($player) {
               $player.pauseVideo();
             }
             $slick.slickPrev();
             $slick.slickPlay();
           }
         });
       } else {
         $firstSlide = $(".primary-hero .unslick .slide__media");

         if ($firstSlide.data("youtubeid") !== "") {
           $firstSlide.removeClass("video__play");
           $player.loadVideoById($firstSlide.data("youtubeid"), 0);
           $player.loadPlaylist($firstSlide.data("youtubeid"), 0, 0);
         }
       }
     }

   })(jQuery, Drupal, drupalSettings);
