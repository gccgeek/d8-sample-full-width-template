// Wrap IIFE around your code
(function ($, viewport) {

  
  /* grauation */
  $(document).scroll(function(e){
      // console.log($(window).scrollTop());
      $("#gradbackground").css('background-position-y', $(window).scrollTop() + 'px');
  });

  $(document).ready(function () {
    /* orientation */
    if (window.innerHeight > window.innerWidth) {
      $('body').addClass('portrait');
    }
    if (window.innerWidth > window.innerHeight) {
      $('body').addClass('landscape');
    }

    $(window).resize(function () {
      if (window.innerHeight > window.innerWidth) {
        $('body').removeClass('portrait landscape').addClass('portrait');
      }
      if (window.innerWidth > window.innerHeight) {
        $('body').removeClass('portrait landscape').addClass('landscape');
      }
    });

    
    /* popover */
    $('[data-toggle="popover"]').popover();

    $('body').popover({
      selector: '[rel=popover]',
      trigger: 'click focus',
      html: true,
      placement: 'bottom'
    });

    $('[rel="popover"]').on('show.bs.popover', function () {
      var id = $(this).data('popover-id');
      if (id) {
        $(this).attr('data-content', $('#' + id).html());
      }
    });



    // Executes only in XS breakpoint
    // if(viewport.is('xs')) {
    // ...
    // }

    // Executes in SM, MD and LG breakpoints
    // if(viewport.is('>=sm')) {
    // ...
    // }

    // Executes in XS and SM breakpoints
    if (viewport.is('<=md')) {
      // ...
      $(".primary-hero").addClass('custom-bootstrap-less-than-md');
    }
    if (viewport.is('>md')) {
      // ...
      $(".primary-hero").removeClass('custom-bootstrap-less-than-md');
    }

    // Execute code each time window size changes
    $(window).resize(
      viewport.changed(function () {
        //if(viewport.is('xs')) {
        //    // ...
        // }
        if (viewport.is('<=md')) {
          // ...
          $(".primary-hero").addClass('custom-bootstrap-less-than-md');
        }
        if (viewport.is('>md')) {
          // ...
          $(".primary-hero").removeClass('custom-bootstrap-less-than-md');
        }
      })
    );
  });
})(jQuery, ResponsiveBootstrapToolkit);
