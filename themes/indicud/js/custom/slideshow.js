(function ($, Drupal, drupalSettings) {

  $(window).resize(function () {
    var div_height = $('.views_slideshow_cycle_main').height();
    $('.views_slideshow_cycle_slide,.views_slideshow_cycle_teaser_section').height(div_height);


    /* Reference: https://www.drupal.org/node/1510526 */
    $('.views_slideshow_cycle_main').each(function(){
      var cycleMain = $(this);
      var img_width = 0,
          img_height = 0;
      var clearCSS = {width: "auto", height: "auto"};
      var cycle = cycleMain.children('.views_slideshow_cycle_teaser_section');
      cycleElements = cycle.data("cycle.opts");
      cycle.css(clearCSS);
      cycleMain.find('.views_slideshow_cycle_slide').each(function(i){
        $(this).css(clearCSS);
        var tmp_img_width = $(this).width();
        var tmp_img_height = $(this).height();
        if(tmp_img_width > img_width)
          img_width = tmp_img_width;
        if(tmp_img_height > img_height)
          img_height = tmp_img_height;
        cycleElements.elements[i].cycleW = tmp_img_width;
        cycleElements.elements[i].cycleH = tmp_img_height;
        $(this).css({width: tmp_img_width, height: tmp_img_height});
      });
      cycleMain.height(img_height);
      cycle.css({width: img_width, height: img_height});
      cycle.data("cycle.opts.elements", cycleElements);
    });
  });

})(jQuery, Drupal, drupalSettings);
