(function ($, Drupal, drupalSettings) {

  $(document).ready(function () {
    // clickable - 06/26/2019
    // add  data-clickable="true" data-href="http://google.com"
    // delegate
    $(document.body).on('click', '[data-clickable]', (e) => {
      if ($(e.currentTarget).data('clickable') == true) {

        /* ---- landing page - architecture ---- */
        $(".card-img-overlay .js-custom-none").each(function () {
          $(this).addClass("d-none");
        });

        // make the secondary action
        // bubble to the next target element
        // that responds the current selection
        if (typeof ($(e.currentTarget).data('secondary-target-id')) !== 'undefined') {
          var secondary_target_id = $(e.currentTarget).data('secondary-target-id');

          if (typeof ($(e.currentTarget).data('secondary-change-target-href')) !== 'undefined') {
            var secondary_change_target_href = $(e.currentTarget).data('secondary-change-target-href');

            $(secondary_target_id).data("href", secondary_change_target_href);
          }

        }

        if (typeof ($(e.currentTarget).data('add-class')) !== 'undefined') {
          var add_class = $(e.currentTarget).data('add-class');
          if (add_class == "selected") {
            $(e.currentTarget).find(".card-img-overlay .js-custom-none").removeClass("d-none");
          }
        }
        /* ---- /landing page - architecture specific ---- */

        // open link data-href="{url}"
        if (typeof ($(e.currentTarget).data('href')) !== 'undefined') {
          var href = $(e.currentTarget).data('href');
          window.location = href;
        }

        // toggle class data-toggle-class="{selector class}"
        // applies only on children
        // current target doesn't include
        if (typeof ($(e.currentTarget).data('toggle-class')) !== 'undefined') {
          $(e.currentTarget).find('.toggle').toggleClass($(e.currentTarget).data('toggle-class'));
        }
      }
    });

    $("[data-hoverable='true']").hoverIntent({
      over: function (e) {
        $(e.currentTarget).find('.toggle').removeClass('d-none');
      },
      timeout: 50,
      out: function (e) {
        $(e.currentTarget).find('.toggle').addClass('d-none');
      }
    });

    // accordions
    // toggle class but remove the siblings open class
    $("[data-toggle='collapse']").click(function () {
      if ($(this).hasClass("open")) {
        $(this).removeClass("open");
      } else {
        $("[data-toggle='collapse']").each(function () {
          $(this).removeClass("open");
        });

        $(this).addClass('open');
      }
    });

    // bootstrap modal
    // https://stackoverflow.com/questions/13799377/twitter-bootstrap-modal-stop-youtube-video
    // on modal show: append an autoplay=1 on each possible i-frames on show
    // on modal hide: remove all autoplay=1 on the video url to stop them from playing on the background 
    $('body').on('show.bs.modal', function (e) {
      var $iframes = $(e.target).find("iframe");
      $iframes.each(function (index, iframe) {
        $(iframe)[0].src += "&autoplay=1";
      });
    });

    $("body").on('hidden.bs.modal', function (e) {
      var $iframes = $(e.target).find("iframe");
      $iframes.each(function (index, iframe) {
        if (typeof $(iframe).stopVideo === "function") {
          $(iframe).stopVideo();
        }
        var rawVideoURL = $(iframe)[0].src;
        rawVideoURL = rawVideoURL.replace("&autoplay=1", "");
        $(iframe)[0].src = rawVideoURL;
      });
    });

    $('body').on('click', '.close', function() {
      if ($(this).hasClass("disintegrate")) {
        $("body .disintegration").addClass("disintegrate-active");
      }
    });

    // $('[data-youtube]').youtube_background();

    var testimonials = $('.post-graduation .testimonials').slick({
      prevArrow: '<span class="testimonials-prev"><i class="fa fa-arrow-circle-left fa-2x text-white" aria-hidden="true"></i></span>',
      nextArrow: '<span class="testimonials-next"><i class="fa fa-arrow-circle-right fa-2x text-white" aria-hidden="true"></i></span>',
      infinite: true,
      slidesToShow: 3,
      arrows: true,
      autoplay: true,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 767,
          settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          }
        }
      ],
      afterChange: function(){
        if( item_length == slider.slickCurrentSlide() ){
            slider.slickSetOption("autoplay",false,false)
        };
      }        
    });

    testimonials.on('afterChange', function(event, slick, currentSlide){
      testimonials.slick('slickPause');
    });
        
    $('.post-graduation .filter .testimonial-type').on('click', function(){

      if ($(this).data('id') !== "All") {
        
        $('.post-graduation .testimonials').slick('slickUnfilter');

        if ($(this).data('id') === "Alumni") {
          $('.post-graduation .testimonials').slick('slickFilter', '.Alumni');
        } else if ($(this).data('id') === "Students") {
          $('.post-graduation .testimonials').slick('slickFilter', '.Students');
        } else  if ($(this).data('id') === "Transfer") {
            $('.post-graduation .testimonials').slick('slickFilter', '.Transfer');
        }
      } else {
        console.log("All");
        $('.post-graduation .testimonials').slick('slickUnfilter');
      }

    });
    
  });

})(jQuery, Drupal, drupalSettings);
