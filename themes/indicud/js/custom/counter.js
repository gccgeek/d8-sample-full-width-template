(function ($, Drupal, drupalSettings) {

  $(document).ready(function () {
  
    function makeTimer() {

          var DubaiTime = new Date(2020,5,5,20,00,0,0).toLocaleString("en-US", {timeZone: "Asia/Dubai"});

          // console.log(DubaiTime);
            
          DubaiTime = new Date(DubaiTime);

          var endTime = (Date.parse(DubaiTime) / 1000);
    
          var now = new Date();
          now = (Date.parse(now) / 1000);
    
          var timeLeft = endTime - now;
    
          var days = Math.floor(timeLeft / 86400); 
          var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
          var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
          var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
      
          if (hours < "10") { hours = "0" + hours; }
          if (minutes < "10") { minutes = "0" + minutes; }
          if (seconds < "10") { seconds = "0" + seconds; }
    
          $(".countdown .days").html(days + "<span>Days</span>");
          $(".countdown .hours").html(hours + "<span>Hours</span>");
          $(".countdown .minutes").html(minutes + "<span>Minutes</span>");
          $(".countdown .seconds").html(seconds + "<span>Seconds</span>");		
    
      }
    
      setInterval(function() { makeTimer(); }, 1000);
  
  });

})(jQuery, Drupal, drupalSettings);
