(function ($, Drupal, drupalSettings) {

  $(document).ready(function () {
   
    $('.before-and-after').cocoen();

    $(window).resize(function () {
        $('.before-and-after').each(function(){
            var first = $(this).children().first();
            first.css("width","50%");
            $(this).find(".cocoen-drag").css("left","50%");
        });
      });
  

  });

})(jQuery, Drupal, drupalSettings);
