/*! modernizr 3.5.0 (Custom Build) | MIT *
 * https://modernizr.com/download/?-cssvhunit-cssvmaxunit-cssvminunit-cssvwunit-objectfit-setclasses !*/
!function(e,t,n){function r(e,t){return typeof e===t}function i(){var e,t,n,i,o,s,l;for(var a in x)if(x.hasOwnProperty(a)){if(e=[],t=x[a],t.name&&(e.push(t.name.toLowerCase()),t.options&&t.options.aliases&&t.options.aliases.length))for(n=0;n<t.options.aliases.length;n++)e.push(t.options.aliases[n].toLowerCase());for(i=r(t.fn,"function")?t.fn():t.fn,o=0;o<e.length;o++)s=e[o],l=s.split("."),1===l.length?Modernizr[l[0]]=i:(!Modernizr[l[0]]||Modernizr[l[0]]instanceof Boolean||(Modernizr[l[0]]=new Boolean(Modernizr[l[0]])),Modernizr[l[0]][l[1]]=i),w.push((i?"":"no-")+l.join("-"))}}function o(e){var t=S.className,n=Modernizr._config.classPrefix||"";if(_&&(t=t.baseVal),Modernizr._config.enableJSClass){var r=new RegExp("(^|\\s)"+n+"no-js(\\s|$)");t=t.replace(r,"$1"+n+"js$2")}Modernizr._config.enableClasses&&(t+=" "+n+e.join(" "+n),_?S.className.baseVal=t:S.className=t)}function s(t,n,r){var i;if("getComputedStyle"in e){i=getComputedStyle.call(e,t,n);var o=e.console;if(null!==i)r&&(i=i.getPropertyValue(r));else if(o){var s=o.error?"error":"log";o[s].call(o,"getComputedStyle returning null, its possible modernizr test results are inaccurate")}}else i=!n&&t.currentStyle&&t.currentStyle[r];return i}function l(e,t){return e-1===t||e===t||e+1===t}function a(e){return e.replace(/([a-z])-([a-z])/g,function(e,t,n){return t+n.toUpperCase()}).replace(/^-/,"")}function f(){return"function"!=typeof t.createElement?t.createElement(arguments[0]):_?t.createElementNS.call(t,"http://www.w3.org/2000/svg",arguments[0]):t.createElement.apply(t,arguments)}function u(){var e=t.body;return e||(e=f(_?"svg":"body"),e.fake=!0),e}function d(e,n,r,i){var o,s,l,a,d="modernizr",c=f("div"),p=u();if(parseInt(r,10))for(;r--;)l=f("div"),l.id=i?i[r]:d+(r+1),c.appendChild(l);return o=f("style"),o.type="text/css",o.id="s"+d,(p.fake?p:c).appendChild(o),p.appendChild(c),o.styleSheet?o.styleSheet.cssText=e:o.appendChild(t.createTextNode(e)),c.id=d,p.fake&&(p.style.background="",p.style.overflow="hidden",a=S.style.overflow,S.style.overflow="hidden",S.appendChild(p)),s=n(c,e),p.fake?(p.parentNode.removeChild(p),S.style.overflow=a,S.offsetHeight):c.parentNode.removeChild(c),!!s}function c(e,t){return!!~(""+e).indexOf(t)}function p(e,t){return function(){return e.apply(t,arguments)}}function h(e,t,n){var i;for(var o in e)if(e[o]in t)return n===!1?e[o]:(i=t[e[o]],r(i,"function")?p(i,n||t):i);return!1}function m(e){return e.replace(/([A-Z])/g,function(e,t){return"-"+t.toLowerCase()}).replace(/^ms-/,"-ms-")}function v(t,r){var i=t.length;if("CSS"in e&&"supports"in e.CSS){for(;i--;)if(e.CSS.supports(m(t[i]),r))return!0;return!1}if("CSSSupportsRule"in e){for(var o=[];i--;)o.push("("+m(t[i])+":"+r+")");return o=o.join(" or "),d("@supports ("+o+") { #modernizr { position: absolute; } }",function(e){return"absolute"==s(e,null,"position")})}return n}function g(e,t,i,o){function s(){u&&(delete j.style,delete j.modElem)}if(o=r(o,"undefined")?!1:o,!r(i,"undefined")){var l=v(e,i);if(!r(l,"undefined"))return l}for(var u,d,p,h,m,g=["modernizr","tspan","samp"];!j.style&&g.length;)u=!0,j.modElem=f(g.shift()),j.style=j.modElem.style;for(p=e.length,d=0;p>d;d++)if(h=e[d],m=j.style[h],c(h,"-")&&(h=a(h)),j.style[h]!==n){if(o||r(i,"undefined"))return s(),"pfx"==t?h:!0;try{j.style[h]=i}catch(y){}if(j.style[h]!=m)return s(),"pfx"==t?h:!0}return s(),!1}function y(e,t,n,i,o){var s=e.charAt(0).toUpperCase()+e.slice(1),l=(e+" "+N.join(s+" ")+s).split(" ");return r(t,"string")||r(t,"undefined")?g(l,t,i,o):(l=(e+" "+T.join(s+" ")+s).split(" "),h(l,t,n))}var w=[],x=[],C={_version:"3.5.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,t){var n=this;setTimeout(function(){t(n[e])},0)},addTest:function(e,t,n){x.push({name:e,fn:t,options:n})},addAsyncTest:function(e){x.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=C,Modernizr=new Modernizr;var S=t.documentElement,_="svg"===S.nodeName.toLowerCase(),b=C.testStyles=d;b("#modernizr { height: 50vh; }",function(t){var n=parseInt(e.innerHeight/2,10),r=parseInt(s(t,null,"height"),10);Modernizr.addTest("cssvhunit",r==n)}),b("#modernizr1{width: 50vmax}#modernizr2{width:50px;height:50px;overflow:scroll}#modernizr3{position:fixed;top:0;left:0;bottom:0;right:0}",function(e){var t=e.childNodes[2],n=e.childNodes[1],r=e.childNodes[0],i=parseInt((n.offsetWidth-n.clientWidth)/2,10),o=r.clientWidth/100,a=r.clientHeight/100,f=parseInt(50*Math.max(o,a),10),u=parseInt(s(t,null,"width"),10);Modernizr.addTest("cssvmaxunit",l(f,u)||l(f,u-i))},3),b("#modernizr { width: 50vw; }",function(t){var n=parseInt(e.innerWidth/2,10),r=parseInt(s(t,null,"width"),10);Modernizr.addTest("cssvwunit",r==n)});var z="Moz O ms Webkit",N=C._config.usePrefixes?z.split(" "):[];C._cssomPrefixes=N;var I=function(t){var r,i=prefixes.length,o=e.CSSRule;if("undefined"==typeof o)return n;if(!t)return!1;if(t=t.replace(/^@/,""),r=t.replace(/-/g,"_").toUpperCase()+"_RULE",r in o)return"@"+t;for(var s=0;i>s;s++){var l=prefixes[s],a=l.toUpperCase()+"_"+r;if(a in o)return"@-"+l.toLowerCase()+"-"+t}return!1};C.atRule=I;var T=C._config.usePrefixes?z.toLowerCase().split(" "):[];C._domPrefixes=T,b("#modernizr1{width: 50vm;width:50vmin}#modernizr2{width:50px;height:50px;overflow:scroll}#modernizr3{position:fixed;top:0;left:0;bottom:0;right:0}",function(e){var t=e.childNodes[2],n=e.childNodes[1],r=e.childNodes[0],i=parseInt((n.offsetWidth-n.clientWidth)/2,10),o=r.clientWidth/100,a=r.clientHeight/100,f=parseInt(50*Math.min(o,a),10),u=parseInt(s(t,null,"width"),10);Modernizr.addTest("cssvminunit",l(f,u)||l(f,u-i))},3);var E={elem:f("modernizr")};Modernizr._q.push(function(){delete E.elem});var j={style:E.elem.style};Modernizr._q.unshift(function(){delete j.style}),C.testAllProps=y;var P=C.prefixed=function(e,t,n){return 0===e.indexOf("@")?I(e):(-1!=e.indexOf("-")&&(e=a(e)),t?y(e,t,n):y(e,"pfx"))};Modernizr.addTest("objectfit",!!P("objectFit"),{aliases:["object-fit"]}),i(),o(w),delete C.addTest,delete C.addAsyncTest;for(var W=0;W<Modernizr._q.length;W++)Modernizr._q[W]();e.Modernizr=Modernizr}(window,document);
(function($){

    $(window).on('load', function(){

        // iOS 8.x supports source, srcset but not the picture
        // object fit is support by this version of iOS
        // with this statement, we can also check semi-advance 
        // browser
        var pic = !!window.HTMLPictureElement;

        setTimeout(function(){

                // && !(/Edge/.test(navigator.userAgent))
                if ( !Modernizr.objectfit || !pic) {


                    $('.preview').each(function () {

                        var container = $(this).find('.preview__media'),
                            imgUrl = container.find('img').attr('srcset');

                        if (imgUrl) {
                            container
                                .css({'backgroundImage': 'url(' + imgUrl + ')', 'backgroundSize': 'cover'})
                                .addClass('compat-object-fit');

                            // container.find('img').css({'opacity' : 0});
                        }



                    });


                    /* -------------------------------- custom image/source(s) srcset handler -------------------------------- */

                    /* rs helper for picture srcset */
                    $.fn.srcset = function( options ) {

                        var settings = {
                            mediaQuerySnippetString: '',
                            isBackground: true
                        };

                        settings = $.extend(settings, options);

                        return this.each(function(){
                            var container = $(this);
                            var src = $(this).find('source');
                            var imgSrcset = $(this).find('img').attr('srcset');
                            var containerImageElement = $(this).find('img');

                            // are we using srcset? if yes, then process the changing of image
                            if (imgSrcset) {
                                // image ssrcset is available but
                                // sources might be around
                                // give the sources a chance, if they match or media query string.
                                if (src) {
                                    src.each(function(){

                                        var mediaAttribute = $(this).attr('media');
                                        var mediaQuerySnippetString = settings.mediaQuerySnippetString;

                                        if (mediaAttribute.indexOf(mediaQuerySnippetString) !== -1) {
                                            var srcset = $(this).attr('srcset');

                                            if (srcset) {
                                                srcset = srcset.split(/,/);
                                                if (srcset.length) {
                                                    srcset = srcset[0].split(/\s+/);

                                                    // a lucky image that matches the media query string
                                                    imgSrcset = srcset;
                                                }
                                            }
                                        }
                                    });
                                }

                                // whew, cool
                                // candidate image find was successful
                                if (settings.isBackground) {
                                    container
                                        .css({'backgroundImage': 'url(' + imgSrcset + ')', 'backgroundSize': 'cover'})
                                        .addClass('compat-object-fit');

                                    $(this).find('img').hide();

                                } else {
                                    // don't make it
                                    // a background
                                    $(this).find('img').attr('src', imgSrcset);
                                }

                            }

                            // console.log('------------ each element ------------');
                        });
                    };

                    rspHelper.addMQ({

                        '(min-width: 768px)': {

                            on: function () {

                                $('#block-page-header-media-dynamic-instantiate .hero').srcset({mediaQuerySnippetString: '(min-width: 768px)'});
                                $('#main-front-hero-slider.slick-slider .slide').srcset({mediaQuerySnippetString: '(min-width: 768px)'});
                                $('.news li').srcset({mediaQuerySnippetString: '(min-width: 768px)', isBackground: false});
                                $('.events li').srcset({mediaQuerySnippetString: '(min-width: 768px)', isBackground: false});
                            },

                            off: function () {
                            }
                        },

                        '(min-width: 512px) and (max-width:767px)': {

                            on: function () {
                                $('#block-page-header-media-dynamic-instantiate .hero').srcset({mediaQuerySnippetString: '(min-width: 512px) and (max-width:767px)' });
                                $('#main-front-hero-slider.slick-slider .slide').srcset({mediaQuerySnippetString: '(min-width: 512px) and (max-width:767px)' });
                                $('.news li').srcset({mediaQuerySnippetString: '(min-width: 512px) and (max-width:767px)', isBackground: false});
                                $('.events li').srcset({mediaQuerySnippetString: '(min-width: 512px) and (max-width:767px)', isBackground: false});
                            },

                            off: function () {
                            }
                        },
                        '(min-width: 0px) and (max-width:511px)': {

                            on: function () {
                                $('#block-page-header-media-dynamic-instantiate .hero').srcset({mediaQuerySnippetString: '(min-width: 0px) and (max-width:511px)' });
                                $('#main-front-hero-slider.slick-slider .slide').srcset({mediaQuerySnippetString: '(min-width: 0px) and (max-width:511px)' });
                                $('.news li').srcset({mediaQuerySnippetString: '(min-width: 0px) and (max-width:511px)', isBackground: false});
                                $('.events li').srcset({mediaQuerySnippetString: '(min-width: 0px) and (max-width:511px)', isBackground: false});
                            },

                            off: function () {
                            }
                        }
                    });

                }

            },
            100);


    });

})(jQuery);
