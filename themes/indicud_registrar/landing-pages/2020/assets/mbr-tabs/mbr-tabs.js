jQuery.fn.outerFind = function(selector) {
    return this.find(selector).addBack(selector);
};
function updateId(target) {
    if (jQuery(target).find('.nav-tabs').length !== 0) {
        jQuery(target).outerFind('section[id^="tabs"]').each(function() {
            var componentID = jQuery(this).attr('id');
            var $tabsNavItem = jQuery(this).find('.nav-tabs .nav-item');
            var $tabPane = jQuery(this).find('.tab-pane');

            $tabPane.removeClass('active').eq(0).addClass('active');

            $tabsNavItem.find('a').removeClass('active').removeAttr('aria-expanded')
                .eq(0).addClass('active');

            $tabPane.each(function() {
                jQuery(this).attr('id', componentID + '_tab' + jQuery(this).index());
            });

            $tabsNavItem.each(function() {
                jQuery(this).find('a').attr('href', '#' + componentID + '_tab' + jQuery(this).index());
            });
        });
    }
}

// Mobirise Initilizaton
var isBuilder = jQuery('html').hasClass('is-builder');
if (isBuilder) {
    jQuery(document).on('add.cards', function(e) {
        updateId(e.target);
    });
} else {
    if (typeof window.initTabsPlugin === 'undefined'){
        window.initTabsPlugin = true;
        console.log('init tabs by plugin');
        updateId(document.body);
    }
}