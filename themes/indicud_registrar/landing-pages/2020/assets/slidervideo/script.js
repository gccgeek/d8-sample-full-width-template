(function() {
    'use strict';

    // youtube or vimeo
    function checkPlayerName(url) {
        if (url === 'false') return false;

        var result = url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(&\S+)?/);

        if (result && /youtu\.?be/g.test(result[3])) {
            return 'youtube';
        } else if (result && /vimeo/g.test(result[3])) {
            return 'vimeo';
        }

        return false;
    }

    // youtube or vimeo
    function getVideoId(url) {
        if (url === 'false') return false;

        var result = url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(&\S+)?/);

        return result ? result[6] : false;
    }

    function getYTPreviewUrl(videoId, quality) {
        return 'https://img.youtube.com/vi/' + videoId + '/' +
            (quality || '') + 'default.jpg';
    }

    function getVimeoPreviewUrl(vimeoId, callback) {
        var request = new XMLHttpRequest();
        request.open('GET', 'https://vimeo.com/api/v2/video/' + vimeoId + '.json', true);
        request.onreadystatechange = function() {
            if (this.readyState === 4) {
                if (this.status >= 200 && this.status < 400) {
                    var response = JSON.parse(this.responseText);

                    callback(response[0].thumbnail_large);
                }
            }
        };
        request.send();
        request = null;
    }

    var getYTPreviewUrlWithBestQuality = (function() {
        var cache = {};

        return function(id) {
            var def = jQuery.Deferred();
            if (id in cache) {
                if (cache[id]) {
                    def.resolve(cache[id]);
                } else {
                    def.reject('Preview image not found.');
                }
            } else {
                jQuery('<img>').on('load', function() {
                    if (120 === (this.naturalWidth || this.width)) {
                        // selection of preview in the best quality
                        var file = this.src.split('/').pop();
                        switch (file) {
                            case 'maxresdefault.jpg':
                                this.src = this.src.replace(file, 'sddefault.jpg');
                                break;
                            case 'sddefault.jpg':
                                this.src = this.src.replace(file, 'hqdefault.jpg');
                                break;
                            case 'hqdefault.jpg':
                                this.src = this.src.replace(file, 'default.jpg');
                                break;
                            default:
                                cache[id] = null;
                                def.reject('Preview image not found.');
                                break;
                        }
                    } else {
                        def.resolve(cache[id] = this.src);
                    }
                }).attr('src', getYTPreviewUrl(id, 'maxres'));
            }

            return def;
        };
    })();

    /*
     * check for youtube/vimeo video section and
     * load videopreview from youtube/vimeo on 'add' event and enable YTPlayer/vimeo_player
     * */
    if (!jQuery('html').hasClass('is-builder')) {
        jQuery(document).on('add.cards', function(event) {
            if (!jQuery(event.target).hasClass('carousel')) return;
            var isDesktop = jQuery('html').hasClass('desktop');

            jQuery(event.target).outerFind('[data-bg-video-slide]').each(function() {
                var videoId = getVideoId(jQuery(this).attr('data-bg-video-slide'));
                if (!videoId) return;

                var $preview = jQuery('<div class="mbr-background-video-preview"></div>').css({
                    display: 'none',
                    backgroundSize: 'cover',
                    backgroundPosition: 'center'
                });

                jQuery('.container-slide', this).before($preview);
                var playerName = checkPlayerName(jQuery(this).attr('data-bg-video-slide'));
                var $overlay = jQuery(this).find('.mbr-overlay');

                if (playerName === 'youtube') {
                    getYTPreviewUrlWithBestQuality(videoId).done(function(url) {
                        $preview.css('background-image', 'url("' + url + '")').show();
                    });

                    if (isDesktop && jQuery.fn.YTPlayer && !jQuery(this).find('.playerBox').length) {
                        jQuery('.container-slide', this)
                            .before('<div class="mbr-background-video"></div>').prev()
                            .YTPlayer({
                                videoURL: videoId,
                                containment: 'self',
                                showControls: false,
                                mute: true
                            });

                        if ($overlay.length) {
                            jQuery('.YTPOverlay', this).css({
                                opacity: $overlay.css('opacity'),
                                backgroundColor: $overlay.css('background-color')
                            });
                        }

                        jQuery(this).find('.image_wrapper img').css('opacity', '0');
                        jQuery(this).find('.image_wrapper .mbr-overlay').css('opacity', '0');
                    }
                } else {
                    getVimeoPreviewUrl(videoId, function(url) {
                        $preview.css('background-image', 'url("' + url + '")').show();
                    });

                    if (isDesktop && jQuery.fn.vimeo_player && !jQuery(this).find('.playerBox').length) {
                        jQuery('.container-slide', this)
                            .before('<div class="mbr-background-video"></div>').prev()
                            .vimeo_player({
                                videoURL: videoId,
                                containment: 'self',
                                showControls: false,
                                mute: true
                            });

                        if ($overlay.length) {
                            jQuery('.vimeo_player_overlay', this).css({
                                opacity: $overlay.css('opacity'),
                                backgroundColor: $overlay.css('background-color')
                            });
                        }
                    }
                }
            });

            jQuery(event.target).find('.carousel-item iframe').css({
                transitionProperty: 'opacity',
                transitionDuration: '1000ms'
            });

            // pause YTPlayer/vimeo_player in hidden slides, apply some css rules
            jQuery(this).on('slide.bs.carousel', 'section.carousel', function(event) {
                jQuery(event.target).find('.carousel-item.active .mb_YTPlayer').each(function() {
                    jQuery(this).YTPPause();
                });

                jQuery(event.target).find('.carousel-item.active .vimeo_player').each(function() {
                    jQuery(this).v_pause();
                });

                jQuery(event.target).find('.carousel-item:not(.active) iframe').css('opacity', 0);
            });

            // start YTPPlayer in active slides, apply some css rules
            jQuery(this).on('slid.bs.carousel', 'section.carousel', function(event) {
                jQuery(event.target).find('.carousel-item.active .mb_YTPlayer').each(function() {
                    jQuery(this).YTPPlay();
                });

                jQuery(event.target).find('.carousel-item.active .vimeo_player').each(function() {
                    jQuery(this).v_play();
                });

                jQuery(event.target).find('.carousel-item.active iframe').resize().css('opacity', 1);
            });
        });
    }
})();
