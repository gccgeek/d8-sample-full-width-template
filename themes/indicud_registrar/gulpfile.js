var gulp = require('gulp'),
	sass = require('gulp-ruby-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	plumber = require('gulp-plumber'),
	browserSync = require('browser-sync').create();

var cfg = {
	stylesPath: 'devel/styles',
	stylesPathRtl: 'devel/styles-rtl',
	scriptsPath: 'devel/scripts'
};

gulp.task('compile-styles', function () {
	return sass(cfg.stylesPath + '/main.scss', {
			style: 'expanded', // expanded, compressed
			loadPath: [
				cfg.stylesPath
			],
        sourcemap: false
		})
		.on('error', sass.logError)
		//.pipe(sourcemaps.write('maps', {
        //			includeContent: false,
        //			sourceRoot: cfg.stylesPath
		// }))
		.pipe(gulp.dest('css'))
		.pipe(browserSync.stream({
			match: '**/*.css'
		}));
});

gulp.task('cud-compile-styles', function () {
	return sass(cfg.stylesPath + '/main.scss', {
		style: 'expanded', // expanded, compressed
		loadPath: [
			cfg.stylesPath
		],
		sourcemap: false
	})
		.on('error', sass.logError)
		//.pipe(sourcemaps.write('maps', {
        //			includeContent: false,
        //			sourceRoot: cfg.stylesPath
       //         }))
		.pipe(gulp.dest('css'))
		.pipe(browserSync.stream({
			match: '**/*.css'
		}));
});


gulp.task('compile-styles-rtl', function () {
	return sass(cfg.stylesPathRtl + '/main-rtl.scss', {
			style: 'expanded', // expanded, compressed
			loadPath: [
				cfg.stylesPathRtl
			],
			sourcemap: false
		})
		.on('error', sass.logError)
		// .pipe(sourcemaps.write('maps', {
		// 	includeContent: false,
		// 	sourceRoot: cfg.stylesPathRtl
		// }))
		.pipe(gulp.dest('css'))
		.pipe(browserSync.stream({
			match: '**/*.css'
		}));
});

gulp.task('bs-reload', function () {
	browserSync.reload();
});

gulp.task('bs-init', function () {
	browserSync.init({
		server: '.',
		directory: true
	});
});

gulp.task('uglify-scripts', function () {
	return gulp.src(['js/main.js'])
		.pipe(plumber({
			errorHandler: function (err) {
				console.log(err);
				this.emit('end');
			}
		}))
		.pipe(uglify())
		.pipe(gulp.dest('js'));
});

gulp.task('concat-scripts', function() {
	return gulp.src([cfg.scriptsPath + '/libs/*.js', cfg.scriptsPath + '/!(main).js', cfg.scriptsPath + '/main.js'])
		.pipe(concat('main.js'))
		.pipe(gulp.dest('js'));
});

gulp.task('watch', function () {
	gulp.watch(cfg.stylesPath + '/**/*.scss', ['compile-styles', 'cud-compile-styles'])
	gulp.watch(cfg.stylesPathRtl + '/**/*.scss', ['compile-styles-rtl']);
	gulp.watch(cfg.scriptsPath + '/**/*.js', ['concat-scripts', 'bs-reload']);
	gulp.watch('templates/*.html', ['bs-reload']);
});

gulp.task('default', ['bs-init', 'watch']);