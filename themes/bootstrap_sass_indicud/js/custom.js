/* sticky top */
// When the user scrolls the page, execute myFunction 
/* window.onscroll = function(){
  var header = document.getElementById("header");
  var sticky = header.offsetTop;

  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
} */

/*
* rsHelper using matchMedia
* https://github.com/paulirish/matchMedia.js/
*/
var rspHelper = (function () {
	var isListening = false,
		timeoutID = null,
		queries = {};

	function handleChange(e) {
		clearTimeout(timeoutID);

		timeoutID = setTimeout(function () {
			var execHandlers = {
				on: [],
				off: []
			};

			for(var queryStr in queries) {
				if(queries[queryStr] && queries.hasOwnProperty(queryStr)) {
					var query = queries[queryStr],
						matches = window.matchMedia(queryStr).matches,
						handlers = query.handlers || [],
						handler, action;

					if(matches !== query.matches) {
						query.matches = matches;
						action = matches ? 'on' : 'off';

						handlers.forEach(function (handler) {
							execHandlers[action].push(handler[action]);
						});
					}
				}
			}

			execHandlers.off.forEach(function (fn) {
				fn();
			});

			execHandlers.on.forEach(function (fn) {
				fn();
			});
		}, 100);
	}

	function addMQ(queryObj) {
		if(!isListening) {
			isListening = true;
			window.addEventListener('resize', handleChange, true);
		}

		for(var queryStr in queryObj) {
			if(queryObj[queryStr] && queryObj.hasOwnProperty(queryStr)) {
				var query = queries[queryStr] || (queries[queryStr] = {handlers: [], matches: false}),
					handlers = query.handlers,
					index;

				index = handlers.push({
					on: queryObj[queryStr].on || function () {},
					off: queryObj[queryStr].off || function () {}
				});

				if(window.matchMedia(queryStr).matches) {
					query.matches = true;
					handlers[index - 1].on();
				}
			}
		}
	}

	return {
		addMQ: addMQ
	};
}());

var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
document.documentElement.className += isTouchDevice ? ' touch' : ' no-touch';


(function ($, Drupal, drupalSettings) {

  // 'use strict';

  /* bootstrap indicud */
  /* modal youtube iframe close */
  function playStopVideo(element) {
    if (!element.length) {} else {
      var youtubeFunc = '';
      var youtubeIframe = element.find("iframe")[0].contentWindow;
      element.on('hidden.bs.modal', function (e) {
        youtubeFunc = 'stopVideo';
        youtubeIframe.postMessage('{"event":"command","func":"' + youtubeFunc + '","args":""}', '*');
      });
      element.on('shown.bs.modal', function (e) {
        youtubeFunc = 'playVideo';
        youtubeIframe.postMessage('{"event":"command","func":"' + youtubeFunc + '","args":""}', '*');
      });

      element.modal('handleUpdate');
    }
  }

  $(document).ready(function(){

    $(".header.sticky").css({'padding-top': $(".toolbar-tray-open").css('padding-top')});

     // This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var playerVars = { autoplay: 1, controls: 0, disablekb: 1, loop: 1, showinfo: 0, rel: 0 };
    var player;

    function onYouTubeIframeAPIReady(id) {                         
      if (id) {
          player = new YT.Player('video', {
          width: '100%',
          height: '664',
          frameBorder: 0,
          playlist: id,
          videoId: id,
          playerVars: playerVars,
          events: {
              'onReady': onPlayerReady,
              'onStateChange': onPlayerStateChange
          }
          });
      }
    }

    function onPlayerReady(event) {
      if (player) {
        if (typeof player.unMute === "function") {
          player.unMute();
        }
        event.target.playVideo();
      }
    }

    function onPlayerStateChange(event) {
      if (event.data === YT.PlayerState.ENDED) {
        if (player) {
          if (typeof player.unMute === "function") {
            player.unMute();
          }
          event.target.playVideo();
        }
      }
    }  

    function stopVideo() {
      if (player) {
        if (typeof player.mute === "function") {
          player.mute();
        }
        if (typeof player.stopVideo === "function") {
          player.stopVideo();
        }
        
      }
    }

    function playVideo() {
      if (player) {
        if (typeof player.unMute === "function") {
          player.unMute();
        }
        if (typeof player.playVideo === "function") {
          player.playVideo();
        }
        
      }
    }

    function pauseVideo() {
      if (player) {
        if (typeof player.mute === "function") {
          player.mute();
        }
        if (typeof player.pauseVideo === "function") {
          player.pauseVideo();
        }
        
      }
    }

    $direct_video = $(".direct-video"); 
    $direct_video.on("click", function (e) {
        e.defaultPrevented;

        $direct_video.addClass('remove-button');
        $direct_video.find("img").addClass('d-none');
        $(".direct-video-container .close").removeClass('d-none');
        $direct_video.find("#video").removeClass('d-none');

        // $youtube_id = 'e0P-5VyrZ_w';
        $youtube_id = '';

        if ($direct_video.data("youtubeid")) {
          $youtube_id = $direct_video.data("youtubeid");
          console.log($youtube_id);
        }
        onYouTubeIframeAPIReady($youtube_id);

        playVideo();

    });

    $(".direct-video-container .close").on("click", function (e) {
        e.defaultPrevented;

        pauseVideo();

        $direct_video.removeClass("remove-button");
        $direct_video.find("img").removeClass("d-none");
        $(".direct-video-container .close").addClass("d-none");
        $direct_video.find("#video").addClass("d-none");
    });

    // iframe
    // for mobile
    playStopVideo($("#exampleModalCenter"));
    playStopVideo($("#videoModal"));

    /* TODO://
    rspHelper.addMQ({
      '(max-width: 991px)': {
        
      }
    }); */

    // colorbox
    if (typeof $.colorbox.resize === "function") {
      $.colorbox.resize();
    }

    // animate
    $('.animate').hover(
      function(){
      var $animate = $(this).find('.animated');

      $animate.each(function(){
        if ($(this).data('animate')) {
          $(this).addClass($(this).data('animate'));
        }
      });
    },
    function(){
      var $animate = $(this).find('.animated');

      $animate.each(function(){
        if ($(this).data('animate')) {
          $(this).removeClass($(this).data('animate'));
        }
      });
    }
    );

  });
 

  $(window).bind("load", function () {
    $(".toolbar-icon-toggle-horizontal").on("click", function(){
      $(".header.sticky").css({'padding-top': $(".toolbar-tray-open").css('padding-top')});
    });
  });

})(jQuery, Drupal, drupalSettings);
