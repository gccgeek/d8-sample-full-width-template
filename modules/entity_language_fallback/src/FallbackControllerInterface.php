<?php

namespace Drupal\entity_language_fallback;

use Drupal\Core\Entity\ContentEntityInterface;

interface FallbackControllerInterface  {

  /**
   * @param string $lang_code
   *
   * @return []
   *   Array of language codes for the fallback chain, most preferred languages first.
   */
  public function getFallbackChain($lang_code);

  /**
   * Provides the list of language candidates for a given entity.
   *
   * @param ContentEntityInterface $entity
   *   The entity we are checking for.
   * @param string $language_code
   *   The language we have requested.
   *
   * @return array
   *   List of language fallback candidates for the language for the entity.
   */
  public function getEntityFallbackCandidates(ContentEntityInterface $entity, $language_code);

  /**
   * @param string $lang_code
   *   Language code
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity
   * @return mixed
   *   Fallback entity translation, or FALSE if nothing was found.
   */
  public function getTranslation($lang_code, ContentEntityInterface $entity);

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return []
   *   Array of entity translations, including fallback content.
   */
  public function getTranslations(ContentEntityInterface $entity);

}
