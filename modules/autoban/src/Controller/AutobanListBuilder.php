<?php
/**
 * @file
 * Contains \Drupal\autoban\Controller\AutobanListBuilder.
 */

namespace Drupal\autoban\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a listing of autoban entities.
 *
 * @package Drupal\autoban\Controller
 *
 * @ingroup autoban
 */
class AutobanListBuilder extends ConfigEntityListBuilder {

  private $banProviderList = NULL;

  /**
   * Get ban providers list.
   *
   * @return array
   *   An array ban providers name.
   */
  private function getBanProvidersList() {
    $controller = new AutobanController();
    $providers = [];
    $banManagerList = $controller->getBanProvidersList();
    if (!empty($banManagerList)) {
      foreach ($banManagerList as $id => $item) {
        $providers[$id] = $item['name'];
      }
    }

    return $providers;
  }

  /**
   * Builds the header row for the entity listing.
   *
   * @return array
   *   A render array structure of header strings.
   *
   * @see Drupal\Core\Entity\EntityListController::render()
   */
  public function buildHeader() {
    $header['id'] = $this->t('Id');
    $header['type'] = $this->t('Type');
    $header['message'] = $this->t('Message pattern');
    $header['referer'] = $this->t('Referrer');
    $header['threshold'] = $this->t('Threshold');
    $header['provider'] = $this->t('Provider');

    return $header + parent::buildHeader();
  }

  /**
   * Builds a row for an entity in the entity listing.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to build the row.
   *
   * @return array
   *   A render array of the table row for displaying the entity.
   *
   * @see Drupal\Core\Entity\EntityListController::render()
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['type'] = $entity->type;
    $row['message'] = $entity->message;
    $row['referer'] = $entity->referer;
    $row['threshold'] = $entity->threshold;

    if (!$this->banProviderList) {
      $this->banProviderList = $this->getBanProvidersList();
    }

    if (!empty($this->banProviderList) && isset($this->banProviderList[$entity->provider])) {
      $row['provider'] = $this->banProviderList[$entity->provider];
    }
    else {
      // If ban provider module is disabled.
      $row['provider'] = t('Inactive provider %provider', ['%provider' => $entity->provider]);
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * Operations list in the entity listing.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to build the row.
   *
   * @return array
   *   A render array of the operations.
   */
  public function getOperations(EntityInterface $entity) {
    $operations = $this->getDefaultOperations($entity);

    $operations['test'] = [
      'title' => $this->t('Test'),
      'url' => Url::fromRoute('autoban.test', ['rule' => $entity->id()], ['query' => drupal_get_destination()]),
      'weight' => 1,
    ];
    $operations['ban'] = [
      'title' => $this->t('Ban'),
      'url' => Url::fromRoute('autoban.ban', ['rule' => $entity->id()], ['query' => drupal_get_destination()]),
      'weight' => 2,
    ];

    return $operations;
  }

}
