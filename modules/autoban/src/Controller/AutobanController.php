<?php
/**
 * @file
 * Contains \Drupal\autoban\Controller\AutobanController.
 */

namespace Drupal\autoban\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\autoban\Entity\Autoban;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller routines for user routes.
 */
class AutobanController extends ControllerBase {

  /**
   * Retrieve IP addresses for autoban rule.
   *
   * @param string $rule
   *   Autoban rule ID.
   */
  public function getBannedIp($rule) {
    $entity = autoban::load($rule);

    $connection = Database::getConnection();
    $query = $connection->select('watchdog', 'log');
    $query->fields('log', ['hostname']);

    $group = $query->orConditionGroup();
    $message = trim($entity->message);
    
    // Checking for multiple messages divided by separator.
    $message_items = explode('|', $message);
    if (count($message_items) > 1) {
      foreach ($message_items as $message_item) {
        $group->condition('log.message', '%' . $query->escapeLike(trim($message_item)) . '%', 'LIKE')
          ->condition('log.variables', '%' . $query->escapeLike(trim($message_item)) . '%', 'LIKE');
      }
    }
    else {
      $group->condition('log.message', '%' . $query->escapeLike($message) . '%', 'LIKE')
        ->condition('log.variables', '%' . $query->escapeLike($message) . '%', 'LIKE');
   }

    $query->condition('log.type', trim($entity->type), '=')
      ->condition($group);

    if (!empty($rule->referer)) {
      $query->condition('log.message', '%' . $query->escapeLike(trim($entity->message)) . '%', 'LIKE');
    }
    $query->groupBy('log.hostname');
    $query->addExpression('COUNT(log.hostname)', 'hcount');
    $query->having('COUNT(log.hostname) >= :cnt', [':cnt' => (int) $entity->threshold]);

    $result = $query->execute()->fetchAll();
    return $result;
  }

 /**
   * Get IP ban manager data from provider name.
   *
   * @param string $provider
   *   Ban provider ID.
   *
   * @return array
   *   Ban manager object and ban_type.
   */
  public function getBanManagerData($provider) {
    // Retrieve Ban provider data for the current rule.
    $banProvider = $this->getBanProvidersList($provider);
    if ($banProvider) {
      $connection = Database::getConnection();

      // Get Ban Manager object from AutobanProviderInterface implementation.
      $service = $banProvider['service'];
      if ($service) {
        return [
          'ban_manager' => $service->getBanIpManager($connection),
          'ban_type' => $service->getBanType(),
        ];
      }
    }

    return NULL;

  }

  /**
   * Get IP ban manager data from autoban rule.
   *
   * @param string $rule
   *   Autoban rule ID.
   *
   * @return array
   *   Ban manager object and ban_type.
   */
  public function getBanManagerDataRule($rule) {
    $entity = autoban::load($rule);
    return $this->getBanManagerData($entity->provider);
  }

  /**
   * Get Ban providers list.
   *
   * @param string $provider_id
   *   Ban provider ID.
   *
   * @return array
   *   List ban providers or provider's data.
   */
  public function getBanProvidersList($provider_id = NULL) {
    $banProvidersList = [];

    $container = \Drupal::getContainer();
    $kernel = $container->get('kernel');
    $services = $kernel->getCachedContainerDefinition()['services'];
    foreach ($services as $service_id => $value) {
      $service_def = unserialize($value);
      if (!empty($service_def['properties']) && !empty($service_def['properties']['_serviceId'])) {
        $service_id = $service_def['properties']['_serviceId'];
        $aservices = explode('.', $service_id);
        if (!empty($aservices[1]) && $aservices[1] == 'ban_provider') {
          $service = \Drupal::service($service_id);
          $id = $service->getId();
          $name = $service->getName();
          $banProvidersList[$id] = ['name' => $name, 'service' => $service];
        }
      }
    }

    if (!empty($provider_id)) {
      return isset($banProvidersList[$provider_id]) ? $banProvidersList[$provider_id] : NULL;
    }
    else {
      return $banProvidersList;
    }
  }

  /**
   * Direct ban controller.
   *
   * @param string $ip
   *   IP address.
   * @param string $provider
   *   Provider name.
   *
   * @return bool
   *   IP banned status.
   */
  public function banIpAction($ip, $provider) {
    $banManagerData = $this->getBanManagerData($provider);
    $banned = $this->banIp($ip, $banManagerData, TRUE);

    if ($banned) {
      drupal_set_message($this->t('IP %ip has been banned', ['%ip' => $ip]));
    }
    else {
      drupal_set_message($this->t('IP %ip has not been banned', ['%ip' => $ip]), 'warning');
    }

    $destination = $this->getDestinationArray();
    if (!empty($destination)) {
      $url = Url::fromUserInput($destination['destination']);
      return new RedirectResponse($url->toString());
    }
    else {
      return $this->redirect('entity.autoban.list');
    }
  }
  
  /**
   * Ban address.
   *
   * @param string $ip
   *   IP address.
   * @param array $banManagerData
   *   Ban manager data.
   * @param bool $debug
   *   Show debug message.
   *
   * @return bool
   *   IP banned status.
   */
  public function banIp($ip, $banManagerData, $debug = FALSE) {
    if (empty($banManagerData)) {
      if ($debug) {
        drupal_set_message($this->t('Empty banManagerData.'), 'warning');
      }
      return FALSE;
    }

    $banManager = $banManagerData['ban_manager'];
    
    if (!$this->canIpBan($ip)) {
      if ($debug) {
        drupal_set_message($this->t('Cannot ban this IP.'), 'warning');
      }
      return FALSE;
    }

    if ($banManager->isBanned($ip)) {
      if ($debug) {
        drupal_set_message($this->t('This IP already banned.'), 'warning');
      }
      return FALSE;
    }

    $banType = $banManagerData['ban_type'];

    switch ($banType) {
      case 'single':
        $banManager->banIp($ip);
        break;

      case 'range':
        $ip_range = $this->createIpRange($ip);
        if (empty($ip_range)) {
          // If cannot create IP range banned single IP.
          $banManager->banIp($ip);
        }
        else {
          $banManager->banIp($ip_range['ip_start'], $ip_range['ip_end']);
        }
        break;
     }

     return TRUE;
  } 

  /**
   * Ban addresses.
   *
   * @param array $ip_list
   *   IP addresses list.
   * @param string $rule
   *   Autoban rule ID.
   *
   * @return int
   *   IP banned count.
   */
  public function banIpList(array $ip_list, $rule) {
    $count = 0;
    if (!empty($ip_list) && $rule) {
      // Retrieve Ban manager object for current rule.
      $banManagerData = $this->getBanManagerDataRule($rule);
      if ($banManagerData) {
        foreach ($ip_list as $item) {
          $banStatus = $this->banIp($item->hostname, $banManagerData);
          if ($banStatus) { 
            $count++;
            drupal_set_message($this->t('IP %ip has been banned.', ['%ip' => $item->hostname]));
          }
        }
      }
      else {
        drupal_set_message($this->t('No ban manager for rule %rule', ['%rule' => $rule]), 'error');
      }
    }
    return $count;
  }

  /**
   * Callback for test autoban rule.
   *
   * @param string $rule
   *   Autoban rule ID.
   */
  public function test($rule) {
    $build = [];
    $result = $this->getBannedIP($rule);
    if ($result !== NULL) {
      $header = [
        $this->t('Count'),
        $this->t('Ip address'),
        $this->t('Ban status'),  
      ];

      $rows = [];
      $banManager = NULL;
      $banManagerData = $this->getBanManagerDataRule($rule);
      if ($banManagerData) {
        $banManager = $banManagerData['ban_manager'];
      }

      foreach ($result as $item) {
        $rows[] = [
          'data' => [
            $item->hcount,
            $item->hostname,
            $banManager ? ($banManager->isBanned($item->hostname) ? $this->t('Banned') : '') : $this->t('?'),
          ],
        ];
      }

      $entity = autoban::load($rule);
      $destination = ['destination' => Url::fromRoute('<current>')->toString()];
      $buttons = [];
      $url = Url::fromRoute('autoban.ban', ['rule' => $entity->id()],
        ['query' => $destination, 'attributes' => ['class' => 'button button-action button--primary button--small']]
      );
      $buttons['ban'] = Link::fromTextAndUrl($this->t('Ban IP'), $url)->toString();
      $build['buttons'] = [
        '#theme' => 'item_list',
        '#items' => $buttons,
        '#attributes' => ['class' => 'action-links'],
      ];

      $build['test_table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => $this->t('No hostnames was found.'),
      ];
    }

    return $build;
  }

  /**
   * Check IP address for ban.
   *
   * @param string $ip
   *   IP candidate for ban.
   *
   * @return bool
   *   Can ban.
   */
  public function canIpBan($ip) {
    // You cannot ban your current IP address.
    return !($ip == \Drupal::request()->getClientIp());
  }

  /**
   * Create IP range from single IP.
   *
   * @param string $hostname
   *   IP address for ban.
   *
   * @return array
   *   IP range string for insert to ban table.
   */
  private function createIpRange($hostname) {
    // Make range IP from aaa.bbb.ccc.ddd to aaa.bbb.ccc.0 - aaa.bbb.ccc.255 .
    if (!ip2long($hostname)) {
      // Only IPV4 is available for IP range.
      return NULL;
    }
    $parts = explode('.', $hostname);
    if (count($parts) == 4) {
      $parts[3] = '0';
      $ip_start = implode('.', $parts);
      $parts[3] = '255';
      $ip_end = implode('.', $parts);
      return ['ip_start' => $ip_start, 'ip_end' => $ip_end];
    }
    return NULL;
  }

}
