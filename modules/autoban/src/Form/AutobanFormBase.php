<?php

namespace Drupal\autoban\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\autoban\Controller\AutobanController;

/**
 * @file
 * Class AutobanFormBase.
 *
 * Typically, we need to build the same form for both adding a new entity,
 * and editing an existing entity. Instead of duplicating our form code,
 * we create a base class. Drupal never routes to this class directly,
 * but instead through the child classes of AutobanAddForm and AutobanEditForm.
 *
 * @package Drupal\autoban\Form
 *
 * @ingroup autoban
 */
class AutobanFormBase extends EntityForm {

  /**
   * Define AutobanFormBase class.
   */

  /**
   * @var \Drupal\Core\Entity\Query\QueryFactory
   *   Define QueryFactory variable.
   */
  protected $entityQueryFactory;

  /**
   * Construct the AutobanFormBase.
   *
   * For simple entity forms, there's no need for a constructor. Our autoban
   * form base, however, requires an entity query factory to be injected into it
   * from the container. We later use this query factory to build an entity
   * query for the exists() method.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   An entity query factory for the autoban entity type.
   */
  public function __construct(QueryFactory $query_factory) {
    $this->entityQueryFactory = $query_factory;
  }

  /**
   * Factory method for AutobanFormBase.
   *
   * When Drupal builds this class it does not call the constructor directly.
   * Instead, it relies on this method to build the new object. Why? The class
   * constructor may take multiple arguments that are unknown to Drupal. The
   * create() method always takes one parameter -- the container. The purpose
   * of the create() method is twofold: It provides a standard way for Drupal
   * to construct the object, meanwhile it provides you a place to get needed
   * constructor parameters from the container.
   *
   * In this case, we ask the container for an entity query factory. We then
   * pass the factory to our class as a constructor parameter.
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity.query'));
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::form().
   *
   * Builds the entity add/edit form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   An associative array containing the autoban add/edit form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get anything we need from the base class.
    $form = parent::buildForm($form, $form_state);

    $autoban_rule = $this->entity;

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Id'),
      '#description' => $this->t('Unique rules name'),
      '#default_value' => $autoban_rule->isNew() ? $this->new_default_id() : $autoban_rule->id(),
      '#maxlength' => 64,
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'replace_pattern' => '([^a-z0-9_]+)|(^custom$)',
        'error' => $this->t('Must be unique, and can only contain lowercase letters, numbers, and underscores. Additionally, it can not be the reserved word "custom".'),
      ],
      '#disabled' => !$autoban_rule->isNew(),
    ];

    // Build the form.
    $form['type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Type'),
      '#description' => $this->t('e.g., "page not found".'),
      '#maxlength' => 255,
      '#default_value' => $autoban_rule->type,
      '#required' => TRUE,
    ];

    $form['message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message pattern'),
      '#description' => $this->t('Dblog message pattern. Use delimiter "|" for multiple values.'),
      '#maxlength' => 255,
      '#default_value' => $autoban_rule->message,
      '#required' => TRUE,
    ];

    $form['referer'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Referrer pattern'),
      '#description' => $this->t('URL referrer pattern.'),
      '#maxlength' => 255,
      '#default_value' => $autoban_rule->referer,
      '#required' => FALSE,
    ];

    $thresholds_config = \Drupal::config('autoban.settings')->get('autoban_thresholds');
    $thresholds = !empty($thresholds_config) ?
    explode("\n", $thresholds_config)
    : [1, 2, 3, 5, 10, 20, 50, 100];

    $form['threshold'] = [
      '#type' => 'select',
      '#title' => $this->t('Threshold'),
      '#description' => $this->t('The threshold number of the log entries.'),
      '#default_value' => $autoban_rule->threshold,
      '#options' => array_combine($thresholds, $thresholds),
      '#required' => TRUE,
    ];

    // Retrieve Ban manager list.
    $controller = new AutobanController();
    $providers = [];
    $banManagerList = $controller->getBanProvidersList();
    if (!empty($banManagerList)) {
      foreach ($banManagerList as $id => $item) {
        $providers[$id] = $item['name'];
      }
    }
    else {
      drupal_set_message(
        $this->t('List ban providers is empty. You have to enable at least one Autoban providers module.'),
        'warning'
      );
    }

    // For a single provider.
    if ($autoban_rule->isNew() && count($providers) == 1) {
      $autoban_rule->provider = array_shift(array_values($providers));
    }

    $form['provider'] = [
      '#type' => 'select',
      '#title' => $this->t('IP ban provider'),
      '#default_value' => $autoban_rule->provider,
      '#options' => $providers,
      '#required' => TRUE,
    ];

    // Return the form.
    return $form;
  }

  /**
   * Checks for an existing autoban rule.
   *
   * @param string|int $entity_id
   *   The entity ID.
   * @param array $element
   *   The form element.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if this format already exists, FALSE otherwise.
   */
  public function exists($entity_id, array $element, FormStateInterface $form_state) {
    // Use the query factory to build a new autoban rule entity query.
    $query = $this->entityQueryFactory->get('autoban');

    // Query the entity ID to see if its in use.
    $result = $query->condition('id', $element['#field_prefix'] . $entity_id)
      ->execute();

    // We don't need to return the ID, only if it exists or not.
    return (bool) $result;
  }

  /**
   * Generate default ID for new item.
   *
   * @return string
   *   Default ID.
   */
  public function new_default_id() {
    $query = $this->entityQueryFactory->get('autoban');
    $next = $query->count()->execute() + 1;

    $entity_id = "rule$next";
    $result = $query->condition('id', $entity_id)
      ->execute();
    $exists = (bool) $result;

    return $exists ? '' : $entity_id;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::actions().
   *
   * To set the submit button text, we need to override actions().
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   An array of supported actions for the current entity form.
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    // Get the basic actions from the base class.
    $actions = parent::actions($form, $form_state);

    // Change the submit button text.
    $actions['submit']['#value'] = $this->t('Save');

    // Return the result.
    return $actions;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   *
   * Saves the entity. This is called after submit() has built the entity from
   * the form values. Do not override submit() as save() is the preferred
   * method for entity form controllers.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function save(array $form, FormStateInterface $form_state) {
    // EntityForm provides us with the entity we're working on.
    $autoban = $this->getEntity();

    // Drupal already populated the form values in the entity object. Each
    // form field was saved as a public variable in the entity class. PHP
    // allows Drupal to do this even if the method is not defined ahead of
    // time.
    $status = $autoban->save();

    // Grab the URL of the new entity. We'll use it in the message.
    $url = $autoban->urlInfo();

    // Create an edit link.
    $edit_link = Link::fromTextAndUrl($this->t('Edit'), $url)->toString();

    if ($status == SAVED_UPDATED) {
      // If we edited an existing entity...
      drupal_set_message($this->t('Autoban rule %label has been updated.', ['%label' => $autoban->id()]));
      $this->logger('autoban')->notice('Autoban rule %label has been updated.', ['%label' => $autoban->id(), 'link' => $edit_link]);
    }
    else {
      // If we created a new entity...
      drupal_set_message($this->t('Autoban rule %label has been added.', ['%label' => $autoban->id()]));
      $this->logger('autoban')->notice('Autoban rule %label has been added.', ['%label' => $autoban->id(), 'link' => $edit_link]);
    }

    // Redirect the user back to the listing route after the save operation.
    $form_state->setRedirect('entity.autoban.list');
  }

}
