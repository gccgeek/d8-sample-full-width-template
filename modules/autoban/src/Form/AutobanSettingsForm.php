<?php

namespace Drupal\autoban\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure autoban settings for this site.
 */
class AutobanSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'autoban_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'autoban.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('autoban.settings');
    $thresholds = $config->get('autoban_thresholds');
    if (empty($thresholds)) {
      $thresholds = "1\n2\n3\n5\n10\n20\n50\n100";
    }

    $form['autoban_thresholds'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Thresholds'),
      '#default_value' => $thresholds,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $thresholds = explode("\n", $form_state->getValue('autoban_thresholds'));
    foreach ($thresholds as $threshold) {
      if (!is_numeric($threshold)) {
        $form_state->setErrorByName('autoban_thresholds', $this->t('Threshold value must be an integer.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::configFactory()->getEditable('autoban.settings')
      ->set('autoban_thresholds', $form_state->getValue('autoban_thresholds'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
