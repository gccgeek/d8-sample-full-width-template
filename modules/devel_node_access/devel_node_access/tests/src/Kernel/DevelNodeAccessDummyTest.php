<?php

namespace Drupal\Tests\devel_node_access\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests whether DNA can be installed (dummy test for the testbot).
 *
 * @group devel_node_access
 */
class DevelNodeAccessDummyTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [];

  /**
   * Tests success (to allow the testbot to run).
   */
  public function testDNAInstalled() {
    $this->assertTrue(TRUE);
  }

}
