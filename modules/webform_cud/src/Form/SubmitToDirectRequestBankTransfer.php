<?php

/**
 * @file
 * Contains \Drupal\webform_cud\Form\SubmitToDirectRequestBankTransfer.
 */

namespace Drupal\webform_cud\Form;

use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\search\SearchPageRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the search form for the search block.
 */
class SubmitToDirectRequestBankTransfer extends FormBase {

   /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new SubmitToMigsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Render\RendererInterface
   *   The renderer.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RendererInterface $renderer) {
    $this->configFactory = $config_factory;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_cud_submit_to_migs_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $sus_order_id = null, $ws_agree_to_terms_and_conditions = null) {
   
    if (!$sus_order_id) {
      $response = new LocalRedirectResponse("/international-summer-school/registration/form");
            $response->send();
            return;
    }

    $form['#action'] = "/international-summer-school/registration/paybybank";
    $form['#method'] = 'POST';
    $form["#name"] = 'banktransfer';

    $form['id'] = array(
      '#type' => 'hidden',
      '#value' => $sus_order_id,
    );
    
   

    $form['container']['info_banktransfer'] =
        array(
            '#type' => 'markup',
            '#markup' => "<h2 class='text-align-center m-0 p-0'>Bank Transfer</h2><p class='text-align-center'>Complete your registration.<br />Include Registration Number and International Summer School Informaton.</p>",
        );

        $form['container']['terms_and_conditions'] =
        array(
            '#type' => 'container',
            '#attributes' => array(
                'class' => 'text-align-center m-0 p-0',
            ),
        );
    
    
        $form['container']['terms_and_conditions']['html'] = array(
            '#type' => 'checkbox',
            '#required' => 'true',
            '#value' => $ws_agree_to_terms_and_conditions,
            '#attributes' => array('checked' => 'checked'),
            '#title' => $this->t('Agree to our  <a class="use-ajax" data-dialog-options="{&quot;closeOnEscape&quot;:true, &quot;height&quot;:&quot;95%&quot;, &quot;width&quot;:&quot;60%&quot;}" data-dialog-type="modal" href="/international-summer-school/registration">Refund Policy</a>, <a class="use-ajax" data-dialog-options="{&quot;closeOnEscape&quot;:true, &quot;height&quot;:&quot;95%&quot;, &quot;width&quot;:&quot;60%&quot;}" data-dialog-type="modal" href="/legal-information/terms-and-conditions">Terms and Conditions</a>'),
        );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Bank Transfer'),
      '#name' => '',
    );

    \Drupal::service('page_cache_kill_switch')->trigger();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // This form submits to the search page, so processing happens there.
  }
}
