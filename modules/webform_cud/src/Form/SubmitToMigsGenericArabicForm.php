<?php

/**
 * @file
 * Contains \Drupal\webform_cud\Form\SubmitToMigsGenericArabicForm.
 */

namespace Drupal\webform_cud\Form;

use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\search\SearchPageRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Builds the search form for the search block.
 */
class SubmitToMigsGenericArabicForm extends FormBase {

   /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new SubmitToMigsGenericArabicForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Render\RendererInterface
   *   The renderer.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RendererInterface $renderer) {
    $this->configFactory = $config_factory;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_cud_submit_to_migs_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $hash = NULL, $cbdonline_gateway = NULL, $ws_agree_to_terms_and_conditions = null) {
   
    $language = \Drupal::service('language_manager')->getCurrentLanguage();
     
    if (!$hash || !$cbdonline_gateway) {
             
      $url = Url::fromUri("internal:/payment/form/arabic", ['language' => $language]);

      $response = new LocalRedirectResponse($url->toString());
      
      $response->send();

      return;
    }

    $form['#action'] = $cbdonline_gateway;
    $form['#method'] = 'POST';
    $form["#name"] = 'redirect';

    $form['c'] = array(
      '#type' => 'hidden',
      '#value' => $hash,
    );    

    $theme_manager = \Drupal::service('theme.manager')->getActiveTheme();

    $accepted_logo = 'media/img/accepted-cards.jpg';
    if ($theme_manager) {
        $accepted_logo = '/'. $theme_manager->getPath() .'/'. $accepted_logo;
    }
    
    $form['container']['info_migs'] =         array(
        '#type' => 'markup',
        '#markup' => "<div class='accepted-cards text-align-center mb-3'><img src='$accepted_logo' /></div>",
    );

    $form['container']['info_migs']['html'] =  array(
        '#type' => 'markup',
        '#markup' => "<div class='small mb-3 pb-3 text-align-center w-75 m-auto'><strong>معلومات!</strong> ستتم إعادة توجيهك إلى بوابة الدفع الآمنة عبر الإنترنت لاستكمال عملية الدفع</div>",
    );

    $form['container']['info_migs']['html_2'] =  array(
      '#type' => 'markup',
      '#markup' => "<div class='small mb-3 pb-3 text-align-center w-75 m-auto'><strong>يرجى الانتباه  قبل تقديم الطلب</strong><p>ملاحظة: بمجرد تقديم طلبك ، سيتم إرساله إلى فريق القبول والتسجيل بالجامعة للمراجعة. سيتصل بك أحد أعضاء الفريق خلال 2-5 أيام عمل لمناقشة أي معلومات أخرى مطلوبة وإستكمال اجراءات التقديم. إذا كان لديك أي أسئلة أو استفسارات، يرجى الاتصال بنا على:</p>
      <p>رقم الهاتف / 043219090 <br />
      أو على البريد الالكتروني / apply@cud.ac.ae</p></div>",
    );


    $form['container']['terms_and_conditions'] =
        array(
            '#type' => 'container',
            '#attributes' => array(
                'class' => 'text-align-center m-0 p-0',
            ),
        );


    $form['container']['terms_and_conditions']['html'] = array(
        '#type' => 'checkbox',
        '#required' => 'true',
        '#value' => $ws_agree_to_terms_and_conditions,
        '#attributes' => array('checked' => 'checked'),
        '#title' => $this->t('Agree to our <a class="use-ajax" data-dialog-options="{&quot;closeOnEscape&quot;:true, &quot;height&quot;:&quot;95%&quot;, &quot;width&quot;:&quot;60%&quot;}" data-dialog-type="modal" href="/payment/refund">Refund Policy</a>, <a class="use-ajax" data-dialog-options="{&quot;closeOnEscape&quot;:true, &quot;height&quot;:&quot;95%&quot;, &quot;width&quot;:&quot;60%&quot;}" data-dialog-type="modal" href="/legal-information/terms-and-conditions">Terms and Conditions</a>'),
    );

    if ($language->getId() == 'ar') {
      $form['container']['terms_and_conditions']['html'] = array(
        '#type' => 'checkbox',
        '#required' => 'true',
        '#value' => $ws_agree_to_terms_and_conditions,
        '#attributes' => array('checked' => 'checked'),
        '#title' => $this->t('الموافقة على <a class="use-ajax" data-dialog-options="{&quot;closeOnEscape&quot;:true, &quot;height&quot;:&quot;95%&quot;, &quot;width&quot;:&quot;60%&quot;}" data-dialog-type="modal" href="/payment/refund">Refund Policy</a>, <a class="use-ajax" data-dialog-options="{&quot;closeOnEscape&quot;:true, &quot;height&quot;:&quot;95%&quot;, &quot;width&quot;:&quot;60%&quot;}" data-dialog-type="modal" href="/legal-information/terms-and-conditions">الشروط والأحكام</a>'),
      );
    }

    $form['actions'] = array(
      '#type' => 'actions',
      '#attributes' => array('class' => array('mt-3', 'pt-3', 'text-align-center')),
    );

    // show buttons, if the s is correct
    // sha256 - ThereCanOnlyBeOne
    // A96759FEA229238B2D9FAD7E593AEDBF162BB18491ADC1BAD1C4EE373F962B1A
    // on:
    // <domain>/ar/payment/<id>?s=A96759FEA229238B2D9FAD7E593AEDBF162BB18491ADC1BAD1C4EE373F962B1A
    // if (\Drupal::request()->query->get('s') == 'A96759FEA229238B2D9FAD7E593AEDBF162BB18491ADC1BAD1C4EE373F962B1A') {
      $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Pay'),
        '#name' => '',
        '#attributes' => array('class' => array('btn', 'btn-primary', 'mt-3', 'pt-3', 'text-align-center')),
      );
  
      if ($language->getId() == 'ar') {
        $form['actions']['submit'] = array(
              '#type' => 'submit',
              '#value' => $this->t('ادفع'),
              '#name' => '',
              '#attributes' => array('class' => array('btn', 'btn-primary', 'mt-3', 'pt-3', 'text-align-center')),
            );
      }
    //}

    \Drupal::service('page_cache_kill_switch')->trigger();

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // This form submits to the search page, so processing happens there.
  }
}
