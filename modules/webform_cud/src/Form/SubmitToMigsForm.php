<?php

/**
 * @file
 * Contains \Drupal\webform_cud\Form\SubmitToMigsForm.
 */

namespace Drupal\webform_cud\Form;

use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\search\SearchPageRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the search form for the search block.
 */
class SubmitToMigsForm extends FormBase {

   /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new SubmitToMigsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Render\RendererInterface
   *   The renderer.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RendererInterface $renderer) {
    $this->configFactory = $config_factory;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_cud_submit_to_migs_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $hash = NULL, $cbdonline_gateway = NULL, $ws_agree_to_terms_and_conditions = null) {
   
    if (!$hash || !$cbdonline_gateway) {
      $response = new LocalRedirectResponse("/international-summer-school/registration/form");
            $response->send();
            return;
    }

    $form['#action'] = $cbdonline_gateway;
    $form['#method'] = 'POST';
    $form["#name"] = 'redirect';

    $form['c'] = array(
      '#type' => 'hidden',
      '#value' => $hash,
    );

    

    $theme_manager = \Drupal::service('theme.manager')->getActiveTheme();

    $accepted_logo = 'images/accepted-cards.jpg';
    if ($theme_manager) {
        $accepted_logo = '/'. $theme_manager->getPath() .'/'. $accepted_logo;
    }
    
    $form['container']['info_migs'] =         array(
        '#type' => 'markup',
        '#markup' => "<div class='accepted-cards text-align-center mb-3'><img src='$accepted_logo' /></div>",
    );

    $form['container']['info_migs']['html'] =  array(
        '#type' => 'markup',
        '#markup' => "<div class='alert alert-info alert-dismissible fade show text-align-center w-75 m-auto'><strong>Info!</strong> You will be redirected to Our Secure Online Payment Gateway for payment processing</div>",
    );

    $form['container']['terms_and_conditions'] =
        array(
            '#type' => 'container',
            '#attributes' => array(
                'class' => 'text-align-center m-0 p-0',
            ),
        );


    $form['container']['terms_and_conditions']['html'] = array(
        '#type' => 'checkbox',
        '#required' => 'true',
        '#value' => $ws_agree_to_terms_and_conditions,
        '#attributes' => array('checked' => 'checked'),
        '#title' => $this->t('Agree to our  <a class="use-ajax" data-dialog-options="{&quot;closeOnEscape&quot;:true, &quot;height&quot;:&quot;95%&quot;, &quot;width&quot;:&quot;60%&quot;}" data-dialog-type="modal" href="/international-summer-school/registration">Refund Policy</a>, <a class="use-ajax" data-dialog-options="{&quot;closeOnEscape&quot;:true, &quot;height&quot;:&quot;95%&quot;, &quot;width&quot;:&quot;60%&quot;}" data-dialog-type="modal" href="/legal-information/terms-and-conditions">Terms and Conditions</a>'),
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Pay'),
      '#name' => '',
    );

    \Drupal::service('page_cache_kill_switch')->trigger();

    return $form;
  }

  /* public function webform_cud_cancel(array $form, FormStateInterface &$form_state) {
    $form_state['redirect'] = '/international-summer-school/form';
  } */

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // This form submits to the search page, so processing happens there.
  }
}
