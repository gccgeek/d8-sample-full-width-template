<?php

namespace Drupal\webform_cud\lib;

class RijndaelOpenSSL
{
    const METHOD = 'AES-128-CBC';

    public function decrypt($inputText, $password)
    {
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $ivdata = openssl_random_pseudo_bytes($ivlen);       
        $iv = substr($password, 0, 16);     
        $output = openssl_decrypt(base64_decode($inputText), 'AES-128-CBC', $password, OPENSSL_RAW_DATA, $iv."\0");
        return  $output;
    }

    public function encrypt($inputText, $password)
    {    
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $ivdata = openssl_random_pseudo_bytes($ivlen);       
        $iv = substr($password, 0, 16);       
        $textUTF = mb_convert_encoding($inputText, 'UTF-8');
        $encrypted = openssl_encrypt($inputText, $cipher, $password, $options=OPENSSL_RAW_DATA, $iv."\0");
        $encrypteddata= base64_encode($encrypted);      
        return $encrypteddata;
    }
  
}
