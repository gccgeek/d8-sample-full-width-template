<?php
/**
 * @file
 * Contains \Drupal\webform_cud\Controller\WebformPaymentGenericController.
 */

namespace Drupal\webform_cud\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Url;
use Drupal\webform_cud\lib\RijndaelOpenSSL;

/* use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Config\Entity\Query\Query;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node; */

/**
 * Class WebformPaymentGenericController
 * @package Drupal\webform_cud\Controller
 *
 * Prepare for Direct Migs Payment
 * after webform submission
 * the redirect is added in the Migs Handler section
 *
 */
class WebformPaymentGenericController extends ControllerBase
{
    protected $cbd_whitelisted_return_url = "/payment/process";

    // https://www.cbdonline.ae/Cybersource/Payment/InitiateCreditCardPayment
    protected $cbd_gateway = "https://test.cbdonline.ae/Cybersource/Payment/InitiateCreditCardPayment/";

     // TEST: CBD_43
     protected $cbd_gateway_client_id = "CBD_43";

    // TEST: CJ7HX15T9QLO7TI
    protected $cbd_gateway_encryption_key= "CJ7HX15T9QLO7TI";

    // for encrypting submission id
    // used for transferring from and to processes
    protected $custom_local_use_encryption_key = "117781CUD117781";

    // keys are from webform
    // values are SalesforceFled Ids
    protected $salesforceCommonCUDWebToAPIFields = [

        'campaign_id' => 'Campaign_ID', // hidden, prepopulated in form
        'email' => 'Email', // user
        'first_name' => 'FirstName', // user
        'middle_name' => 'MiddleName', // user
        'last_name' => 'LastName', // user
        'phone' => 'Phone', // user
        'inquiry' => 'Inquiry__c',
        'programs' => 'Program_of_interest__c',
        'birthdate' => 'Date_of_Birth__c',        
        
        'country' => 'Country_of_Residence__c',
        
        'passport' => 'Passport_Number__c',
        'passport_expiry' => 'Passport_Expiry_Date__c', // yyyy-mm-dd
        'file_passport' => 'Passport_Copy__c',

        'is_uae_resident' => 'Is_UAE_Resident__c',
        'emirates_id' => 'State_of_Residence__c',
        'file_emirates_id' => 'Emirates_ID_Copy__c',        
        
        'agree_to_terms_and_conditions' => 'Agreed_to_Terms_and_Conditions__c', // user
        'emailoptout' => 'HasOptedOutOfEmail', // hidden, populated in form
        
        'lead_source' => 'LeadSource', // hidden, dynamic
        'origin_code_1' => 'ORIGIN_CODE_1__c', // hidden, dynamic
        'origin_code_2' => 'ORIGIN_CODE_2__c', // hidden, dynamic
        
        'payment_status' => "Payment_Status__c", // populated after payment
        'payment_order_id' => "Payment_Order_ID__c", // populated after payment
        'payment_amount' => "Payment_Amount__c", // populated after payment
        'payment_response' => "Bank_Response__c", // populated after payment
    ];
    
    /********************************************************/
    /********************************************************/
    /*******************PROCESS******************************/
    /********************************************************/
    /********************************************************/
    public function paymentredirect($str_id = null)
    {
        $language = \Drupal::service('language_manager')->getCurrentLanguage();
        
        $url = Url::fromUri("internal:/payment/form", ['language' => $language]);

        $request = \Drupal::request();
        
        if ($language->getId() == 'ar') {
            if ($route = $request->attributes->get(\Symfony\Cmf\Component\Routing\RouteObjectInterface::ROUTE_OBJECT)) {
            $route->setDefault('_title', 'خروج');
            }
        }

        // redirect URL
        // needs to be whitelisted by the CBD Bank Gateway
        // done by the bank's end
        $config = \Drupal::config('cud.settings');
 
        $host = \Drupal::request()->getSchemeAndHttpHost();

        if ($config->get('migs_generic_whitelisted_url') && ($config->get('migs_generic_whitelisted_url') !== "")) {

            if ($language->getId() == 'ar') {
                
                $url_translated = Url::fromUri("internal:" . $config->get('migs_generic_whitelisted_url'), ['language' => $language]);

                // the full return whitelisted url
                $this->cbd_whitelisted_return_url = $host . $url_translated->toString();
            } else {
                // the full return whitelisted url
                $this->cbd_whitelisted_return_url = $host . $config->get('migs_generic_whitelisted_url');
            }
            
            if ($config->get('migs_gateway_client_id') && $config->get('migs_gateway_client_id') !== "") {
                $this->cbd_gateway_client_id = $config->get('migs_gateway_client_id');
            }

            if ($config->get('migs_gateway_live_encryption_key') && $config->get('migs_gateway_live_encryption_key') !== "") {
                $this->cbd_gateway_encryption_key = $config->get('migs_gateway_live_encryption_key');
            }

            if ($config->get('migs_gateway') && $config->get('migs_gateway') !== "") {
                $this->cbd_gateway = $config->get('migs_gateway');
            }

        } else {
            
            if ($language->getId() == 'ar') {
                
                $url_translated = Url::fromUri("internal:" . $this->cbd_whitelisted_return_url, ['language' => $language]);

                // the full return whitelisted url
                $this->cbd_whitelisted_return_url = $host . $url_translated->toString();
            } else {
                 // the full return whitelisted url
                 $this->cbd_whitelisted_return_url = $host . $this->cbd_whitelisted_return_url;
            }
        }

        $rijndael = new RijndaelOpenSSL();

        $str_id = str_replace(array('PPPPP'), array('%'), $str_id);
        $unhash = $rijndael->decrypt(urldecode($str_id), $this->custom_local_use_encryption_key);

        if ($unhash != true) {
            $response = new LocalRedirectResponse($url->toString());
            $response->send();
            return;
        }

        $webform_submitted_data = $this->getWebformSubmissionData($unhash, $webform_id);

        if ($webform_submitted_data == false) {
            $response = new LocalRedirectResponse($url->toString());
            $response->send();
            return;
        }

        $three_letter_code = '';
      
        // process Salesforce
        $this->processSalesforce($webform_submitted_data);

        // code and fees
        $payable_total_amount = 0;
        $course_full_title = '';
        $order_id = '';

        if ($webform_id === "ar_registration_and_payment") {
            
            $three_letter_code = 'ARA';

            //  program
            $course_full_title = $webform_submitted_data['programs'];
           
            if (preg_match('/(MBA)|(Master)/i', $course_full_title)) {
                // Application fee for Graduate programs is AED 1575 with VAT
                $payable_total_amount = 1575;
            } else {
                // Application fee for Undergraduate programs is AED 525 with VAT
                $payable_total_amount = 525;
            }
            
        }
        
        if ($webform_id === "registration_and_payment") {
            
                $course_node_id = (int)$webform_submitted_data['course'];

                // node embedded information
                // Short Courses
                $node = Node::load($course_node_id);
    
                if (is_numeric($node->field_course_fees->value)) {
                    $payable_total_amount = $node->field_course_fees->value;
                }
    
                if ($node->title->value) {
                    $course_full_title = $node->title->value;
                }
    
                if ($node->field_alternate_title->value) {
                    $three_letter_code = strtoupper(substr($node->field_alternate_title->value, 0, 3));
                }
        }

        if (!$three_letter_code && !$payable_total_amount) {

            $response = new LocalRedirectResponse($url->toString());

            $response->send();
            return;
        }

        if ($three_letter_code === "ARA") {
            $order_id = "$three_letter_code-$unhash";
        } else {
            $order_id = "CE$three_letter_code-$unhash";
        }
        
        $ws_name = (empty($webform_submitted_data['name']['title'])) ? "" : ($webform_submitted_data['name']['title'] . " ");
        $ws_first_name = (empty($webform_submitted_data['name']['first'])) ? "" : ($webform_submitted_data['name']['first'] . " ");
        $ws_middle_name = (empty($webform_submitted_data['name']['middle'])) ? "" : ($webform_submitted_data['name']['middle'] . " ");
        $ws_last_name = (empty($webform_submitted_data['name']['last'])) ? "" : ($webform_submitted_data['name']['last'] . " ");

        $ws_name = $ws_name . $ws_first_name . $ws_middle_name . $ws_last_name;

        $data = '<BankInformation>';
        $data .= '<ClientID>'. $this->cbd_gateway_client_id. '</ClientID>';
        $data .= '<ReturnPage>' . $this->cbd_whitelisted_return_url . '</ReturnPage>';

        $data .= '<CreateToken>false</CreateToken>';
        $data .= '<locale>en-us</locale>';
        $data .= '<PaymentInformation>';
        $data .= '<OrderID>' . (string) $order_id . '</OrderID>';
        $data .= '<TotalAmount>' . $payable_total_amount . '</TotalAmount>';
        $data .= '<TransactionType>sale</TransactionType>';
        $data .= "<OrderDescription>Course Code - $three_letter_code</OrderDescription>";
        $data .= '<Currency>AED</Currency>';
        $data .= '</PaymentInformation>';
        $data .= '</BankInformation>';

        $chars = $rijndael->encrypt($data, $this->cbd_gateway_encryption_key);

        // $decrypted_data = $rijndael->decrypt($chars, $this->cbd_gateway_encryption_key);

        $ws_agree_to_terms_and_conditions = (int) $webform_submitted_data['agree_to_terms_and_conditions'];


        $migs_result_form = \Drupal::formBuilder()->getForm('Drupal\webform_cud\Form\SubmitToMigsGenericForm', $chars, $this->cbd_gateway, $ws_agree_to_terms_and_conditions);
        if ($language->getId() == 'ar')
        {
            $migs_result_form = \Drupal::formBuilder()->getForm('Drupal\webform_cud\Form\SubmitToMigsGenericArabicForm', $chars, $this->cbd_gateway, $ws_agree_to_terms_and_conditions);
        } 

        $output['prefix'] = array(
            '#type' => 'markup',
            '#prefix' => '<div class="content-wrapper">',
        );

        $output['container'] =
        array(
            '#type' => 'container',
            '#attributes' => array(
                'class' => 'col-none',
            ),
        );

        $str_table = "";

        if ($order_id) {
            
            if ($language->getId() == 'ar')
            {
                $str_table .= "<tr><td>رقم الملف</td><td>$order_id</td></tr>";
            } else {
                $str_table .= "<tr><td>Registration Number</td><td>$order_id</td></tr>";
            }         
        }

        if ($payable_total_amount) {
           
            if ($language->getId() == 'ar')
            {
                $str_table .= "<tr><td>رسوم الملف</td><td>".(string)number_format($payable_total_amount,2,".",",")."</td></tr>";
            } else {
                $str_table .= "<tr><td>Registration Amount</td><td>".(string)number_format($payable_total_amount,2,".",",")."</td></tr>";

            }        
        }

        if ($ws_name) {            
            
            if ($language->getId() == 'ar')
            {
                $str_table .= "<tr><td>الإسم الاول</td><td>$ws_name</td></tr>";
            }  else {
                $str_table .= "<tr><td>Name</td><td>$ws_name</td></tr>";

            }          
        }

        if ($webform_submitted_data['email']) {

            if ($language->getId() == 'ar')
            {
                $str_table .= "<tr><td>البريد الالكتروني</td><td>" . $webform_submitted_data['email'] . "</td></tr>";
            } else {
                $str_table .= "<tr><td>Email</td><td>" . $webform_submitted_data['email'] . "</td></tr>";
            }            
        }

        if ($webform_submitted_data['phone']) {

            if ($language->getId() == 'ar')
            {
                $str_table .= "<tr><td>الهاتف / الجوال</td><td>" . $webform_submitted_data['phone'] . "</td></tr>";
            } else {
                $str_table .= "<tr><td>Phone</td><td>" . $webform_submitted_data['phone'] . "</td></tr>";
            }            
        }

        if (array_key_exists('is_cud_student', $webform_submitted_data))
        {
            if ($webform_submitted_data['is_cud_student']) {
                $str_table .= "<tr><td>Is CUD Student?</td><td>" . ($webform_submitted_data['is_cud_student'] ? "Yes" : "") . "</td></tr>";
            }

            if ($webform_submitted_data['cud_student_id']) {
                $str_table .= "<tr><td>ID No.</td><td>" . $webform_submitted_data['cud_student_id'] . "</td></tr>";
            }
        }

        if ($three_letter_code === "ARA") {

            if (array_key_exists('programs', $webform_submitted_data))
            {
                if ($webform_submitted_data['programs']) {
                    if ($language->getId() == 'ar')
                    {
                        $str_table .= "<tr><td>البرامج العربية</td><td>" . $course_full_title . "</td></tr>";
                    } else {
                        $str_table .= "<tr><td>Program</td><td>" . $course_full_title . "</td></tr>";
                    }                   
                }
            }
           
         }
        else if (array_key_exists('course', $webform_submitted_data)) {
            if ($webform_submitted_data['course']) {
                $str_table .= "<tr><td>Course</td><td>" . $course_full_title . "</td></tr>";
            }
        }

        if ($three_letter_code === "ARA") {
            
            $output['container']['title'] =
            array(
                '#type' => 'markup',
                '#markup' => "<h3 class='medium-title mb-1 text-align-center mb-3'>Review and Pay</h3>",
            );

            if ($language->getId() == 'ar')
            {
                 $output['container']['title'] =
                array(
                    '#type' => 'markup',
                    '#markup' => "<h3 class='medium-title mb-1 text-align-center mb-3'>عرض المحتوى والدفع</h3>",
                );
            }

        } else {

            array(
                '#type' => 'markup',
                '#markup' => "<h3 class='medium-title mb-1 text-align-center'>Review and Pay</h3><p class='text-align-center'><strong>(Short Courses)</strong></p>",
            );
           
        }
       
        if ($language->getId() == 'ar')
        {
            $output['container']['table'] =
            array(
                '#type' => 'markup',
                '#markup' => "<div class='info'>
                   <table class='table table-bordered mb-5'>
                   <thead>
                        <tr class='table-active'>
                        <th scope='col'>خاطئة</th>
                        <th scope='col'>بيانات</th>
                        </tr>
                    </thead>$str_table</table>
                   </div>",
            );
        } else {
            $output['container']['table'] =
            array(
                '#type' => 'markup',
                '#markup' => "<div class='info'>
                   <table class='table table-bordered mb-5'>
                   <thead>
                        <tr class='table-active'>
                        <th scope='col'>Field</th>
                        <th scope='col'>Data</th>
                        </tr>
                    </thead>$str_table</table>
                   </div>",
            );
        }        

        
        
      
        $output['container']['form'] = $migs_result_form;
       

        $output['suffix'] = array(
            '#type' => 'markup',
            '#suffix' => '</div>',
        );

        $output['#cache'] = ['max-age' => 0];

        \Drupal::service('page_cache_kill_switch')->trigger();

        return $output;
    }

    /********************************************************/
    /********************************************************/
    /*******************PROCESS******************************/
    /********************************************************/
    /********************************************************/
    public function paymentprocess()
    {
        \Drupal::service('page_cache_kill_switch')->trigger();

        $language = \Drupal::service('language_manager')->getCurrentLanguage();
        
        $host = \Drupal::request()->getSchemeAndHttpHost();

        $request = \Drupal::request();
        
        if ($language->getId() == 'ar') {
            if ($route = $request->attributes->get(\Symfony\Cmf\Component\Routing\RouteObjectInterface::ROUTE_OBJECT)) {
                $route->setDefault('_title', 'حالة الدفع');
            }
        }

        // encode xmls chars
        // and gateway
        $config = \Drupal::config('cud.settings');

        // payment redirect will update the $this->migs_generic_whitelisted_url
        // but with gateway redirection.
        // will reset this value to the default local value
        // so below comparison is clearly respected
        if ($config->get('migs_generic_whitelisted_url') && $config->get('migs_generic_whitelisted_url') !== "") {

            if ($config->get('migs_gateway_live_encryption_key') && $config->get('migs_gateway_live_encryption_key') !== "") {
                $this->cbd_gateway_encryption_key = $config->get('migs_gateway_live_encryption_key');
            }
            
        }

        $rijndael = new RijndaelOpenSSL();

        $encrypted_data = $_POST['c'];

        $decrypted_data = $rijndael->decrypt($encrypted_data, $this->cbd_gateway_encryption_key);

        $str_decrypted_data = (string) $decrypted_data;

        $str_payment_status = "";
        $payment_status = -1;

        $payment_CBDReferenceNo = "";
        $payment_CCReferenceNo = "";
        $payment_OrderID = "";
        $payment_Amount = "";
        $payment_PaymentDate = "";
        $payment_PaymentTime = "";

        $xmlobj = simplexml_load_string($str_decrypted_data);

        if (is_object($xmlobj)) {
            if (property_exists($xmlobj, 'Header')) {
                if (property_exists($xmlobj->Header, 'ResponseCode')) {
                    if ((string) $xmlobj->Header->ResponseCode === "00") {
                        $str_payment_status = (string) $xmlobj->Header->ResponseMsg;
                        $payment_status = (int) $xmlobj->Header->ResponseCode;
                    } else {
                        $str_payment_status = (string) $xmlobj->Header->ResponseMsg;
                        $payment_status = (int) $xmlobj->Header->ResponseCode;
                    }
                }
            }
            if (property_exists($xmlobj, 'Body')) {
                if (property_exists($xmlobj->Body, 'PaymentInformation')) {
                    if (property_exists($xmlobj->Body->PaymentInformation, 'CBDReferenceNo')) {
                        $payment_CBDReferenceNo = (string) $xmlobj->Body->PaymentInformation->CBDReferenceNo;
                    }
                    if (property_exists($xmlobj->Body->PaymentInformation, 'CCReferenceNo')) {
                        $payment_CCReferenceNo = (string) $xmlobj->Body->PaymentInformation->CCReferenceNo;
                    }
                    if (property_exists($xmlobj->Body->PaymentInformation, 'OrderID')) {
                        $payment_OrderID = (string) $xmlobj->Body->PaymentInformation->OrderID;
                    }
                    if (property_exists($xmlobj->Body->PaymentInformation, 'Amount')) {
                        $payment_Amount = (string) $xmlobj->Body->PaymentInformation->Amount;
                    }
                    if (property_exists($xmlobj->Body->PaymentInformation, 'PaymentDate')) {
                        $payment_PaymentDate = (string) $xmlobj->Body->PaymentInformation->PaymentDate;
                    }
                    if (property_exists($xmlobj->Body->PaymentInformation, 'PaymentTime')) {
                        $payment_PaymentTime = (string) $xmlobj->Body->PaymentInformation->PaymentTime;
                    }

                }
            }
        }

        $submission_id = (int) preg_replace('/\D/', '', $payment_OrderID);
        $webform_id = '';        

        // TODO:// work on unsuccessful payment
        // push it to salesforce
        if (!empty($submission_id)) {

            if ($this->cbd_gateway_encryption_key == "CJ7HX15T9QLO7TI")
            {
                $str_decrypted_data = "---TEST---" . $str_decrypted_data;
            }

            $this->updateWebformSubmissionData($submission_id, array(
                'payment_status' => $payment_status == 0 ? "PAID" : "UNSUCCESFUL",
                'payment_order_id' => $payment_OrderID,
                'payment_amount' => $payment_Amount,
                'payment_response' => $str_decrypted_data,
            ));

            // update webform
            $webform_submission = $this->getWebformSubmissionData($submission_id, $webform_id);

            if ($webform_submission) {
                $this->processSalesforce($webform_submission);
            }
        }

        $output['prefix'] = array(
            '#type' => 'markup',
            '#prefix' => '<div class="container-wrapper">',
        );

        $output['container'] =
        array(
            '#type' => 'container',
            '#attributes' => array(
                'class' => 'col-none',
            ),
        );

        $output['title']['html'] =
        array(
            '#type' => 'markup',
            '#markup' => "",
        );

        if ($payment_status == 0) {

            $output['title']['html']['status'] =
            array(
                '#type' => 'markup',
                '#markup' => "<div class='position-relative w-25 alert alert-success m-auto'><h1 class='text-align-center'><i class='fa fa-check-circle'><!-- empty --></i></h1><h4 class='text-align-center  mb-0'><strong>" . ucwords($str_payment_status) . "</strong></h4></div><div class='description text-align-center mt-3 pt-3 mb-3'><h5>Registration and Payment successful.</h5><p class='mt-3 mb-3'><strong>You will be receiving a confirmation receipt.<br />Check your email.</strong></p></div>",
            );

            if ($language->getId() == 'ar') {
                $output['title']['html']['status'] =
                array(
                    '#type' => 'markup',
                    '#markup' => "<div class='position-relative w-25 alert alert-success m-auto'><h1 class='text-align-center'><i class='fa fa-check-circle'><!-- empty --></i></h1><h4 class='text-align-center  mb-0'><strong>" . ucwords($str_payment_status) . "</strong></h4></div><div class='description text-align-center mt-3 pt-3 mb-3'><h5>.تم التسجيل والدفع بنجاح</h5><p class='mt-3 mb-3'><strong>.سوف يتم استلام رسالة تأكيد <br />.راجع البريد الإلكتروني الخاص بك</strong></p></div>",
                );
            } 
          
        } else {

            $url = Url::fromUri("internal:/payment/form", ['language' => $language])->toString();            

            $output['title']['html']['status_red'] =
            array(
                '#type' => 'markup',
                '#markup' => "<div class='position-relative alert w-75 alert-danger m-auto'><h1 class='text-align-center'><i class='fa fa-times-circle'><!-- empty --></i></h1><h4 class='text-align-center  mb-0'><strong>" . ucwords($str_payment_status) . "</strong></h4></div><div class='description text-align-center mt-3 pt-3 mb-3'><h5>Payment not successful.</h5><p class='mt-3 mb-3'><strong>Please retry or contact <a href='mailto:info@cud.ac.ae'>info@cud.ac.ae</a> and attach the registration information.</p><p><a href='$url'>Back to registration form</p></a></p></div>",
            );

            if ($language->getId() == 'ar') {

                if ($webform_id === "ar_registration_and_payment") {
                    $url = Url::fromUri("internal:/payment/form/arabic", ['language' => $language])->toString();
                }

                $output['title']['html']['status_red'] =
                array(
                    '#type' => 'markup',
                    '#markup' => "<div class='position-relative alert w-75 alert-danger m-auto'><h1 class='text-align-center'><i class='fa fa-times-circle'><!-- empty --></i></h1><h4 class='text-align-center  mb-0'><strong>" . ucwords($str_payment_status) . "</strong></h4></div><div class='description text-align-center mt-3 pt-3 mb-3'><h5>لم تتم عملية الدفع</h5><p class='mt-3 mb-3'><strong>يرجى المحاولة مره أرى أو تواصل على <a href='mailto:info@cud.ac.ae'>info@cud.ac.ae</a><br /> .يرجى إرفاق بيانات التسجيل الخاصة بك</p><p><a href='$url'>عودة إلى إستمارة التسجيل</p></a></p></div>",
                );                
            }

            
        }

        $output['suffix'] = array(
            '#type' => 'markup',
            '#suffix' => '</div>',
        );

        $output['#cache'] = ['max-age' => 0];

        return $output;
    }

    /* webform */
    public function getWebformSubmissionData($sid = 0, &$webform_id = null)
    {

        // Load submission using sid.
        /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
        $webform_submission = \Drupal\webform\Entity\WebformSubmission::load($sid);

        if ($webform_submission) {
            // Get webform id
            $webform = $webform_submission->getWebform();

            if ($webform) {
                $webform_id = $webform->Id();
            }

            // Get submission data.
            $data = $webform_submission->getData();

            if ($data) {
                return $data;
            }

            return false;
        }

        return false;
    }

    /**
     * updateWebformSubmissionData
     * Update salesforce with the provided associative array pair
     * of key value
     *
     * @return void
     */
    public function updateWebformSubmissionData($sid, $custom_fields)
    {
        // Load submission using sid.
        /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
        $webform_submission = \Drupal\webform\Entity\WebformSubmission::load($sid);

        if ($webform_submission) {
            // Get submission data.
            $data = $webform_submission->getData();

            foreach ($custom_fields as $k => $v) {
                // Change submission data.
                $data[$k] = $v;
            }

            // Set submission data.
            $webform_submission->setData($data);

            // Save submission.
            $webform_submission->save();

            return true;
        }

        return false;
    }

    /* salesforce */
    /* helpers */

    /**
     * processSalesforce
     * Submit submission and payment info to Salesforce
     *
     * @return bool|mixed|void
     */
    protected function processSalesforce($webform_submission, $code = null)
    {

        // we have data
        // then process it
        if ($webform_submission) {
            $this->salesforceAuthorize();

            if ($this->access) {
                $this->salesforceRemotePost($webform_submission, $code);
            }
        }

    }

    /**
     * salesforceAuthorize
     * Get Salesforce authorization to be able the
     * Api connectivity
     *
     * @return bool|mixed|void
     */
    protected function salesforceAuthorize()
    {

        $config = \Drupal::config('cud.settings');

        if (!$config->get('webform_salesforce')) {

            \Drupal::logger('WebformPaymentController')->notice("CUD config settings MUST be checked to allow submission to Salesforce.");

            return;
        }

        $salesforce_token_url = $config->get('sales_force_token_url');

        if (empty($salesforce_token_url)) {
            \Drupal::logger('WebformPaymentController')->notice("CUD can't connect to Salesforce. Token URL missing.");
            return;
        }

        $sales_force_lead_endpoint_url = $config->get('sales_force_lead_endpoint_url');
        $salesforce_client_id = $config->get('client_id');
        $salesforce_grant_type = "password";
        $salesforce_redirect_url = $config->get('redirect_url');
        $salesforce_client_secret = $config->get('client_secret');
        $salesforce_client_username = $config->get('username');
        $salesforce_client_password = $config->get('password');

        if ($salesforce_redirect_url != "") {
            $oauthUrl = "$salesforce_token_url?grant_type=$salesforce_grant_type&client_id=$salesforce_client_id&redirect_uri=$salesforce_redirect_url&client_secret=$salesforce_client_secret&username=$salesforce_client_username&password=$salesforce_client_password";
        } else {
            $oauthUrl = "$salesforce_token_url?grant_type=$salesforce_grant_type&client_id=$salesforce_client_id&client_secret=$salesforce_client_secret&username=$salesforce_client_username&password=$salesforce_client_password";
        }

        if ($access = $this->getCurlWithHeaderPostfields($oauthUrl, 1)) {
            if (isset(json_decode($access)->access_token)) {

                $this->access = json_decode($access);

                return $this->access;
            }
        }

        return false;
    }

    /**
     * salesforceFinalMapWebToApi
     * Map the data for salesforce api posting
     *
     * @param array $request_post_data
     * @return array
     */
    protected function salesforceFinalMapWebToApi(array $request_post_data)
    {

        // date: 1999-01-01
        // checkbox: true

        // process all known documents
        // and append to the inquiry field
        $inquiry = '';

        foreach ($request_post_data as $k => $v) {

            if (in_array($k, array('file_passport','file_emirates_id','file_high_school_diploma', 'file_high_school_transcript','file_bachelor_degree', 'file_university_transcript', 'file_english_proficiency','file_additional_document_1','file_additional_document_2','file_additional_document_3'))) {
                $request_post_data[$k] = $this->getFileUri($request_post_data[$k]);

                if ($request_post_data[$k] != null) {
                    $inquiry .= $k . " : " . $request_post_data[$k] . "; "; 
                }
            }
            
            if ($k === 'is_cud_student') 
                $request_post_data['is_cud_student'] = ((int)$request_post_data['is_cud_student'] >= 1) ? 1 : 0;

            if ($k === 'cud_student_id') 
                $request_post_data['cud_student_id'] = ((int)$request_post_data['cud_student_id'] >= 1) ? 1 : 0;

            if ($k === 'is_uae_resident') 
                $request_post_data['is_uae_resident'] = ((int)$request_post_data['is_uae_resident'] >= 1) ? 1 : 0;

            if ($k === 'agree_to_terms_and_conditions') 
                $request_post_data['agree_to_terms_and_conditions'] = ((int)$request_post_data['agree_to_terms_and_conditions'] >= 1) ? 1 : 0;
            
            if ($k === 'course') 
            {
                $node = Node::load((int)$v);

                if ($node->title->value) {
                    $request_post_data['programs'] = $node->title->value;
                }                
            }

            if ($k === 'emailoptout') 
                $request_post_data['emailoptout'] = ((int)$request_post_data['emailoptout'] >= 1) ? 1 : 0;

            if (is_array($v) && $k === 'name') {
                $request_post_data['first_name'] = $v['first'];
                $request_post_data['middle_name'] = $v['middle'];
                $request_post_data['last_name'] = $v['last'];
            }

        }

        $request_post_data['inquiry'] = $inquiry;

        foreach ($this->salesforceCommonCUDWebToAPIFields as $k => $v) {
            if (array_key_exists($k, $request_post_data)) {
                    $new_request_post_data[$v] = $request_post_data[$k];
            }
        }

        if (!array_key_exists('LeadSource', $request_post_data) || empty($request_post_data['LeadSource'])) {
            $new_request_post_data['LeadSource'] = \Drupal::request()->get('src') != null ? htmlentities(\Drupal::request()->get('src'), ENT_QUOTES) : "webform";
        }
        if (!array_key_exists('ORIGIN_CODE_1__c', $request_post_data) || empty($request_post_data['ORIGIN_CODE_1__c'])) {
            $new_request_post_data['ORIGIN_CODE_1__c'] = \Drupal::request()->get('o') != null ? htmlentities(\Drupal::request()->get('o'), ENT_QUOTES) : "main-site";
        }
        if (!array_key_exists('ORIGIN_CODE_2__c', $request_post_data) || empty($request_post_data['ORIGIN_CODE_2__c'])) {
            $new_request_post_data['ORIGIN_CODE_2__c'] = \Drupal::request()->get('so') != null ? htmlentities(\Drupal::request()->get('so'), ENT_QUOTES) : "";
        }

        if ($new_request_post_data['Email'] != "" && $new_request_post_data['Phone'] != "") {
            return $new_request_post_data;
        }
        
        return null;
    }

    protected function getFileUri($fid = null) {
        
        if ($fid == null) {
            return null;
        }

        $service = \Drupal::service('entity.manager')->getStorage('file');
        $file = $service->load($fid);

        if ($file) {
            return file_create_url($file->getFileUri());
        }

        return null;
    }

    /**
     * salesforceGetLead
     * Get the Salesforce Lead
     *
     * @param null $email
     *  Email field value of Lead during submission process
     *
     * @return bool|mixed|string
     *  Lead object or false
     */
    public function salesforceGetLead($email = null)
    {

        if (!$this->access) {
            return false;
        }

        $result = \Drupal::httpClient()->get($this->access->instance_url . "/services/data/v40.0/query?q=SELECT+Id,Email+from+Lead+Where+Email='$email'", [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->access->access_token,
            ],
        ])->getBody()->getContents();

        return $result;

    }

    /**
     * salesforceGetLeadMembership
     * Membership of the Lead in Salesforce CampaignMember object
     *
     * @param null $lead_id
     *  Lead ID of the Salesforce Lead object
     *
     * @return bool|mixed|string
     *  CampaignMember object or false
     */
    public function salesforceGetLeadMembership($lead_id = null, $campaign_id = null)
    {

        if (!$this->access) {
            return false;
        }

        $result = \Drupal::httpClient()->get($this->access->instance_url . "/services/data/v40.0/query?q=SELECT+LeadId,CampaignId+from+CampaignMember+Where+LeadId='" . $lead_id . "'+AND+CampaignId='" . $campaign_id . "'", [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->access->access_token,
            ],
        ])->getBody()->getContents();

        return $result;

    }

    /**
     * salesforceAddUpdateLead
     * Updates the Salesforce object in relation to the mapped submitted key=>value pairs data string
     *
     * @param null $data_string
     *  key => value pair; using the API REST / SOAP object and endpoints
     *
     * @param null $object_url
     *  the endpoint (minus the instance url) for the CampaignMember object
     *
     * @param null $operation
     *  standard operation string, such as "update"
     *
     * @return bool|mixed|string
     *  Successful salesforce json formatted response object or false
     *
     */
    public function salesforceAddUpdateLead($data_string = null, $object_url = null, $operation = null)
    {

        if (!$this->access) {
            return false;
        }

        $http_header = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'Authorization: Bearer ' . $this->access->access_token,
        );

        $result = $this->getCurlWithHeaderPostfields($this->access->instance_url . $object_url, 0, $http_header, $data_string, $operation);

        return $result;

    }

    /**
     * salesforceAddUpdateLeadMembership
     * Updates the Salesforce CampaignMember object
     *
     * @param null $lead_id
     *  ID of the Lead object
     *
     * @param null $campaign_id
     *  ID of the Campaign object
     *
     * @return bool|mixed|string
     *  Successful salesforce json formatted response object or false
     *
     */
    public function salesforceAddUpdateLeadMembership($lead_id = null, $campaign_id = null)
    {

        if (!$this->access) {
            return false;
        }

        $data_string = json_encode(
            array(
                'LeadId' => $lead_id,
                'CampaignId' => $campaign_id,
            )
        );

        $http_header = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'Authorization: Bearer ' . $this->access->access_token,
        );

        $url = $this->access->instance_url . "/services/data/v40.0/sobjects/CampaignMember";

        // update
        $result = $this->getCurlWithHeaderPostfields($url, 1, $http_header, $data_string, null);

        return $result;

    }

    /**
     * remotePost
     * Execute a remote post.
     *
     * @param string $operation
     *   The type of webform submission operation to be posted. Can be 'insert',
     *   'update', or 'delete'.
     *
     * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
     *   The webform submission to be posted.
     *
     * @return array|bool
     */
    protected function salesforceRemotePost($webform_submission)
    {

        if (!$this->access) {
            return false;
        }

        $config = \Drupal::config('cud.settings');

        // re-align with api fields
        $request_post_data = $this->salesforceFinalMapWebToApi($webform_submission);

        if (!$request_post_data) {
            return false;
        }

        // semicolon sv array values
        // salesforce dislikes array/json
        // on passing thru api
        foreach ($request_post_data as $k => $v) {

            if (is_array($v)) {
                $request_post_data[$k] = implode(';', $v);
            }
        }

        \Drupal::logger('WebformPaymentController')->notice("request posted data: " . serialize($request_post_data));

        // get lead
        if (array_key_exists('Email', $request_post_data)) {

            $campaign_ids = null;

            if (array_key_exists('Campaign_ID', $request_post_data)) {

                $campaign_ids = $request_post_data["Campaign_ID"];

                // remove the campaign
                // and don't push it along with data
                unset($request_post_data["Campaign_ID"]);
            }

            $lead = $this->salesforceGetLead($request_post_data['Email']);

            if (json_decode($lead)->totalSize > 0) {

                $data_string = json_encode($request_post_data);

                try {

                    if ($config->get('webform_salesforce')) {
                       $result = $this->salesforceAddUpdateLead($data_string, json_decode($lead)->records[0]->attributes->url, "Update");

                       \Drupal::logger('WebformPaymentController')->notice("Final Result: Update Lead: " . serialize($result));
                    }

                } catch (RequestException $request_exception) {

                    \Drupal::logger('WebformPaymentController')->notice("Non-Error: UPDATE Lead: " . serialize($request_exception));

                }

            } else {

                $request_post_data["Status"] = "New";

                $data_string = json_encode($request_post_data);

                try {

                    if ($config->get('webform_salesforce')) {
                        $result = $this->salesforceAddUpdateLead($data_string, "/services/data/v20.0/sobjects/Lead/", "New");

                        \Drupal::logger('WebformPaymentController')->notice("Non-Error: NEW Lead: " . serialize($result));
                    }

                } catch (RequestException $request_exception) {

                    \Drupal::logger('WebformPaymentController')->notice("ERROR: NEW Lead: " . serialize($request_exception));

                }
            }

            // try getting the lead
            // again
            $lead = $this->salesforceGetLead($request_post_data['Email']);

            if ($lead && $campaign_ids && (json_decode($lead)->totalSize > 0)) {

                $lead_id = json_decode($lead)->records[0]->Id;

                $arr_campaign_id = explode(";", $campaign_ids);

                foreach ($arr_campaign_id as $c) {

                    $member = $this->salesforceGetLeadMembership($lead_id, $c);

                    if ($member) {

                        if (json_decode($member)->totalSize > 0) {
                            // don't do anything
                            // already a member
                            continue;
                        }

                        if ($config->get('webform_salesforce')) {
                            $member = $this->salesforceAddUpdateLeadMembership($lead_id, $c);
                        }
                    }
                }
            }
        }

        \Drupal::logger('WebformPaymentController')->notice("final request post data: " . serialize($request_post_data));

        return true;
    }

    /**
     * getCurlWithHeaderPostfields
     * Custom Curl Posting with tweaks for properly submitting
     * to Salesforce endponts
     *
     *
     * @param $url
     *
     * @param int $post_type
     *
     * @param null $http_header
     *
     * @param null $post_fields
     *
     * @param null $operation
     *
     * @return bool|mixed|string
     */
    public function getCurlWithHeaderPostfields($url, $post_type = 0, $http_header = null, $post_fields = null, $operation = null)
    {

        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            /* get 0, post 1 */
            curl_setopt($ch, CURLOPT_POST, $post_type);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            if ($operation == "Update") {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
            }

            if ($http_header != null) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
            }

            if ($post_fields != null) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
            }

            $result = curl_exec($ch);

            $error = curl_error($ch);

            if (!$error) {
                // \Drupal::logger("CudHelperSalesforce")->notice("Successful using curl_init");
                return $result;
            } else {
                \Drupal::logger("CudHelperSalesforce")->notice("Error using curl_init: " . serialize($error));
                return false;
            }

        } else {
            // \Drupal::logger("CudHelperSalesforce")->notice("Using file_get_contents");

            return file_get_contents($url);
        }

    }

    function generateQueryString($webform_submission) {

        $query_string = array();

        foreach( $webform_submission as $key => $value ){

            $query_string[] = urlencode( $key ) . '=' . urlencode( $value );

        }

        return implode( '&', $query_string );
    }

}