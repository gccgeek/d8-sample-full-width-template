<?php
/**
 * @file
 * Contains \Drupal\webform_cud\Controller\WebformSalesforceSubscriptionStatusController
 */

namespace Drupal\webform_cud\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Class WebformSalesforceSubscriptionStatusController
 * @package Drupal\webform_cud\Controller
 *
 * Prepare for Direct Migs Payment
 * after webform submission
 * the redirect is added in the Migs Handler section
 *
 */
class WebformSalesforceSubscriptionStatusController extends ControllerBase
{
    protected $salesforceCommonCUDWebToAPIFields = [

        'email' => 'Email', // user
        'email_with_confirm' => 'Email', // user
        'first_name' => 'FirstName', // user
        'middle_name' => 'MiddleName', // user
        'last_name' => 'LastName', // user
        'phone' => 'Phone', // user
        'inquiry' => 'Inquiry__c',

        'campaign_id' => 'Campaign_ID', // hidden, pre-populated in form
        'lead_source' => 'LeadSource', // hidden, dynamic
        'origin_code_1' => 'ORIGIN_CODE_1__c', // hidden, dynamic
        'origin_code_2' => 'ORIGIN_CODE_2__c', // hidden, dynamic
    ];

    public function unsubscribe($webform_submission_id, $webform_submitted_email_encoded) {
       
        $webform_submission_id = filter_var($webform_submission_id, FILTER_VALIDATE_INT);

        if ($webform_submission_id) {
            
            $webform_submitted_data = $this->getWebformSubmissionData($webform_submission_id);

            if ($webform_submitted_data == false) {
                \Drupal::messenger()->addMessage(t('An error occurred and processing did not complete.'), 'error');
                $response = new LocalRedirectResponse("/unsubscribe");
                $response->send();
                return;
            }

            $ws_name = (empty($webform_submitted_data['name']['title'])) ? "" : ($webform_submitted_data['name']['title'] . " ");
            $ws_first_name = (empty($webform_submitted_data['name']['first'])) ? "" : ($webform_submitted_data['name']['first'] . " ");
            $ws_middle_name = (empty($webform_submitted_data['name']['middle'])) ? "" : ($webform_submitted_data['name']['middle'] . " ");
            $ws_last_name = (empty($webform_submitted_data['name']['last'])) ? "" : ($webform_submitted_data['name']['last'] . " ");

            $ws_name = $ws_name . $ws_first_name . $ws_middle_name . $ws_last_name;

            $this->processSalesforce($webform_submitted_data);

            $output['container'] =
           
            array(
                '#type' => 'container',
                '#attributes' => array(
                    'class' => '',
                ),
            );
    
            $str_table = "";
    
            if ($ws_name) {
                $str_table .= "<tr><td>Name</td><td>$ws_name</td></tr>";
            }
    
            if ($webform_submitted_data['email_with_confirm']) {
                $str_table .= "<tr><td>Email</td><td>" . $webform_submitted_data['email_with_confirm'] . "</td></tr>";
            }
    
            $output['container']['title'] =
            array(
                '#type' => 'markup',
                '#markup' => "<h3 class='mb-3 mx-auto text-center'>Removed</h3><p class='small mx-auto text-center'>The following details has been unsubscribed.</p><p class='small italic cud fw-100  mx-auto text-center'><strong>Note:</strong> You may re-register on all of our online forms, if you wish to get back to us.</p>",
            );
            
            $output['container']['table'] =
            array(
                '#type' => 'markup',
                '#markup' => "<div class='info'>
                   <table class='table table-bordered'>
                   <thead>
                        <tr class='table-active'>
                        <th scope='col'>Field</th>
                        <th scope='col'>Data</th>
                        </tr>
                    </thead>$str_table</table>
                   </div>",
            );
        
            
            $output['#cache'] = ['max-age' => 0];

            \Drupal::service('page_cache_kill_switch')->trigger();

            return $output;
        }

        \Drupal::messenger()->addMessage(t('An error occurred and processing did not complete.'), 'error');
        $response = new LocalRedirectResponse("/unsubscribe");
        $response->send();
        return;
    }
    
    /* webform */
    public function getWebformSubmissionData($sid = 0)
    {

        // Load submission using sid.
        /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
        $webform_submission = \Drupal\webform\Entity\WebformSubmission::load($sid);

        if ($webform_submission) {
            // Get submission data.
            $data = $webform_submission->getData();

            if ($data) {
                return $data;
            }

            return false;
        }

        return false;
    }

    /**
     * updateWebformSubmissionData
     * Update salesforce with the provided associative array pair
     * of key value
     *
     * @return void
     */
    public function updateWebformSubmissionData($sid, $custom_fields)
    {
        // Load submission using sid.
        /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
        $webform_submission = \Drupal\webform\Entity\WebformSubmission::load($sid);

        if ($webform_submission) {
            // Get submission data.
            $data = $webform_submission->getData();

            foreach ($custom_fields as $k => $v) {
                // Change submission data.
                $data[$k] = $v;
            }

            // Set submission data.
            $webform_submission->setData($data);

            // Save submission.
            $webform_submission->save();

            return true;
        }

        return false;
    }

    /* salesforce */
    /* helpers */

    /**
     * processSalesforce
     * Submit submission and payment info to Salesforce
     *
     * @return bool|mixed|void
     */
    protected function processSalesforce($webform_submission)
    {

        // we have data
        // then process it
        if ($webform_submission) {
            $this->salesforceAuthorize();

            if ($this->access) {

                $this->salesforceRemotePost($webform_submission);

            }
        }

    }

    /**
     * salesforceAuthorize
     * Get Salesforce authorization to be able the
     * Api connectivity
     *
     * @return bool|mixed|void
     */
    protected function salesforceAuthorize()
    {

        $config = \Drupal::config('cud.settings');

        if (!$config->get('webform_salesforce')) {

            \Drupal::logger('WebformSalesforceSubscriptionStatusController')->notice("CUD config settings MUST be checked to allow submission to Salesforce.");
            return;
        }

        $salesforce_token_url = $config->get('sales_force_token_url');

        if (empty($salesforce_token_url)) {

            \Drupal::logger('WebformSalesforceSubscriptionStatusController')->notice("CUD can't connect to Salesforce. Token URL missing.");
            return;

        }

        $sales_force_lead_endpoint_url = $config->get('sales_force_lead_endpoint_url');
        $salesforce_client_id = $config->get('client_id');
        $salesforce_grant_type = "password";
        $salesforce_redirect_url = $config->get('redirect_url');
        $salesforce_client_secret = $config->get('client_secret');
        $salesforce_client_username = $config->get('username');
        $salesforce_client_password = $config->get('password');

        if ($salesforce_redirect_url != "") {
            $oauthUrl = "$salesforce_token_url?grant_type=$salesforce_grant_type&client_id=$salesforce_client_id&redirect_uri=$salesforce_redirect_url&client_secret=$salesforce_client_secret&username=$salesforce_client_username&password=$salesforce_client_password";
        } else {
            $oauthUrl = "$salesforce_token_url?grant_type=$salesforce_grant_type&client_id=$salesforce_client_id&client_secret=$salesforce_client_secret&username=$salesforce_client_username&password=$salesforce_client_password";
        }

        if ($access = $this->getCurlWithHeaderPostfields($oauthUrl, 1)) {
            if (isset(json_decode($access)->access_token)) {

                $this->access = json_decode($access);

                return $this->access;
            }
        }

        return false;
    }

    /**
     * salesforceFinalMapWebToApi
     * Map the data for salesforce api posting
     *
     * @param array $request_post_data
     * @return array
     */
    protected function salesforceFinalMapWebToApi(array $request_post_data)
    {
        foreach ($request_post_data as $k => $v) {            
            if (is_array($v) && $k === 'name') {
                $request_post_data['first_name'] = $v['first'];
                $request_post_data['middle_name'] = $v['middle'];
                $request_post_data['last_name'] = $v['last'];
            }
        }

        foreach ($this->salesforceCommonCUDWebToAPIFields as $k => $v) {
            if (array_key_exists($k, $request_post_data)) {
                    $new_request_post_data[$v] = $request_post_data[$k];
            }
        }

        if (!array_key_exists('HasOptedOutOfEmail', $request_post_data) || empty($request_post_data['DoNotCall'])) {
            $new_request_post_data['HasOptedOutOfEmail'] = 1;
        }

        if (!array_key_exists('DoNotCall', $request_post_data) || empty($request_post_data['DoNotCall'])) {
            $new_request_post_data['DoNotCall'] = 1;
        }

        if (!array_key_exists('LeadSource', $request_post_data) || empty($request_post_data['LeadSource'])) {
            $new_request_post_data['LeadSource'] = \Drupal::request()->get('src') != null ? htmlentities(\Drupal::request()->get('src'), ENT_QUOTES) : "webform";
        }

        if (!array_key_exists('ORIGIN_CODE_1__c', $request_post_data) || empty($request_post_data['ORIGIN_CODE_1__c'])) {
            $new_request_post_data['ORIGIN_CODE_1__c'] = \Drupal::request()->get('o') != null ? htmlentities(\Drupal::request()->get('o'), ENT_QUOTES) : "main-site";
        }
        
        if (!array_key_exists('ORIGIN_CODE_2__c', $request_post_data) || empty($request_post_data['ORIGIN_CODE_2__c'])) {
            $new_request_post_data['ORIGIN_CODE_2__c'] = \Drupal::request()->get('so') != null ? htmlentities(\Drupal::request()->get('so'), ENT_QUOTES) : "";
        }

        return $new_request_post_data;
    }

    /**
     * salesforceGetLead
     * Get the Salesforce Lead
     *
     * @param null $email
     *  Email field value of Lead during submission process
     *
     * @return bool|mixed|string
     *  Lead object or false
     */
    public function salesforceGetLead($email = null)
    {

        if (!$this->access) {
            return false;
        }

        $result = \Drupal::httpClient()->get($this->access->instance_url . "/services/data/v40.0/query?q=SELECT+Id,Email+from+Lead+Where+Email='$email'", [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->access->access_token,
            ],
        ])->getBody()->getContents();

        return $result;

    }


    /**
     * salesforceGetLeadMemberships
     * Membership(s) of the Lead in Salesforce CampaignMember object
     *
     * @param null $lead_id
     *  Lead ID of the Salesforce Lead object
     *
     * @return bool|mixed|string
     *  CampaignMember object or false
     */
    public function salesforceGetLeadMemberships($lead_id = null, $campaign_id = null)
    {

        if (!$this->access) {
            return false;
        }

        $campaign_query = "/services/data/v40.0/query?q=SELECT+LeadId,CampaignId+from+CampaignMember+Where+LeadId='" . $lead_id . "'";

        if ($campaign_id) {

            $campaign_query = "/services/data/v40.0/query?q=SELECT+LeadId,CampaignId+from+CampaignMember+Where+LeadId='" . $lead_id . "'+AND+CampaignId='" . $campaign_id . "'";
        
        }

        $result = \Drupal::httpClient()->get($this->access->instance_url . $campaign_query, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->access->access_token,
            ],
        ])->getBody()->getContents();

        return $result;

    }

    /**
     * salesforceDeleteLeadMembershipUsingSQL
     * Delete membership of the Lead in Salesforce CampaignMember object
     *
     * @param null $lead_id
     *  Lead ID of the Salesforce Lead object
     *
     * @return bool|mixed|string
     *  CampaignMember object or false
     */
    public function salesforceDeleteLeadMembershipUsingSQL($lead_id = null, $campaign_id = null)
    {
        if (!$this->access) {
            return false;
        }

        if ($lead_id && $campaign_id) {

            // $query = "Delete [ Select Id  from CampaignMember Where LeadId='$lead_id' AND CampaignId='$campaign_id' ];";

            $query = "Select Id  from CampaignMember Where LeadId='$lead_id' AND CampaignId='$campaign_id'";


            $campaign_query = "/services/data/v40.0/query?q=". urlencode($query);
        
            $campaign_member = \Drupal::httpClient()->get($this->access->instance_url . $campaign_query, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->access->access_token,
                ],
            ])->getBody()->getContents();
    
            if (json_decode($campaign_member)->totalSize > 0) {

                /* $http_header = array(
                    'Authorization' => 'Bearer ' . $this->access->access_token,
                );
        
                $url = json_decode($campaign_member)->records[0]->attributes->url;
                
                 // delete
                $result = $this->getCurlWithHeaderPostfields($this->access->instance_url . $url, 0, $http_header, null, "Delete");
            
                return $result; */

                $curl = curl_init($this->access->instance_url . json_decode($campaign_member)->records[0]->attributes->url);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $this->access->access_token));
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_exec($curl);

                $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                if ( $status != 204 ) {
                    \Drupal::logger("WebformSalesforceSubscriptionStatusController")->notice("Error: call to URL $url failed with status $status, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
                }
                
                curl_close($curl);

                return $curl;
            }
        }

        return null;
    }

    /**
     * salesforceAddUpdateLead
     * Updates the Salesforce object in relation to the mapped submitted key=>value pairs data string
     *
     * @param null $data_string
     *  key => value pair; using the API REST / SOAP object and endpoints
     *
     * @param null $object_url
     *  the endpoint (minus the instance url) for the CampaignMember object
     *
     * @param null $operation
     *  standard operation string, such as "update"
     *
     * @return bool|mixed|string
     *  Successful salesforce json formatted response object or false
     *
     */
    public function salesforceAddUpdateLead($data_string = null, $object_url = null, $operation = null)
    {

        if (!$this->access) {
            return false;
        }

        $http_header = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'Authorization: Bearer ' . $this->access->access_token,
        );

        $result = $this->getCurlWithHeaderPostfields($this->access->instance_url . $object_url, 0, $http_header, $data_string, $operation);

        return $result;

    }

    /**
     * salesforceAddUpdateLeadMembership
     * Updates the Salesforce CampaignMember object
     *
     * @param null $lead_id
     *  ID of the Lead object
     *
     * @param null $campaign_id
     *  ID of the Campaign object
     *
     * @return bool|mixed|string
     *  Successful salesforce json formatted response object or false
     *
     */
    public function salesforceUpdateLeadMembership($lead_id = null, $campaign_id = null, $operation = null)
    {

        if (!$this->access) {
            return false;
        }

        $data_string = json_encode(
            array(
                'LeadId' => $lead_id,
                'CampaignId' => $campaign_id,
            )
        );

        $http_header = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'Authorization: Bearer ' . $this->access->access_token,
        );

        $url = $this->access->instance_url . "/services/data/v40.0/sobjects/CampaignMember";

        // update
        $result = $this->getCurlWithHeaderPostfields($url, 1, $http_header, $data_string, $operation);

        return $result;

    }

    protected function salesforceAddContentNote($json_encoded_data = null)
    {

        if (!$this->access) {
            return false;
        }

        $http_header = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json_encoded_data),
            'Authorization: Bearer ' . $this->access->access_token,
        );
       
        try {

                $result = $this->getCurlWithHeaderPostfields($this->access->instance_url . "/services/data/v40.0/sobjects/ContentNote", 0, $http_header, $json_encoded_data, "New");

                return $result;

        } catch (RequestException $ex) {

                \Drupal::logger('WebformSalesforceSubscriptionStatusController')->notice("ERROR: NEW ContentNote Creation: " . serialize($ex));
        }
    
        return null;    
    }


    protected function salesforceAddContentDocumentLink($json_encoded_data = null)
    {

        if (!$this->access) {
            return false;
        }

        $http_header = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json_encoded_data),
            'Authorization: Bearer ' . $this->access->access_token,
        );
       
        try {

                $result = $this->getCurlWithHeaderPostfields($this->access->instance_url . "/services/data/v40.0/sobjects/ContentDocumentLink", 0, $http_header, $json_encoded_data, "New");

                return $result;

        } catch (RequestException $ex) {

                \Drupal::logger('WebformSalesforceSubscriptionStatusController')->notice("ERROR: NEW ContentNote Creation: " . serialize($ex));
        }
    
        return null;    
    }

    /**
     * remotePost
     * Execute a remote post.
     *
     * @param string $operation
     *   The type of webform submission operation to be posted. Can be 'insert',
     *   'update'
     *
     * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
     *   The webform submission to be posted.
     *
     * @return array|bool
     */
    protected function salesforceRemotePost($webform_submission)
    {

        if (!$this->access) {
            return false;
        }

        // re-align with api fields
        $request_post_data = $this->salesforceFinalMapWebToApi($webform_submission);

        // semicolon sv array values
        // salesforce dislikes array/json
        // on passing thru api
        foreach ($request_post_data as $k => $v) {

            if (is_array($v)) {
                $request_post_data[$k] = implode(';', $v);
            }
        }

        \Drupal::logger('WebformSalesforceSubscriptionStatusController')->notice("request posted data: " . serialize($request_post_data));

        // get lead
        if (array_key_exists('Email', $request_post_data)) {

            $campaign_ids = null;

            if (array_key_exists('Campaign_ID', $request_post_data)) {

                $campaign_ids = $request_post_data["Campaign_ID"];

                // remove the campaign
                // and don't push it along with data
                unset($request_post_data["Campaign_ID"]);
            }

            $lead = $this->salesforceGetLead($request_post_data['Email']);

            /* 
            Ignore: for visual purposes
                    there's no need to update/save personal info on salesforce
                    webform archive copy only
            */
            if (json_decode($lead)->totalSize > 0) {

                $data_string = json_encode($request_post_data);

                try {

                    $this->salesforceAddUpdateLead($data_string, json_decode($lead)->records[0]->attributes->url, "Update");

                } catch (RequestException $request_exception) {

                    \Drupal::logger('WebformSalesforceSubscriptionStatusController')->notice("ERROR: UPDATE Lead: " . serialize($request_exception));

                }

            }

            if ($lead && $campaign_ids && (json_decode($lead)->totalSize > 0)) {

                $lead_id = json_decode($lead)->records[0]->Id;
                $content = "";

                // get all memberships and remove the lead from it
                $arr_memberships = $this->salesforceGetLeadMemberships($lead_id);

                // remove all campaign memberships
                if ($arr_memberships) {
                    if (json_decode($arr_memberships)->totalSize > 0) {
                        $content .= "Remove: ";
                        foreach (json_decode($arr_memberships)->records as $c) {
                            $member = $this->salesforceDeleteLeadMembershipUsingSQL($lead_id, $c->CampaignId);

                            $content .= $c->CampaignId . " ";
                        }
                    }
                }

                // add on the campaign memberships provided during submission
                $arr_add_campaign_ids = explode(";", $campaign_ids);

                if ($arr_add_campaign_ids) 
                {
                    $content .= "Add: ";
                    foreach ($arr_add_campaign_ids as $c) {

                        $member = $this->salesforceGetLeadMemberships($lead_id, $c);
    
                        if ($member) {
    
                            if (json_decode($member)->totalSize > 0) {
                                // don't do anything
                                // already a member
                                continue;
                            }
    
                            $content .= $c . " ";
                            $member = $this->salesforceUpdateLeadMembership($lead_id, $c);
                        }
                    }
                }   

                // add note
                // note: add a ContentNote then after add ContentDocumentLink
                //      to associate it to the account
                if ($lead_id) {
                    $obj = new \stdClass();
                    $obj->Content = base64_encode($content);
                    $obj->Title = "Remove Campaigns. Add to --DO NOT CALL-- Campaign.";
                    
                    $content_notes = $this->salesforceAddContentNote(json_encode($obj));

                    if ($content_notes) {
                        if (json_decode($content_notes)->success == true) {
                            
                            $obj = new \stdClass();
                            $obj->ContentDocumentId = json_decode($content_notes)->id;
                            $obj->LinkedEntityId = $lead_id;
                            $obj->ShareType = "V";
                            
                            // attached to lead account
                            $this->salesforceAddContentDocumentLink(json_encode($obj));
                        }
                    }
                }
            }
        }

        \Drupal::logger('WebformSalesforceSubscriptionStatusController')->notice("final request post data: " . serialize($request_post_data));

        return true;
    }

    /**
     * getCurlWithHeaderPostfields
     * Custom Curl Posting with tweaks for properly submitting
     * to Salesforce endponts
     *
     *
     * @param $url
     *
     * @param int $post_type
     *
     * @param null $http_header
     *
     * @param null $post_fields
     *
     * @param null $operation
     *
     * @return bool|mixed|string
     */
    public function getCurlWithHeaderPostfields($url, $post_type = 0, $http_header = null, $post_fields = null, $operation = null)
    {

        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            
            /* get 0, post 1 */
            if ($post_type) {
                curl_setopt($ch, CURLOPT_POST, $post_type);
            }

            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            if ($operation == "Update") {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
            }

            if ($operation == "Delete") {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            }

            if ($http_header != null) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
            }

            if ($post_fields != null) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
            }

            $result = curl_exec($ch);

            $error = curl_error($ch);

            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            \Drupal::logger("WebformSalesforceSubscriptionStatusController")->notice($status);
            
            if ( $status != 204 ) {
                // die("Error: call to URL $url failed with status $status, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
                \Drupal::logger("WebformSalesforceSubscriptionStatusController")->notice("Error: call to URL $url failed with status $status, curl_error " . curl_error($ch) . ", curl_errno " . curl_errno($ch));
            }

            if (!$error) {
                // \Drupal::logger("WebformSalesforceSubscriptionStatusController")->notice("Successful using curl_init");
                return $result;
            } else {
                \Drupal::logger("WebformSalesforceSubscriptionStatusController")->notice("Error using curl_init: " . serialize($error));
                return false;
            }

        } else {
            // \Drupal::logger("WebformSalesforceSubscriptionStatusController")->notice("Using file_get_contents");

            return file_get_contents($url);
        }

    }

    function generateQueryString($webform_submission) {

        $query_string = array();

        foreach( $webform_submission as $key => $value ){

            $query_string[] = urlencode( $key ) . '=' . urlencode( $value );

        }

        return implode( '&', $query_string );
    }

}