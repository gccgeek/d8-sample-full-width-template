<?php
/**
 * @file
 * Contains \Drupal\webform_cud\Controller\WebformPaymentController.
 */

namespace Drupal\webform_cud\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_cud\lib\RijndaelOpenSSL;

/* use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Config\Entity\Query\Query;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node; */

/**
 * Class WebformPaymentController
 * @package Drupal\webform_cud\Controller
 *
 * Prepare for Direct Migs Payment
 * after webform submission
 * the redirect is added in the Migs Handler section
 *
 */
class WebformPaymentController extends ControllerBase
{
    // 'http://www.cud.ac.ae/international-summer-school/registration/pay/process';
    protected $cbd_whitelisted_return_url = "http://d8.appearance.loc/international-summer-school/registration/pay/process";

    // https://www.cbdonline.ae/Cybersource/Payment/InitiateCreditCardPayment
    protected $cbd_gateway = "https://test.cbdonline.ae/Cybersource/Payment/InitiateCreditCardPayment/";

     // TEST: CBD_43
     protected $cbd_gateway_client_id = "CBD_43";

      // TEST: CJ7HX15T9QLO7TI
    protected $cbd_gateway_live_encryption_key= "CJ7HX15T9QLO7TI";

    // keys are from webform
    // values are SalesforceFled Ids
    protected $salesforceCommonCUDWebToAPIFields = [
        'campaign_id' => 'Campaign_ID', // hidden, prepopulated in form
        'email' => 'Email', // user
        'first_name' => 'FirstName', // user
        'middle_name' => 'MiddleName', // user
        'last_name' => 'LastName', // user
        'phone' => 'Phone', // user
        'birthdate' => 'Date_of_Birth__c', //user
        'country' => 'Country_of_Residence__c', // user
//        'city' => '',
//        'address' => '',
        'united_arab_emirates_resident' => 'Is_UAE_Resident__c', // user, is uae resident
        'above_16' => 'Is_Above_16__c', // user, is above 16
        'passport_no' => 'Passport_Number__c', // user
        'expiry' => 'Passport_Expiry_Date__c', // user
//        'passport_image_copy' => '', // user
        'cbxs_morning_courses' => 'Morning_Courses__c',
        'cbxs_afternoon_courses' => 'Afternoon_Text__c',
        'agree_to_terms_and_conditions' => 'Agreed_to_Terms_and_Conditions__c', // user
        'emailoptout' => 'HasOptedOutOfEmail', // hidden, populated in form
        'lead_source' => 'LeadSource', // hidden, dynamic
        'origin_code_1' => 'ORIGIN_CODE_1__c', // hidden, dynamic
        'origin_code_2' => 'ORIGIN_CODE_2__c', // hidden, dynamic
        'programs' => 'Program_of_interest__c', // hidden, populated in form
        'payment_status' => "Payment_Status__c", // populated after payment
        'payment_order_id' => "Payment_Order_ID__c", // populated after payment
        'payment_amount' => "Payment_Amount__c", // populated after payment
        'payment_response' => "Bank_Response__c", // populated after payment
    ];
    
    /********************************************************/
    /********************************************************/
    /*******************PROCESS******************************/
    /********************************************************/
    /********************************************************/
    public function paymentredirect($str_id = null)
    {
        $rijndael = new RijndaelOpenSSL();

        $str_id = str_replace(array('PPPPP'), array('%'), $str_id);
        $unhash = $rijndael->decrypt(urldecode($str_id), "117781CUD117781");

        if ($unhash != true) {
            $response = new LocalRedirectResponse("/international-summer-school/registration/form");
            $response->send();
            return;
        }

        $webform_submitted_data = $this->getWebformSubmissionData($unhash);

        if ($webform_submitted_data == false) {
            $response = new LocalRedirectResponse("/international-summer-school/registration/form");
            $response->send();
            return;
        }

        // submit to salesforce
        $this->processSalesforce($webform_submitted_data);
                
        $orderid = 'SUS-' . $unhash;

        // set to
        // the highest amount that can be paid
        // for summer school
        $amount = 5940;

        if ((int) $webform_submitted_data['registration_1']) {
            $amount = 150; // register only. resident.
        }

        if ((int) $webform_submitted_data['registration_2']) {
            $amount += 2820; // full. resident.
        }

        if ((int) $webform_submitted_data['registration_3']) {
            $amount = 300; // register only.
        }

        if ((int) $webform_submitted_data['registration_4']) {
            $amount += 5640; // full.
        }

        $ws_name = (empty($webform_submitted_data['name']['title'])) ? "" : ($webform_submitted_data['name']['title'] . " ");
        $ws_first_name = (empty($webform_submitted_data['name']['first'])) ? "" : ($webform_submitted_data['name']['first'] . " ");
        $ws_middle_name = (empty($webform_submitted_data['name']['middle'])) ? "" : ($webform_submitted_data['name']['middle'] . " ");
        $ws_last_name = (empty($webform_submitted_data['name']['last'])) ? "" : ($webform_submitted_data['name']['last'] . " ");

        $ws_name = $ws_name . $ws_first_name . $ws_middle_name . $ws_last_name;

        // redirect URL
        // needs to be whitelisted by the CBD Bank Gateway
        // done by the bank's end
        $config = \Drupal::config('cud.settings');

        if ($config->get('migs_gateway_client_id') && $config->get('migs_gateway_client_id') !== "") {
            $this->cbd_gateway_client_id = $config->get('migs_gateway_client_id');
        }

        $data = '<BankInformation>';
        $data .= '<ClientID>'. $this->cbd_gateway_client_id. '</ClientID>';

        if ($config->get('migs_iss_whitelisted_url') && $config->get('migs_iss_whitelisted_url') !== "") {
            $this->cbd_whitelisted_return_url = $config->get('migs_iss_whitelisted_url');
        }

        $data .= '<ReturnPage>' . $this->cbd_whitelisted_return_url . '</ReturnPage>';

        $data .= '<CreateToken>false</CreateToken>';
        $data .= '<locale>en-us</locale>';
        $data .= '<PaymentInformation>';
        $data .= '<OrderID>' . (string) $orderid . '</OrderID>';
        $data .= '<TotalAmount>' . $amount . '</TotalAmount>';
        $data .= '<TransactionType>sale</TransactionType>';
        $data .= '<OrderDescription>Summer School</OrderDescription>';
        $data .= '<Currency>AED</Currency>';
        $data .= '</PaymentInformation>';
        // $data .= '<CustomerInformation>';
        // $data .= '<CustomerFirstname>' . $ws_first_name . '</CustomerFirstname>';
        // $data .= '<CustomerLastname>' . $ws_last_name . '</CustomerLastname>';
        // $data .= '<CustomerEmail>' . $webform_submitted_data['email'] . '</CustomerEmail>';

        // $accepted_phone_string = str_replace(' ', '', $webform_submitted_data['phone']);
        // $accepted_phone_string = str_replace('+', '00', $accepted_phone_string);

        // $data .= '<CustomerPhone>' . $accepted_phone_string . '</CustomerPhone>';
        // $data .= '<BillCity>' . $webform_submitted_data['city'] . '</BillCity>';
        // $data .= '<BillAddress>' . $webform_submitted_data['address'] . '</BillAddress>';
        // $data .= '<BillCountry>' . $webform_submitted_data['country'] . '</BillCountry>';
        // $data .= '</CustomerInformation>';
        $data .= '</BankInformation>';

        if ($config->get('migs_gateway_live_encryption_key') && $config->get('migs_gateway_live_encryption_key') !== "") {
            $this->cbd_gateway_live_encryption_key = $config->get('migs_gateway_live_encryption_key');
        }

        $chars = $rijndael->encrypt($data, $this->cbd_gateway_live_encryption_key);
        $decrypted_data = $rijndael->decrypt($chars, $this->cbd_gateway_live_encryption_key);

        $ws_agree_to_terms_and_conditions = (int) $webform_submitted_data['agree_to_terms_and_conditions'];
        
        if ($config->get('migs_gateway') && $config->get('migs_gateway') !== "") {
            $this->cbd_gateway = $config->get('migs_gateway');
        }

        $migs_result_form = \Drupal::formBuilder()->getForm('Drupal\webform_cud\Form\SubmitToMigsForm', $chars, $this->cbd_gateway, $ws_agree_to_terms_and_conditions);

        // just another form
        $banktransfer_result_form = \Drupal::formBuilder()->getForm('Drupal\webform_cud\Form\SubmitToDirectRequestBankTransfer', $orderid, $ws_agree_to_terms_and_conditions);

        $output['prefix'] = array(
            '#type' => 'markup',
            '#prefix' => '<div class="container">',
        );

        $output['container'] =
        array(
            '#type' => 'container',
            '#attributes' => array(
                'class' => 'container col-none',
            ),
        );

        $str_table = "";

        if ($orderid) {
            $str_table .= "<tr><td>Registration Number</td><td>$orderid</td></tr>";
        }

        if ($amount) {
            $str_table .= "<tr><td>Registration Amount</td><td>".(string)number_format($amount,2,".",",")."</td></tr>";
        }

        if ($ws_name) {
            $str_table .= "<tr><td>Name</td><td>$ws_name</td></tr>";
        }

        if ($webform_submitted_data['email']) {
            $str_table .= "<tr><td>Email</td><td>" . $webform_submitted_data['email'] . "</td></tr>";
        }

        if ($webform_submitted_data['phone']) {
            $str_table .= "<tr><td>Phone</td><td>" . $webform_submitted_data['phone'] . "</td></tr>";
        }

        if ($webform_submitted_data['country']) {
            $str_table .= "<tr><td>Country</td><td>" . $webform_submitted_data['country'] . "</td></tr>";
        }

        // if ($webform_submitted_data['city']) {
        //     $str_table .= "<tr><td>City</td><td>" . $webform_submitted_data['city'] . "</td></tr>";
        // }

        // if ($webform_submitted_data['address']) {
        //    $str_table .= "<tr><td>Address</td><td>" . $webform_submitted_data['address'] . "</td></tr>";
        // }

        if ($webform_submitted_data['united_arab_emirates_resident']) {
            $str_table .= "<tr><td>Is UAE Resident?</td><td>" . ($webform_submitted_data['united_arab_emirates_resident'] ? "Yes" : "") . "</td></tr>";
        }

        if ($webform_submitted_data['passport_no']) {
            $str_table .= "<tr><td>Passport No.</td><td>" . $webform_submitted_data['passport_no'] . "</td></tr>";
        }

        if ($webform_submitted_data['expiry']) {
            $str_table .= "<tr><td>Expiry</td><td>" . $webform_submitted_data['expiry'] . "</td></tr>";
        }

        if ($webform_submitted_data['passport_image_copy']) {
            $str_table .= "<tr><td>Is Passport Copy Attached?</td><td>" . ($webform_submitted_data['passport_image_copy'] ? "Attached" : "") . "</td></tr>";
        }

        if (isset($webform_submitted_data['cbxs_morning_courses'])) {
            $str_table .= "<tr><td>Morning Courses</td><td>" . implode(', ', $webform_submitted_data['cbxs_morning_courses']) . "</td></tr>";
        }

        if (isset($webform_submitted_data['cbxs_afternoon_courses'])) {
            $str_table .= "<tr><td>Afternoon Courses</td><td>" . implode(', ', $webform_submitted_data['cbxs_afternoon_courses']) . "</td></tr>";
        }

        if (!isset($webform_submitted_data['cbxs_morning_courses']) && !isset($webform_submitted_data['cbxs_afternoon_courses'])) {
            $str_table .= "<tr><td>Courses</td><td>You haven't pre-selected morning or afternoon classes.<br /><span class='small'>After registration, our customer service will keep in touch to help you with the selection process.</span></td></tr>";
        }

        $output['container']['title'] =
        array(
            '#type' => 'markup',
            '#markup' => "<h3 class='medium-title'>Review and Pay</h3>",
        );

        $output['container']['table'] =
        array(
            '#type' => 'markup',
            '#markup' => "<div class='info'>
               <table class='table table-bordered'>
               <thead>
                    <tr class='table-active'>
                    <th scope='col'>Field</th>
                    <th scope='col'>Data</th>
                    </tr>
                </thead>$str_table</table>
               </div>",
        );
        
        $output['container']['form'] = $migs_result_form;

        $output['container']['divider'] = array(
            array(
                '#type' => 'markup',
                '#markup' => "<h2 class='line-behind no-span text-align-center mt-3 pt-3 mb-3 pb-3 w-100'>OR</h2>",
            ),        
        );

        $output['container']['banktransfer'] = $banktransfer_result_form;

        

        $output['suffix'] = array(
            '#type' => 'markup',
            '#suffix' => '</div>',
        );

        $output['#cache'] = ['max-age' => 0];

        \Drupal::service('page_cache_kill_switch')->trigger();

        return $output;
    }

    /********************************************************/
    /********************************************************/
    /*******************PROCESS******************************/
    /********************************************************/
    /********************************************************/
    public function paymentprocess()
    {

        \Drupal::service('page_cache_kill_switch')->trigger();

        // encode xmls chars
        // and gateway
        $config = \Drupal::config('cud.settings');

        if ($config->get('migs_gateway_live_encryption_key') && $config->get('migs_gateway_live_encryption_key') !== "") {
            $this->cbd_gateway_live_encryption_key = $config->get('migs_gateway_live_encryption_key');
        }
                
        $rijndael = new RijndaelOpenSSL();

        $encrypted_data = $_POST['c'];

        $decrypted_data = $rijndael->decrypt($encrypted_data, $this->cbd_gateway_live_encryption_key);

        $str_decrypted_data = (string) $decrypted_data;

        $str_payment_status = "";
        $payment_status = -1;

        $payment_CBDReferenceNo = "";
        $payment_CCReferenceNo = "";
        $payment_OrderID = "";
        $payment_Amount = "";
        $payment_PaymentDate = "";
        $payment_PaymentTime = "";

        $xmlobj = simplexml_load_string($str_decrypted_data);

        if (is_object($xmlobj)) {
            if (property_exists($xmlobj, 'Header')) {
                if (property_exists($xmlobj->Header, 'ResponseCode')) {
                    if ((string) $xmlobj->Header->ResponseCode === "00") {
                        $str_payment_status = (string) $xmlobj->Header->ResponseMsg;
                        $payment_status = (int) $xmlobj->Header->ResponseCode;
                    } else {
                        $str_payment_status = (string) $xmlobj->Header->ResponseMsg;
                        $payment_status = (int) $xmlobj->Header->ResponseCode;
                    }
                }
            }
            if (property_exists($xmlobj, 'Body')) {
                if (property_exists($xmlobj->Body, 'PaymentInformation')) {
                    if (property_exists($xmlobj->Body->PaymentInformation, 'CBDReferenceNo')) {
                        $payment_CBDReferenceNo = (string) $xmlobj->Body->PaymentInformation->CBDReferenceNo;
                    }
                    if (property_exists($xmlobj->Body->PaymentInformation, 'CCReferenceNo')) {
                        $payment_CCReferenceNo = (string) $xmlobj->Body->PaymentInformation->CCReferenceNo;
                    }
                    if (property_exists($xmlobj->Body->PaymentInformation, 'OrderID')) {
                        $payment_OrderID = (string) $xmlobj->Body->PaymentInformation->OrderID;
                    }
                    if (property_exists($xmlobj->Body->PaymentInformation, 'Amount')) {
                        $payment_Amount = (string) $xmlobj->Body->PaymentInformation->Amount;
                    }
                    if (property_exists($xmlobj->Body->PaymentInformation, 'PaymentDate')) {
                        $payment_PaymentDate = (string) $xmlobj->Body->PaymentInformation->PaymentDate;
                    }
                    if (property_exists($xmlobj->Body->PaymentInformation, 'PaymentTime')) {
                        $payment_PaymentTime = (string) $xmlobj->Body->PaymentInformation->PaymentTime;
                    }

                }
            }
        }

        $output['prefix'] = array(
            '#type' => 'markup',
            '#prefix' => '<div class="container">',
        );

        $output['container'] =
        array(
            '#type' => 'container',
            '#attributes' => array(
                'class' => 'container col-none',
            ),
        );

        $output['title']['html'] =
        array(
            '#type' => 'markup',
            '#markup' => "",
        );

        if ($payment_status == 0) {
            $output['title']['html']['status'] =
            array(
                '#type' => 'markup',
                '#markup' => "<div class='position-relative w-25 alert alert-success m-auto'><h1 class='text-align-center'><i class='fa fa-check-circle'><!-- empty --></i></h1><h4 class='text-align-center  mb-0'><strong>" . ucwords($str_payment_status) . "</strong></h4></div><div class='description text-align-center mt-3 pt-3 mb-3'><h5>Registration and Payment successful.</h5><p class='mt-3 mb-3'><strong>You will be receiving receipt and Summer School School information.<br />Check your email.<br /><br />Go back to <a href='/international-summer-school/registration'>Summer School</a> pages</strong></p></div>",
            );
        } else {
            $output['title']['html']['status_red'] =
            array(
                '#type' => 'markup',
                '#markup' => "<div class='position-relative alert w-75 alert-danger m-auto'><h1 class='text-align-center'><i class='fa fa-times-circle'><!-- empty --></i></h1><h4 class='text-align-center  mb-0'><strong>" . ucwords($str_payment_status) . "</strong></h4></div><div class='description text-align-center mt-3 pt-3 mb-3'><h5>Payment not successful.</h5><p class='mt-3 mb-3'><strong>Please retry or contact <a href='mailto:info@cud.ac.ae'>info@cud.ac.ae</a> and attach the registration information.<br />Check your email.<br /><br />Go back to <a href='/international-summer-school/registration'>Summer School</a> pages</strong></p></div>",
            );
        }

        $output['suffix'] = array(
            '#type' => 'markup',
            '#suffix' => '</div>',
        );

        $output['#cache'] = ['max-age' => 0];

        $submission_id = (int) str_replace("SUS-", "", $payment_OrderID);

        // TODO:// work on unsuccessful payment
        // push it to salesforce
        if (!empty($submission_id)) {

                $this->updateWebformSubmissionData($submission_id, array(
                    'payment_status' => $payment_status == 0 ? "PAID" : "UNSUCCESFUL",
                    'payment_order_id' => $payment_OrderID,
                    'payment_amount' => $payment_Amount,
                    'payment_response' => $str_decrypted_data,
                ));

                // update webform
                $webform_submission = $this->getWebformSubmissionData($submission_id);

                if ($webform_submission) {
                    // processLog();
                    $this->processSalesforce($webform_submission);
                }
        }

        return $output;
    }

    /********************************************************/
    /********************************************************/
    /*******************PROCESS******************************/
    /********************************************************/
    /********************************************************/
    public function paybybanktransferprocess()
    {
        $id = $_POST['id'];

        $submission_id = (int) str_replace("SUS-", "", $id);

        if (!empty($submission_id)) {

            $this->updateWebformSubmissionData($submission_id, array(
                'payment_status' => "BANKTRANSFER",
                'payment_order_id' => $id,
                'payment_amount' => "NONE",
                'payment_response' => "NONE",
            ));

            // update webform
            $webform_submission = $this->getWebformSubmissionData($submission_id);

            if ($webform_submission) {
                // processLog();
                $this->processSalesforce($webform_submission);
            }
        }

        $output['prefix'] = array(
            '#type' => 'markup',
            '#prefix' => '<div class="container">',
        );

        $output['container'] =
        array(
            '#type' => 'container',
            '#attributes' => array(
                'class' => 'container col-none',
            ),
        );

        $output['title']['html'] =
        array(
            '#type' => 'markup',
            '#markup' => "",
        );

        $output['title']['html']['status'] =
            array(
                '#type' => 'markup',
                '#markup' => "<div class='position-relative w-25 alert alert-success m-auto'><h1 class='text-align-center'><i class='fa fa-check-circle'><!-- empty --></i></h1><h4 class='text-align-center  mb-0'><strong>Registration Submitted</strong></h4></div><div class='description text-align-center mt-3 pt-3 mb-3'><h5>Registration successful.</h5><p class='mt-3 mb-3'><strong>You will be receiving a registration information</br />Please use the Registration Number in the Bank Transfer.<br />Check your email.<br /><br />Go back to <a href='/international-summer-school/registration'>Summer School</a> pages</strong></p></div>",
            );

        $output['suffix'] = array(
            '#type' => 'markup',
            '#suffix' => '</div>',
        );

        return $output;
    }

    /* webform */
    public function getWebformSubmissionData($sid = 0)
    {

        // Load submission using sid.
        /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
        $webform_submission = \Drupal\webform\Entity\WebformSubmission::load($sid);

        if ($webform_submission) {
            // Get submission data.
            $data = $webform_submission->getData();

            if ($data) {
                return $data;
            }

            return false;
        }

        return false;
    }

    /**
     * updateWebformSubmissionData
     * Update salesforce with the provided associative array pair
     * of key value
     *
     * @return void
     */
    public function updateWebformSubmissionData($sid, $custom_fields)
    {
        // Load submission using sid.
        /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
        $webform_submission = \Drupal\webform\Entity\WebformSubmission::load($sid);

        if ($webform_submission) {
            // Get submission data.
            $data = $webform_submission->getData();

            foreach ($custom_fields as $k => $v) {
                // Change submission data.
                $data[$k] = $v;
            }

            // Set submission data.
            $webform_submission->setData($data);

            // Save submission.
            $webform_submission->save();

            return true;
        }

        return false;
    }

    /* salesforce */
    /* helpers */

    /**
     * processSalesforce
     * Submit submission and payment info to Salesforce
     *
     * @return bool|mixed|void
     */
    protected function processSalesforce($webform_submission)
    {

        // we have data
        // then process it
        if ($webform_submission) {
            $this->salesforceAuthorize();

            if ($this->access) {

                $this->salesforceRemotePost($webform_submission);

            }
        }

    }

    /**
     * salesforceAuthorize
     * Get Salesforce authorization to be able the
     * Api connectivity
     *
     * @return bool|mixed|void
     */
    protected function salesforceAuthorize()
    {

        $config = \Drupal::config('cud.settings');

        if (!$config->get('webform_salesforce')) {

            \Drupal::logger('WebformPaymentController')->notice("CUD config settings MUST be checked to allow submission to Salesforce.");

            return;
        }

        $salesforce_token_url = $config->get('sales_force_token_url');

        if (empty($salesforce_token_url)) {
            \Drupal::logger('WebformPaymentController')->notice("CUD can't connect to Salesforce. Token URL missing.");
            return;
        }

        $sales_force_lead_endpoint_url = $config->get('sales_force_lead_endpoint_url');
        $salesforce_client_id = $config->get('client_id');
        $salesforce_grant_type = "password";
        $salesforce_redirect_url = $config->get('redirect_url');
        $salesforce_client_secret = $config->get('client_secret');
        $salesforce_client_username = $config->get('username');
        $salesforce_client_password = $config->get('password');

        if ($salesforce_redirect_url != "") {
            $oauthUrl = "$salesforce_token_url?grant_type=$salesforce_grant_type&client_id=$salesforce_client_id&redirect_uri=$salesforce_redirect_url&client_secret=$salesforce_client_secret&username=$salesforce_client_username&password=$salesforce_client_password";
        } else {
            $oauthUrl = "$salesforce_token_url?grant_type=$salesforce_grant_type&client_id=$salesforce_client_id&client_secret=$salesforce_client_secret&username=$salesforce_client_username&password=$salesforce_client_password";
        }

        if ($access = $this->getCurlWithHeaderPostfields($oauthUrl, 1)) {
            if (isset(json_decode($access)->access_token)) {

                $this->access = json_decode($access);

                return $this->access;
            }
        }

        return false;
    }

    /**
     * salesforceFinalMapWebToApi
     * Map the data for salesforce api posting
     *
     * @param array $request_post_data
     * @return array
     */
    protected function salesforceFinalMapWebToApi(array $request_post_data)
    {

        // date: 1999-01-01
        // checkbox: true
        foreach ($request_post_data as $k => $v) {
            
            if ($k === 'united_arab_emirates_resident') 
                $request_post_data['united_arab_emirates_resident'] = ((int)$request_post_data['united_arab_emirates_resident'] >= 1) ? 1 : 0;

            if ($k === 'above_16') 
                $request_post_data['above_16'] = ((int)$request_post_data['above_16'] >= 1) ? 1 : 0;
            
            if ($k === 'agree_to_terms_and_conditions') 
                $request_post_data['agree_to_terms_and_conditions'] = ((int)$request_post_data['agree_to_terms_and_conditions'] >= 1) ? 1 : 0;

            if ($k === 'emailoptout') 
                $request_post_data['emailoptout'] = ((int)$request_post_data['emailoptout'] >= 1) ? 1 : 0;

            if ($k === 'birthdate') 
                $request_post_data['birthdate'] = date("Y-m-d", strtotime($request_post_data['birthdate']));

            if ($k === 'expiry') 
                $request_post_data['expiry'] = date("Y-m-d", strtotime($request_post_data['expiry']));

            if (is_array($v) && $k === 'name') {
                $request_post_data['first_name'] = $v['first'];
                $request_post_data['middle_name'] = $v['middle'];
                $request_post_data['last_name'] = $v['last'];
            }
        }

        foreach ($this->salesforceCommonCUDWebToAPIFields as $k => $v) {
            if (array_key_exists($k, $request_post_data)) {
                    $new_request_post_data[$v] = $request_post_data[$k];
            }
        }

        if (!array_key_exists('LeadSource', $request_post_data) || empty($request_post_data['LeadSource'])) {
            $new_request_post_data['LeadSource'] = \Drupal::request()->get('src') != null ? htmlentities(\Drupal::request()->get('src'), ENT_QUOTES) : "webform";
        }
        if (!array_key_exists('ORIGIN_CODE_1__c', $request_post_data) || empty($request_post_data['ORIGIN_CODE_1__c'])) {
            $new_request_post_data['ORIGIN_CODE_1__c'] = \Drupal::request()->get('o') != null ? htmlentities(\Drupal::request()->get('o'), ENT_QUOTES) : "main-site";
        }
        if (!array_key_exists('ORIGIN_CODE_2__c', $request_post_data) || empty($request_post_data['ORIGIN_CODE_2__c'])) {
            $new_request_post_data['ORIGIN_CODE_2__c'] = \Drupal::request()->get('so') != null ? htmlentities(\Drupal::request()->get('so'), ENT_QUOTES) : "";
        }

        return $new_request_post_data;
    }

    /**
     * salesforceGetLead
     * Get the Salesforce Lead
     *
     * @param null $email
     *  Email field value of Lead during submission process
     *
     * @return bool|mixed|string
     *  Lead object or false
     */
    public function salesforceGetLead($email = null)
    {

        if (!$this->access) {
            return false;
        }

        $result = \Drupal::httpClient()->get($this->access->instance_url . "/services/data/v40.0/query?q=SELECT+Id,Email+from+Lead+Where+Email='$email'", [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->access->access_token,
            ],
        ])->getBody()->getContents();

        return $result;

    }

    /**
     * salesforceGetLeadMembership
     * Membership of the Lead in Salesforce CampaignMember object
     *
     * @param null $lead_id
     *  Lead ID of the Salesforce Lead object
     *
     * @return bool|mixed|string
     *  CampaignMember object or false
     */
    public function salesforceGetLeadMembership($lead_id = null, $campaign_id = null)
    {

        if (!$this->access) {
            return false;
        }

        $result = \Drupal::httpClient()->get($this->access->instance_url . "/services/data/v40.0/query?q=SELECT+LeadId,CampaignId+from+CampaignMember+Where+LeadId='" . $lead_id . "'+AND+CampaignId='" . $campaign_id . "'", [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->access->access_token,
            ],
        ])->getBody()->getContents();

        return $result;

    }

    /**
     * salesforceAddUpdateLead
     * Updates the Salesforce object in relation to the mapped submitted key=>value pairs data string
     *
     * @param null $data_string
     *  key => value pair; using the API REST / SOAP object and endpoints
     *
     * @param null $object_url
     *  the endpoint (minus the instance url) for the CampaignMember object
     *
     * @param null $operation
     *  standard operation string, such as "update"
     *
     * @return bool|mixed|string
     *  Successful salesforce json formatted response object or false
     *
     */
    public function salesforceAddUpdateLead($data_string = null, $object_url = null, $operation = null)
    {

        if (!$this->access) {
            return false;
        }

        $http_header = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'Authorization: Bearer ' . $this->access->access_token,
        );

        $result = $this->getCurlWithHeaderPostfields($this->access->instance_url . $object_url, 0, $http_header, $data_string, $operation);

        return $result;

    }

    /**
     * salesforceAddUpdateLeadMembership
     * Updates the Salesforce CampaignMember object
     *
     * @param null $lead_id
     *  ID of the Lead object
     *
     * @param null $campaign_id
     *  ID of the Campaign object
     *
     * @return bool|mixed|string
     *  Successful salesforce json formatted response object or false
     *
     */
    public function salesforceAddUpdateLeadMembership($lead_id = null, $campaign_id = null)
    {

        if (!$this->access) {
            return false;
        }

        $data_string = json_encode(
            array(
                'LeadId' => $lead_id,
                'CampaignId' => $campaign_id,
            )
        );

        $http_header = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'Authorization: Bearer ' . $this->access->access_token,
        );

        $url = $this->access->instance_url . "/services/data/v40.0/sobjects/CampaignMember";

        // update
        $result = $this->getCurlWithHeaderPostfields($url, 1, $http_header, $data_string, null);

        return $result;

    }

    /**
     * remotePost
     * Execute a remote post.
     *
     * @param string $operation
     *   The type of webform submission operation to be posted. Can be 'insert',
     *   'update', or 'delete'.
     *
     * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
     *   The webform submission to be posted.
     *
     * @return array|bool
     */
    protected function salesforceRemotePost($webform_submission)
    {

        if (!$this->access) {
            return false;
        }

        // re-align with api fields
        $request_post_data = $this->salesforceFinalMapWebToApi($webform_submission);

        // semicolon sv array values
        // salesforce dislikes array/json
        // on passing thru api
        foreach ($request_post_data as $k => $v) {

            if (is_array($v)) {
                $request_post_data[$k] = implode(';', $v);
            }
        }

        \Drupal::logger('WebformPaymentController')->notice("request posted data: " . serialize($request_post_data));

        // get lead
        if (array_key_exists('Email', $request_post_data)) {

            $campaign_ids = null;

            if (array_key_exists('Campaign_ID', $request_post_data)) {

                $campaign_ids = $request_post_data["Campaign_ID"];

                // remove the campaign
                // and don't push it along with data
                unset($request_post_data["Campaign_ID"]);
            }

            $lead = $this->salesforceGetLead($request_post_data['Email']);

            if (json_decode($lead)->totalSize > 0) {

                $data_string = json_encode($request_post_data);

                try {

                    $this->salesforceAddUpdateLead($data_string, json_decode($lead)->records[0]->attributes->url, "Update");

                } catch (RequestException $request_exception) {

                    \Drupal::logger('WebformPaymentController')->notice("ERROR: UPDATE Lead: " . serialize($request_exception));

                }

            } else {

                $request_post_data["Status"] = "New";

                $data_string = json_encode($request_post_data);

                try {

                    $result = $this->salesforceAddUpdateLead($data_string, "/services/data/v20.0/sobjects/Lead/", "New");

                } catch (RequestException $request_exception) {

                    \Drupal::logger('WebformPaymentController')->notice("ERROR: NEW Lead Creation: " . serialize($request_exception));

                }
            }

            // try getting the lead
            // again
            $lead = $this->salesforceGetLead($request_post_data['Email']);

            if ($lead && $campaign_ids && (json_decode($lead)->totalSize > 0)) {

                $lead_id = json_decode($lead)->records[0]->Id;

                $arr_campaign_id = explode(";", $campaign_ids);

                foreach ($arr_campaign_id as $c) {

                    $member = $this->salesforceGetLeadMembership($lead_id, $c);

                    if ($member) {

                        if (json_decode($member)->totalSize > 0) {
                            // don't do anything
                            // already a member
                            continue;
                        }

                        $member = $this->salesforceAddUpdateLeadMembership($lead_id, $c);
                    }
                }
            }
        }

        \Drupal::logger('WebformPaymentController')->notice("final request post data: " . serialize($request_post_data));

        return true;
    }

    /**
     * getCurlWithHeaderPostfields
     * Custom Curl Posting with tweaks for properly submitting
     * to Salesforce endponts
     *
     *
     * @param $url
     *
     * @param int $post_type
     *
     * @param null $http_header
     *
     * @param null $post_fields
     *
     * @param null $operation
     *
     * @return bool|mixed|string
     */
    public function getCurlWithHeaderPostfields($url, $post_type = 0, $http_header = null, $post_fields = null, $operation = null)
    {

        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            /* get 0, post 1 */
            curl_setopt($ch, CURLOPT_POST, $post_type);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            if ($operation == "Update") {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
            }

            if ($http_header != null) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
            }

            if ($post_fields != null) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
            }

            $result = curl_exec($ch);

            $error = curl_error($ch);

            if (!$error) {
                // \Drupal::logger("CudHelperSalesforce")->notice("Successful using curl_init");
                return $result;
            } else {
                \Drupal::logger("CudHelperSalesforce")->notice("Error using curl_init: " . serialize($error));
                return false;
            }

        } else {
            // \Drupal::logger("CudHelperSalesforce")->notice("Using file_get_contents");

            return file_get_contents($url);
        }

    }

    function generateQueryString($webform_submission) {

        $query_string = array();

        foreach( $webform_submission as $key => $value ){

            $query_string[] = urlencode( $key ) . '=' . urlencode( $value );

        }

        return implode( '&', $query_string );
    }

}