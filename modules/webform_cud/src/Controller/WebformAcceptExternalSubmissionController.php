<?php

/**
 * @file
 * Contains \Drupal\webform_cud\Controller\WebformAcceptExternalSubmissionController.
 */

namespace Drupal\webform_cud\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\LocalRedirectResponse;

use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformSubmissionForm;
use Drupal\webform\WebformSubmissionInterface;
use Laminas\Diactoros\Response\RedirectResponse;

/**
 * Class WebformPaymentController
 * @package Drupal\webform_cud\Controller
 *
 * Prepare for Direct Migs Payment
 * after webform submission
 * the redirect is added in the Migs Handler section
 *
 */
class WebformAcceptExternalSubmissionController extends ControllerBase
{

    public function save(){

        $return_url = \Drupal::request()->request->get('retURL');
        $oid = \Drupal::request()->request->get('oid');
        $campaign_id = \Drupal::request()->request->get('Campaign_ID');
        $expected_entrance = \Drupal::request()->request->get('00N58000009M50L');
        $lead_source = \Drupal::request()->request->get('lead_source');
        $lead_channel = \Drupal::request()->request->get('00N4H00000DrkuY');

        $first_name = t(\Drupal::request()->request->get('first_name'));
        $last_name = t(\Drupal::request()->request->get('last_name'));
        $email = t(\Drupal::request()->request->get('email'));
        $phone = t(\Drupal::request()->request->get('phone'));

        $program = t(\Drupal::request()->request->get('00N58000009M50F'));
        $category = t(\Drupal::request()->request->get('00N4H00000Drn0J'));
        $high_school_name = t(\Drupal::request()->request->get('00N58000009M50A'));
        $high_school_curriculum = t(\Drupal::request()->request->get('00N4H00000DrkoC'));

        // Example IDs
        $webform_id = 'gmfs_alumni';
        $webform = Webform::load($webform_id);

        // Create webform submission.
        $values = [
            'webform_id' => $webform->id(),
            'data' => [
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'phone' => $phone,
                'program' => $program,
                'category' => $category,
                'high_school_name' => $high_school_name,
                'high_school_curriculum' => $high_school_curriculum,
            ],
        ];

        /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
        $webform_submission = WebformSubmission::create($values);
        $webform_submission->save();

        /* $output['prefix'] = array(
            '#type' => 'markup',
            '#prefix' => '<div class="content-wrapper">',
        );

        $output['container']['hidden'] =
        array(
            '#type' => 'markup',
            '#markup' => "<div><span>return URL: $return_url</span><br />
            <span>oid: $oid</span><br />
            <span>campaign id: $campaign_id</span><br />
            <span>expected entrance: $expected_entrance</span><br />
            <span>lead source: $lead_source</span><br />
            <span>lead channel: $lead_channel</span><br /></div>",
        );

        $output['container']['input'] =
            array(
                '#type' => 'markup',
                '#markup' => "<div><span>return URL: $return_url</span><br />
            <span>oid: $oid</span><br />
            <span>campaign id: $campaign_id</span><br />
            <span>expected entrance: $expected_entrance</span><br />
            <span>lead source: $lead_source</span><br />
            <span>lead channel: $lead_channel</span><br /></div>",
        );

        $output['suffix'] = array(
            '#type' => 'markup',
            '#suffix' => '</div>',
        );

        $output['#cache'] = ['max-age' => 0]; */

        \Drupal::service('page_cache_kill_switch')->trigger();

        return new RedirectResponse("$return_url");

    }
}
