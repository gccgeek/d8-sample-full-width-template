<?php

namespace Drupal\webform_cud\Plugin\WebformHandler;

use Drupal\webform\Entity\Webform;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformInterface ;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Custom submit to Salesforce api
 *
 * @WebformHandler(
 *   id = "cud_salesforce",
 *   label = @Translation("CUD Salesforce api integration"),
 *   category = @Translation("CUD Salesforce"),
 *   description = @Translation("CUD Salesforce api integration using Webform handler plugin."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class CUDSalesforceWebformHandler extends WebformHandlerBase
{
    /**
     * {@inheritdoc}
     */
    public function alterElements(array &$elements, WebformInterface $webform)
    {

        $form_id = $webform->id();

        // main contact form
        if ($form_id == 'main_contact') {

            /* default values */
            $elements['interest']['programs']['#default_value'] = \Drupal::request()->get('program');

            // config
            $config = \Drupal::config('cud.settings');

            $json = $config->get('json_wheres');
            $obj = json_decode($json);

            if (!is_object($obj)) {
                $json = $this->get_curl_with_header_postfields($config->get('cams_url') . 'json_wheres.asp');
                $obj = json_decode($json);

            }

            $options = array();
            if (is_object($obj)) {

                $ctr_empty_succession = 0;
                foreach ($obj->wheres as $where => $where_detail) {
                    if (preg_match('/.*^\(Select Below\)$.*|^.*(--).*$/i', $where_detail)) {
                        $where_parent = $where_detail;
                        $options[$where_detail] = array();
                        $ctr_empty_succession = 0;
                    }


                    if ($where_detail != $where_parent && $where_parent != "") {

                        if (trim($where_detail) == "") {
                            $ctr_empty_succession++;
                            $where_detail = "None";
                        }

                        if ($ctr_empty_succession <= 1) {
                            array_push($options[$where_parent], $where_detail);
                        }

                    }
                }

                $children = array();
                $children['where_did_hear_label'] = array(
                    '#markup' => t('<h5>Where did you hear about us?</h5>'),
                );

                foreach ($options as $option => $detail) {

                    $fieldset_label = preg_replace('/\(Select Below\)/i', '', $option);
                    $fieldset_label = preg_replace('/[^\da-z\(\)\/\ \s+]/i', '', $fieldset_label);
                    $fieldset_name = preg_replace('/[^\da-z]/i', '', strtolower($fieldset_label));

                    $children[$fieldset_name] = array(
                        '#type' => 'fieldset',
                        '#title' => t($fieldset_label),
                        '#weight' => 50,
                        '#collapsible' => true,
                        '#collapsed' => true,
                        '#prefix' => '<div class="where-did-you-hear-about-us where-did-you-hear-about-us-'. $fieldset_name. '">',
                        '#suffix' => '</div>',
                        '#webform_id' => "main_contact--" . $fieldset_name,
                        '#webform_key' => $fieldset_name,
                        '#webform_parent_key' => '',
                    );

                    $children[$fieldset_name][$fieldset_name] = array(
                        '#type' => 'checkboxes',
                        '#options' => $detail,
                    );
                }

                /* add a checkbox */
                $children[$fieldset_name]['others_please_specify'] = array(
                    '#type' => "textarea",
                    '#title' => "others_please_specify",
                    '#webform_id' => "main_contact--others_please_specify",
                    '#webform_key' => "others_please_specify",
                    '#webform_parent_key' => '',
                    '#webform_parent_flexbox' => FALSE,
                    '#webform_depth' => 0,
                    '#webform_children' => array(),
                    '#admin_title' => NULL,
                );

                $elements['container_where_did_you_hear'][] = $children;
            }


        }
    }

    public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {
        // main contact form
        if ($form && ($form['#form_id'] == 'webform_submission_main_contact_form' ||  $form['#form_id'] == "webform_submission_main_contact_node_21421_form")) {
            if ($form_state) {
                // config
                $config = \Drupal::config('cud.settings');

                $json = $config->get('json_wheres');
                $obj = json_decode($json);

                if (!is_object($obj)) {
                    $json = $this->get_curl_with_header_postfields($config->get('cams_url') . 'json_wheres.asp');
                    $obj = json_decode($json);
                }

                $options = array();
                foreach ($obj->wheres as $where => $where_detail) {
                    if (preg_match('/.*^\(Select Below\)$.*|^.*(--).*$/i', $where_detail)) {
                        $where_parent = $where_detail;
                        $options[$where_detail] = array();
                        $ctr_empty_succession = 0;
                    }


                    if ($where_detail != $where_parent && $where_parent != "") {

                        if (trim($where_detail) == "") {
                            $ctr_empty_succession++;
                            $where_detail = "None";
                        }

                        if ($ctr_empty_succession <= 1) {
                            array_push($options[$where_parent], $where_detail);
                        }

                    }
                }

                $count_checked = 0;

                // cams where did you hear - same as form alter coding
                foreach ($options as $option => $detail) {
                    $fieldset_label = preg_replace('/\(Select Below\)/i', '', $option);
                    $fieldset_label = preg_replace('/[^\da-z\(\)\/\ \s+]/i', '', $fieldset_label);
                    $fieldset_name = preg_replace('/[^\da-z]/i', '', strtolower($fieldset_label));

                    if ($form_state->getValue($fieldset_name)) {
                        foreach ($form_state->getValue($fieldset_name) as $k => $v) {

                            // looping indicates checked checkbox
                            $count_checked++;

                        }
                    }
                }

                if ($count_checked <= 0) {
                    $form_state->setErrorByName('where_did_you_hear_about_us', t("Please check at least one 'Where did you hear about us'"));
                }
            }
        }
    }

    public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {
        /* TODO: //

        Mimic the validation code technique to populate the database with the checkboxes

        */

        $sid = $webform_submission->id();
        $form_id = $webform_submission->getWebform()->getOriginalId();

        // main contact
        if ($form_id == 'main_contact') {
            $query = \Drupal::database()->upsert('webform_submission_data');
            $query->fields([
                'webform_id',
                'sid',
                'name',
                'value'
            ]);
            $query->values([
                $form_id,
                $sid,
                'others_please_specify',
                $form_state->getValue('others_please_specify'),
            ]);
            $query->key('name');
            $query->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {

        $form_id = $webform_submission->getWebform()->getOriginalId();

        $config = \Drupal::config('cud.settings');

        if (!$config->get('webform_salesforce')) {

            \Drupal::logger('CUD Webform Salesforce Submission')->notice("CUD config settings MUST be checked to allow submission to Salesforce.");

            return;
        }

        if ($form_id === 'main_contact') {


            /*
             * NOTE: 
			 * a. Connected Apps > Manage > IP Restrictions > Edit Policy -> Relax Restrictions
             * b. Password must not contain special characters
             * https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG98_Psg5cppya1Pw412d5noYBiwTYs.vQydc2TOxfXA5C5t4ui6pGoS032lcUJrqsaownNrWRYMSANr3jD&client_secret= 6418036922479965133&username=communication@cud.ac.ae&password=6ZwMjGM5
             *
             * {
                  "access_token": "00D58000000I49l!ASAAQIWR0Nuc_7WOXQIbX16IMPy0_YXetFoUnJpqcqYkgrTOcyF3FIjoMrIGzA4HoC2_WVysuEXcmit_5HS9qk19N8m8.EZU",
                  "instance_url": "https://eu6.salesforce.com",
                  "id": "https://login.salesforce.com/id/00D58000000I49lEAC/00558000001LXFvAAO",
                  "token_type": "Bearer",
                  "issued_at": "1483988630023",
                  "signature": "TejdnHLCoGM8cIlCAR9SyNGN4TebHbxy0llIy2wImhM="
                }
             */

            $sales_force_token_url = $config->get('sales_force_token_url');
            $sales_force_lead_endpoint_url = $config->get('sales_force_lead_endpoint_url');
            $client_id = $config->get('client_id');
            $grant_type = 'password';
            $redirect_url = $config->get('redirect_url');
            $client_secret = $config->get('client_secret');
            $client_username = $config->get('username');
            $client_password = $config->get('password');

            // format with redirect uri
            $oauthUrl = "";
            if ($redirect_url != "") {
                $oauthUrl = "$sales_force_token_url?grant_type=$grant_type&client_id=$client_id&redirect_uri=$redirect_url&client_secret=$client_secret&username=$client_username&password=$client_password";
            } else {
                $oauthUrl = "$sales_force_token_url?grant_type=$grant_type&client_id=$client_id&client_secret=$client_secret&username=$client_username&password=$client_password";
            }

            $access = $this->get_curl_with_header_postfields($oauthUrl, 1);

            if ($access) {

                $bearerToken = json_decode($access)->access_token;

                $data = $webform_submission->getData();

                $data = array(
                    "FirstName" => $data['first_name'],
                    "MiddleName" => $data['middle_name'],
                    "LastName" => $data['last_name'],
                    "Gender__c" => $data['gender'],
                    "Email" => $data['email_with_confirm'],
                    "Phone" => $data['phone'],
                    // "MobilePhone" => $data['phone'],
                    "Program_of_interest__c" => $data['programs'],
                    "LeadSource" => \Drupal::request()->get('src') != null ? htmlentities(\Drupal::request()->get('src'), ENT_QUOTES) : 'webform' ,
                    "ORIGIN_CODE_1__c" => \Drupal::request()->get('o') != null ? htmlentities(\Drupal::request()->get('o'), ENT_QUOTES) : 'main-website' ,
                    "ORIGIN_CODE_2__c" => \Drupal::request()->get('so') != null ? htmlentities(\Drupal::request()->get('so'), ENT_QUOTES) : '' ,
                    "How_did_you_hear_about_us__c" => $data['where_did_you_hear_about_us_hidden'],
                    "Status" => 'New',
                    "Inquiry__c" => $data['inquiry'],
                    "Country_of_Residence__c" => $data['country'],
                    "Comments__c" => $form_state->getValue('others_please_specify') != null ? htmlentities($form_state->getValue('others_please_specify'), ENT_QUOTES) : '',

                );

                $data_string = json_encode($data);

                $http_header = array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string),
                    'Authorization: Bearer ' . $bearerToken
                );


                $result = $this->get_curl_with_header_postfields($sales_force_lead_endpoint_url, 0, $http_header, $data_string);

                if ($result) {
                    // \Drupal::logger('CUD Webform Salesforce Submission')->notice("Success Form: $form_id: " . serialize($form_state->getValue('others_please_specify')));
                    \Drupal::logger('CUD Webform Salesforce Submission')->notice("Success Form: $form_id: " . serialize($result));
                } else {
                    \Drupal::logger('CUD Webform Salesforce Submission')->notice("Error Form: $form_id: " . serialize($result));
                }
            } else {
                \Drupal::logger('CUD Webform Salesforce Submission')->notice("Error Form: $form_id: " . serialize($access));
            }
        }

        if (in_array($form_id, array('landing_business', 'landing_engineering', 'landing_psychology','fcad','landing_beat_the_heat'))) {

            $sales_force_token_url = $config->get('sales_force_token_url');
            $sales_force_lead_endpoint_url = $config->get('sales_force_lead_endpoint_url');
            $client_id = $config->get('client_id');
            $grant_type = 'password';
            $redirect_url = $config->get('redirect_url');
            $client_secret = $config->get('client_secret');
            $client_username = $config->get('username');
            $client_password = $config->get('password');

            // format with redirect uri
            $oauthUrl = "";
            if ($redirect_url != "") {
                $oauthUrl = "$sales_force_token_url?grant_type=$grant_type&client_id=$client_id&redirect_uri=$redirect_url&client_secret=$client_secret&username=$client_username&password=$client_password";
            } else {
                $oauthUrl = "$sales_force_token_url?grant_type=$grant_type&client_id=$client_id&client_secret=$client_secret&username=$client_username&password=$client_password";
            }

            $access = $this->get_curl_with_header_postfields($oauthUrl, 1);

            if ($access) {

                $bearerToken = json_decode($access)->access_token;

                // data of webform
                $data = $webform_submission->getData();

                // data for salesforce submission
                $data_final = array(
                    "FirstName" => $data['first_name'],
                    "LastName" => $data['last_name'],
                    "Email" => $data['email_with_confirm'],
                    "Phone" => $data['phone'],
                    "LeadSource" => \Drupal::request()->get('src') != null ? htmlentities(\Drupal::request()->get('src'), ENT_QUOTES) : 'webform' ,
                    "ORIGIN_CODE_1__c" => \Drupal::request()->get('o') != null ? htmlentities(\Drupal::request()->get('o'), ENT_QUOTES) : 'main-website' ,
                    "ORIGIN_CODE_2__c" => \Drupal::request()->get('so') != null ? htmlentities(\Drupal::request()->get('so'), ENT_QUOTES) : 'landing-page-' . $form_id,
                    "Status" => 'New',
                );


                if ($data['interests'] != null || $data['interests'] != '') {
                    $data_final["Program_of_interest__c"] = $data['interests'];
                }

                if ($data['subscribe'] != null || $data['subscribe'] != '') {
                    $data_final["HasOptedOutOfEmail"] = intval($data['interests']) == 1 ? "true" : "false";
                }

                $data_string = json_encode($data_final);

                $http_header = array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string),
                    'Authorization: Bearer ' . $bearerToken
                );


                \Drupal::logger('CUD Webform Salesforce Submission')->notice("Collections: $form_id: " . serialize($data));

                $result = $this->get_curl_with_header_postfields($sales_force_lead_endpoint_url, 0, $http_header, $data_string);

                if ($result) {
                    // \Drupal::logger('CUD Webform Salesforce Submission')->notice("Success Form: $form_id: " . serialize($form_state->getValue('others_please_specify')));
                    \Drupal::logger('CUD Webform Salesforce Submission')->notice("Success Form: $form_id: " . serialize($result));
                } else {
                    \Drupal::logger('CUD Webform Salesforce Submission')->notice("Error Form: $form_id: " . serialize($result));
                }
            } else {
                \Drupal::logger('CUD Webform Salesforce Submission')->notice("Error Form: $form_id: " . serialize($access));
            }
        }

    }

    //Added curl for faster response
    function get_curl_with_header_postfields($url, $post_type = 0, $http_header = null, $post_fields = null)
    {
        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            /* get 0, post 1 */
            curl_setopt($ch, CURLOPT_POST, $post_type);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            if ($http_header != null) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
            }

            if ($post_fields != null) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
            }

            $result = curl_exec($ch);

            $error = curl_error($ch);

            if (!$error) {
                // \Drupal::logger('CUD Webform')->notice("Result Form: " . serialize($result));
                return $result;
            } else {
                \Drupal::logger('CUD Webform')->notice("Error Form: " . serialize($error));

                return false;
            }


        } else {

            \Drupal::logger('CUD Webform using file_get_contents');

            return file_get_contents($url);
        }

    }


}


/*

https://stackoverflow.com/questions/30481979/adding-subscribers-to-a-list-using-mailchimps-api-v3

$data = [
    'email'     => 'johndoe@example.com',
    'status'    => 'subscribed',
    'firstname' => 'john',
    'lastname'  => 'doe'
];

syncMailchimp($data);

function syncMailchimp($data) {
    $apiKey = 'your api key';
    $listId = 'your list id';

    $memberId = md5(strtolower($data['email']));
    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
    $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;

    $json = json_encode([
        'email_address' => $data['email'],
        'status'        => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
        'merge_fields'  => [
            'FNAME'     => $data['firstname'],
            'LNAME'     => $data['lastname']
        ]
    ]);

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

    $result = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return $httpCode;
}

*/