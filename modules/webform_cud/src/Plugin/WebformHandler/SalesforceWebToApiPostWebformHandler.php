<?php

namespace Drupal\webform_cud\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\webform\Element\WebformOtherBase;
use Drupal\webform\Plugin\WebformHandler\RemotePostWebformHandler;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use GuzzleHttp\Exception\RequestException;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;

use Drupal\cud\Controller\CudHelperSalesforce;

/**
 * Webform submission remote post handler using API.
 *
 * @WebformHandler(
 *   id = "sfweb2api_post",
 *   label = @Translation("CUD Salesforce Web-to-API post"),
 *   category = @Translation("CUD Salesforce"),
 *   description = @Translation("Posts webform submissions to a Salesforce.com API."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class SalesforceWebToApiPostWebformHandler extends RemotePostWebformHandler
{

    /**
     * Typical salesforce campaign fields
     * Used for available list of campaign fields.
     *
     * @see https://help.salesforce.com/articleView?id=setting_up_web-to-lead.htm&type=0
     * @var array
     */
    protected $salesforceCampaignFields = [
        'email',
        'first_name',
        'middle_name',
        'last_name',
        'emirates_id',
        'gender',
        'campaign_id',
        'phone',
        'programs',
        'where_did_you_hear',
        'country',
        'others_please_specify',
        'email_opt_out',
        // deprecate subscribe and unsubscribe
        'subscribe',
        // deprecate subscribe and unsubscribe
        'unsubscribe',
        'agree_terms_and_condition',
        'lead_source',
        'lead_channel',
        'semester',
        'inquiry',
        'transfer_to',
        'origin_code_1',
        'origin_code_2',
        'school',
        'schools',
        'school_curriculum',
        'nationality',
        'high_school_status',
        'status',
        'category',
        'expected_enrolment_year',

        'birthdate',
        'passport',
        'passport_expiry',
        'is_uae_resident',
        'agree_to_terms_and_conditions',

        'file_passport', 
        'file_emirates_id', 
        'file_high_school_diploma', 
        'file_high_school_transcript', 
        'file_bachelor_degree', 
        'file_university_transcript', 
        'file_english_proficiency', 
        'file_additional_document_1', 
        'file_additional_document_2', 
        'file_additional_document_3',

        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_term',
        'utm_content',
        'utm_source_type',
        'utm_lead_source',
        'utm_marketing_channel',
        'utm_communication_channel',
        'utm_campaign_name'
    ];

    protected $salesforceCommonCUDWebToAPIFields = [
        'email' => 'Email',
        'first_name' => 'FirstName',
        'middle_name' => 'MiddleName',
        'last_name' => 'LastName',
        'emirates_id' => 'State_of_Residence__c',
        'gender' => 'Gender__c',
        'campaign_id' => 'Campaign_ID',
        'phone' => 'Phone',
        'programs' => 'Program_of_interest__c',
        'where_did_you_hear' => 'How_did_you_hear_about_us__c',
        'country' => 'Country_of_Residence__c',
        'others_please_specify' => 'Comments__c',
        'email_opt_out' => 'HasOptedOutOfEmail',
        // deprecate subscribe and unsubscribe
        'subscribe' => 'HasOptedOutOfEmailNegate',
        // deprecate subscribe and unsubscribe
        'unsubscribe' => 'HasOptedOutOfEmailDirect',
        'lead_source' => 'LeadSource',
        'lead_channel' => 'Lead_Channel__c',
        'semester' => 'Semester__c',
        'inquiry' => 'Inquiry__c',
        'transfer_to' => 'Transfer_to_Canada__c', // Transfer to Canada checkbox
        'origin_code_1' => 'ORIGIN_CODE_1__c',
        'origin_code_2' => 'ORIGIN_CODE_2__c',
        'school' => 'High_School__c', // main school interest
        'schools' => 'Other_Schools_of_Interest__c', // multiple selection / schools
        'school_curriculum' => 'High_School_Curriculum__c',
        'nationality' => 'Nationality__c',
        'high_school_status' => 'High_School_Status__c',
        'agree_terms_and_condition' => 'Agreed_to_Terms_and_Conditions__c',
        'status' => 'Status',
        'category' => 'Category__c',
        'expected_enrolment_year' => 'Year_of_Enrollment__c',
    
        'birthdate' => 'Date_of_Birth__c',
        'passport' => 'Passport_Number__c',
        'passport_expiry' => 'Passport_Expiry_Date__c', // yyyy-mm-dd
        'is_uae_resident' => 'Is_UAE_Resident__c',
        'emirates_id' => 'State_of_Residence__c',
        'agree_to_terms_and_conditions' => 'Agreed_to_Terms_and_Conditions__c', // user


        // just keep the name the same
        // will be mapped during salesforce
        // submission
        'file_passport' => 'file_passport', 
        'file_emirates_id' => 'file_emirates_id', 
        'file_high_school_diploma' => 'file_high_school_diploma', 
        'file_high_school_transcript' => 'file_high_school_transcript', 
        'file_bachelor_degree' => 'file_bachelor_degree', 
        'file_university_transcript' => 'file_university_transcript', 
        'file_english_proficiency' => 'file_english_proficiency', 
        'file_additional_document_1' => 'file_additional_document_1', 
        'file_additional_document_2' => 'file_additional_document_3', 
        'file_additional_document_3' => 'file_additional_document_3',

        'utm_source' => 'utm_source',
        'utm_medium' => 'utm_medium',
        'utm_campaign' => 'utm_campaign',
        'utm_term' => 'utm_term__c',
        'utm_content' => 'utm_content__c',
        'utm_source_type' => 'utm_source_type__c',
        'utm_lead_source' => 'utm_lead_source__c',
        'utm_marketing_channel' => 'utm_marketing_channel__c',
        'utm_communication_channel' => 'utm_communication_channel__c',
        'utm_campaign_name' => 'utm_campaign_name__c'
    ];

    protected $access = false;

    protected $sales_force_lead_endpoint_url = null;

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration()
    {

        $field_names = array_keys(\Drupal::service('entity_field.manager')->getBaseFieldDefinitions('webform_submission'));

        return [
            'salesforce_use_defaults' => TRUE,
            'salesforce_token_url' => '',
            'salesforce_client_id' => '',
            'salesforce_grant_type' => '',
            'salesforce_redirect_url' => '',
            'salesforce_client_secret' => '',
            'salesforce_client_username' => '',
            'salesforce_client_password' => '',
            'salesforce_mapping' => [],
            'excluded_data' => [],
            'custom_data' => '',
            'debug' => FALSE,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {

        $config = \Drupal::config('cud.settings');

        $this->sales_force_lead_endpoint_url =  $config->get('sales_force_lead_endpoint_url');

        if (!$config->get('webform_salesforce')) {

            \Drupal::logger('SalesforceWebToApiPostWebformHandler')->notice("CUD config settings MUST be checked to allow submission to Salesforce.");

            return;
        }

        $webform = $this->getWebform();

        $map_sources = [];

        $elements = $this->webform->getElementsDecoded();

        foreach ($elements as $key => $element) {

            if (strpos($key, '#') === 0 || empty($element['#title'])) {
                if (in_array($element["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container'))) {
                    // proceed to adding children
                } else
                    continue;
            }

            $map_sources[$key] = in_array($element["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container')) ? $key : $element['#title'];

            // 2nd depth // get fieldset children
            if (in_array($element["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container'))) {
                $sub_elements = $element;

                foreach ($sub_elements as $k_2 => $e_2) {

                    if (strpos($k_2, '#') === 0 || empty($e_2['#title'])) {
                        if (in_array($e_2["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container'))) {
                            // proceed to adding children
                        } else
                            continue;
                    }
                    
                    $map_sources[$k_2] = in_array($e_2["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container')) ?  '&nbsp;&nbsp;&nbsp;' . $k_2 : '&nbsp;&nbsp;&nbsp;&#8627;' . $e_2['#title'];

                    // 3rd depth // get fieldset children
                    if (in_array($e_2["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container'))) {
                        $sub_e = $e_2;

                        foreach ($sub_e as $k_3 => $e_3) {


                            if (strpos($k_3, '#') === 0 || empty($e_3['#title'])) {
                                if (in_array($e_3["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container'))) {
                                    // proceed to adding children
                                } else
                                    continue;
                            }
                            
                            $map_sources[$k_3] = in_array($e_3["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container')) ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $k_3 : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8627;' . $e_3['#title'];
        
                            // 4th depth // get fieldset children
                            if (in_array($e_3["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container'))) {
                                $sub_e_3 = $e_3;
        
                                foreach ($sub_e_3 as $k_4 => $e_4) {
        
                                    if (strpos($k_4, '#') === 0 || empty($e_4['#title'])) {
                                        continue;
                                    }
        
                                    $map_sources[$k_4] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8627;' . $e_4['#title'];
        
                                }
                            }
                        }
                    }
                }
            }
        }


        /** @var \Drupal\webform\WebformSubmissionStorageInterface $submission_storage */
        $submission_storage = \Drupal::entityTypeManager()->getStorage('webform_submission');
        $field_definitions = $submission_storage->getFieldDefinitions();
        $field_definitions = $submission_storage->checkFieldDefinitionAccess($webform, $field_definitions);

        foreach ($field_definitions as $key => $field_definition) {
            $map_sources[$key] = $field_definition['title'] . ' (type : ' . $field_definition['type'] . ')';
        }

        $form['salesforce_use_defaults'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Use system salesforce defaults'),
            '#description' => $this->t('If checked, global settings for salesforce will be used.'),
            '#return_value' => TRUE,
            '#default_value' => $this->configuration['salesforce_use_defaults'],
        ];

        $form['salesforce'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Custom'),
            '#states' => array(
                'visible' => array(
                    ':input[name="settings[salesforce_use_defaults]"]' => array('checked' => FALSE),
                ),
            ),
        ];

        $form['salesforce']['salesforce_token_url'] = [
            '#type' => 'url',
            '#title' => $this->t('Salesforce Token Url'),
            '#description' => $this->t('The full token url for posting to salesforce.com api'),
            '#states' => array(
                'required' => array(
                    ':input[name="settings[salesforce_use_defaults]"]' => array('checked' => FALSE),
                ),
            ),
            '#default_value' => !empty($this->configuration['salesforce_token_url']) ? $this->configuration['salesforce_token_url'] : ''
        ];



        $form['salesforce']['salesforce_client_id'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Salesforce Client Id'),
            '#description' => $this->t('The client hashed id for connecting to salesforce.com'),
            '#states' => array(
                'required' => array(
                    ':input[name="settings[salesforce_use_defaults]"]' => array('checked' => FALSE),
                ),
            ),
            '#default_value' => !empty($this->configuration['salesforce_client_id']) ? $this->configuration['salesforce_client_id'] : ''
        ];

        $form['salesforce']['salesforce_grant_type'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Salesforce grant type'),
            '#description' => $this->t('Salesforce grant type for connecting to salesforce.com'),
            '#states' => array(
                'required' => array(
                    ':input[name="settings[salesforce_use_defaults]"]' => array('checked' => FALSE),
                ),
            ),
            '#default_value' => !empty($this->configuration['salesforce_grant_type']) ? $this->configuration['salesforce_grant_type'] : ''
        ];

        $form['salesforce']['salesforce_redirect_url'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Salesforce Redirect Url'),
            '#description' => $this->t('Salesforce redirect url after generating token from salesforce.com'),
            '#states' => array(
                'required' => array(
                    ':input[name="settings[salesforce_use_defaults]"]' => array('checked' => FALSE),
                ),
            ),
            '#default_value' => !empty($this->configuration['salesforce_redirect_url']) ? $this->configuration['salesforce_redirect_url'] : ''
        ];

        $form['salesforce']['salesforce_client_secret'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Salesforce Client Secret'),
            '#description' => $this->t('Salesforce client secret hashed for connecting to salesforce.com'),
            '#states' => array(
                'required' => array(
                    ':input[name="settings[salesforce_use_defaults]"]' => array('checked' => FALSE),
                ),
            ),
            '#default_value' => !empty($this->configuration['salesforce_client_secret']) ? $this->configuration['salesforce_client_secret'] : ''
        ];

        $form['salesforce']['salesforce_client_username'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Salesforce Client Username'),
            '#description' => $this->t('Salesforce client user name for connecting to salesforce.com'),
            '#states' => array(
                'required' => array(
                    ':input[name="settings[salesforce_use_defaults]"]' => array('checked' => FALSE),
                ),
            ),
            '#default_value' => !empty($this->configuration['salesforce_client_username']) ? $this->configuration['salesforce_client_username'] : ''
        ];

        $form['salesforce']['salesforce_client_password'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Salesforce Client Password'),
            '#description' => $this->t('Salesforce client user password for connecting to salesforce.com'),
            '#states' => array(
                'required' => array(
                    ':input[name="settings[salesforce_use_defaults]"]' => array('checked' => FALSE),
                ),
            ),
            '#default_value' => !empty($this->configuration['salesforce_client_password']) ? $this->configuration['salesforce_client_password'] : ''
        ];

        $this->salesforceCampaignFields = array_combine($this->salesforceCampaignFields, $this->salesforceCampaignFields);

        ksort($this->salesforceCampaignFields);

        $form['salesforce_mapping'] = [
            '#type' => 'webform_mapping',
            '#title' => $this->t('Webform to Salesforce mapping'),
            '#description' => $this->t('Only Maps with specified "Salesforce Web-to-Lead Campaign Field" will be submitted to salesforce.'),
            '#source__title' => t('Webform Submitted Data'),
            '#destination__title' => t('Salesforce Web-to-Lead Campaign Field'),
            '#source' => $map_sources,
            '#destination__type' => 'webform_select_other',
            '#destination' =>$this->salesforceCampaignFields,
            '#default_value' => $this->configuration['salesforce_mapping'],
        ];

        $form['custom_data'] = [
            '#type' => 'details',
            '#title' => $this->t('Custom data'),
            '#description' => $this->t('Custom data will take precedence over submission data. You may use tokens.'),
        ];

        $form['custom_data']['custom_data'] = [
            '#type' => 'webform_codemirror',
            '#mode' => 'yaml',
            '#title' => $this->t('Custom data'),
            '#description' => $this->t('Enter custom data that will be included in all remote post requests.'),
            '#parents' => ['settings', 'custom_data'],
            '#default_value' => $this->configuration['custom_data'],
        ];
        $form['custom_data']['custom_data'] = [
            '#type' => 'webform_codemirror',
            '#mode' => 'yaml',
            '#title' => $this->t('Insert data'),
            '#description' => $this->t("Enter custom data that will be included when a webform submission is saved."),
            '#parents' => ['settings', 'custom_data'],
            '#default_value' => $this->configuration['custom_data'],
        ];

        $form['custom_data']['token_tree_link'] = $this->tokenManager->buildTreeLink();

        $form['debug'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Enable debugging'),
            '#description' => $this->t('If checked, posted submissions will be displayed onscreen to all users.'),
            '#return_value' => TRUE,
            '#default_value' => $this->configuration['debug'],
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        $config = \Drupal::config('cud.settings');

        if ($form_state->getValue('salesforce_use_defaults') == TRUE) {
            $this->configuration['salesforce_token_url'] = $config->get('sales_force_token_url');
            $this->configuration['salesforce_client_id'] = $config->get('client_id');
            $this->configuration['salesforce_grant_type'] = "password";
            $this->configuration['salesforce_redirect_url'] = $config->get('redirect_url');
            $this->configuration['salesforce_client_secret'] = $config->get('client_secret');
            $this->configuration['salesforce_client_username'] =  $config->get('username');
            $this->configuration['salesforce_client_password'] = $config->get('password');
        } else {
            $this->configuration['salesforce_token_url'] = $form_state->getValue('salesforce')['salesforce_token_url'];
            $this->configuration['salesforce_client_id'] = $form_state->getValue('salesforce')['salesforce_client_id'];
            $this->configuration['salesforce_grant_type'] = $form_state->getValue('salesforce')['salesforce_grant_type'];
            $this->configuration['salesforce_redirect_url'] = $form_state->getValue('salesforce')['salesforce_redirect_url'];
            $this->configuration['salesforce_client_secret'] = $form_state->getValue('salesforce')['salesforce_client_secret'];
            $this->configuration['salesforce_client_username'] = $form_state->getValue('salesforce')['salesforce_client_username'];
            $this->configuration['salesforce_client_password'] = $form_state->getValue('salesforce')['salesforce_client_password'];
        }

        return $form;
    }

    /**
     * remoteFinalMapWebToApi
     * Map the data for salesforce api posting
     *
     * @param array $request_post_data
     * @return array
     */
    protected function remoteFinalMapWebToApi(array $request_post_data)
    {
        // \Drupal::logger('SalesforceWebToApiPostWebformHandler')->notice("request posted data - RAW: " . serialize($request_post_data));

        // process all known documents
        // and append to the inquiry field
        $file_inquiry = '';

        foreach ($this->salesforceCommonCUDWebToAPIFields as $k => $v) {
            // re-assign key -> value
            if (array_key_exists($k, $request_post_data)) {
                $request_post_data[$v] = $request_post_data[$k];

                if (in_array($k, array('file_passport', 'file_emirates_id', 'file_high_school_diploma', 'file_high_school_transcript', 'file_bachelor_degree', 'file_university_transcript', 'file_english_proficiency', 'file_additional_document_1', 'file_additional_document_2', 'file_additional_document_3'))) {
                    $request_post_data[$k] = $this->getFileUri($request_post_data[$k]);
    
                    if ($request_post_data[$k] != null) {
                        $file_inquiry .= $k . " : " . $request_post_data[$k] . "; ";
    
                        if ($k == 'file_passport') {
                            $request_post_data['Passport_Copy__c'] = $request_post_data[$k];
                        }
                        
                        if ($k == 'file_emirates_id') {
                            $request_post_data['Emirates_ID_Copy__c'] = $request_post_data[$k];
                        }

                        /* if ($k === 'is_cud_student') {
                            $request_post_data['is_cud_student'] = ((int)$request_post_data['is_cud_student'] >= 1) ? 1 : 0;
                        }
            
                        if ($k === 'cud_student_id') {
                            $request_post_data['cud_student_id'] = ((int)$request_post_data['cud_student_id'] >= 1) ? 1 : 0;
                        } */
                    }
                }

                unset($request_post_data[$k]);
            }

            
        }

        if (strlen($file_inquiry))
            $request_post_data['Inquiry__c'] = $request_post_data['Inquiry__c'] . " /// " . $file_inquiry;

        if (array_key_exists('Is_UAE_Resident__c', $request_post_data)) {
            $request_post_data['Is_UAE_Resident__c'] = ((int)$request_post_data['Is_UAE_Resident__c'] >= 1) ? 1 : 0;
        }

        if (array_key_exists('Agreed_to_Terms_and_Conditions__c', $request_post_data)) {
            $request_post_data['Agreed_to_Terms_and_Conditions__c'] = ((int)$request_post_data['Agreed_to_Terms_and_Conditions__c'] >= 1) ? 1 : 0;
        }
        
        if (!array_key_exists('LeadSource', $request_post_data)) {
            $request_post_data['LeadSource'] = \Drupal::request()->get('src') != null ? htmlentities(\Drupal::request()->get('src'), ENT_QUOTES) : "webform";
        }
        if (!array_key_exists('ORIGIN_CODE_1__c', $request_post_data)) {
            $request_post_data['ORIGIN_CODE_1__c'] = \Drupal::request()->get('o') != null ? htmlentities(\Drupal::request()->get('o'), ENT_QUOTES) :  "main-site";
        }

        // if not exists
        // just keep it ignored
        if (!array_key_exists('ORIGIN_CODE_2__c', $request_post_data)) {
            if (\Drupal::request()->get('so') != null) {
                $request_post_data['ORIGIN_CODE_2__c'] = htmlentities(\Drupal::request()->get('so'), ENT_QUOTES);
            }
        }

        // -------------------------------------------------------------------------------------------------------------------
        // Note: unsubscribe / subscribe MUST NOT be present in the same form
        //       it is encouraged to use unsubscribe for direct 1to1 association with has opted field

        // initialize has opted out of email data for submission
        // is the field target used for unsubscribing leads
        // $request_post_data['HasOptedOutOfEmail'] = 1; //  HasOptedOutOfEmail will be checked

        // subscribe association is used
        // negating the value of the subscribe/HasOptedOutOfEmailNegate
        // will result the correct value
        /* if (array_key_exists('HasOptedOutOfEmailNegate', $request_post_data)) {
            // has subscribe/HasOptedOutOfEmail
            // reverse value;
            // after reversal, value 1 will be 0 - HasOptedOutOfEmail will be unchecked
            // after reversal, value 0 will be 1 - HasOptedOutOfEmail will be checked
            $request_post_data['HasOptedOutOfEmail'] = ((int) $request_post_data['HasOptedOutOfEmailNegate'] > 0) ? 0 : 1;

            // needs to unset
            // this isn't recognized by
            // salesforce, thus causing non-submission
            unset($request_post_data['HasOptedOutOfEmailNegate']);
        }

        // instead of the above, start using unsubscribe
        // unsubscribe = "Yes"; has opted out of email true
        // unsubscribe = "No"; has opted out of email false
        if (array_key_exists('HasOptedOutOfEmailDirect', $request_post_data)) {

            // no more value reversal; directly push the value
            if ($request_post_data['HasOptedOutOfEmailDirect'] == "Yes") {
                $request_post_data['HasOptedOutOfEmail'] = 1;
            }

            if ($request_post_data['HasOptedOutOfEmailDirect'] == "No") {
                $request_post_data['HasOptedOutOfEmail'] = 0;
            }

            // needs to unset
            // this isn't recognized by
            // salesforce, thus causing non-submission
            unset($request_post_data['HasOptedOutOfEmailDirect']);
        } */


        if (array_key_exists('HasOptedOutOfEmail', $request_post_data)) {

            if ($request_post_data['HasOptedOutOfEmail'] == "Yes" || $request_post_data['HasOptedOutOfEmail'] == 1) {
                // UI checked; means want to receive
                // so make sure that opt-out is false
                $request_post_data['HasOptedOutOfEmail'] = 0;
            }

            if ($request_post_data['HasOptedOutOfEmail'] == "No" || $request_post_data['HasOptedOutOfEmail'] == 0) {
                // UI NOT checked; means wants to no emails
                // so make sure that opt-out is true
                $request_post_data['HasOptedOutOfEmail'] = 1;
            }
        }


        if (array_key_exists('Agreed_to_Terms_and_Conditions__c', $request_post_data)) {

            if ($request_post_data['Agreed_to_Terms_and_Conditions__c'] == "Yes") {
                $request_post_data['Agreed_to_Terms_and_Conditions__c'] = 1;
            }

            if ($request_post_data['Agreed_to_Terms_and_Conditions__c'] == "No") {
                $request_post_data['Agreed_to_Terms_and_Conditions__c'] = 0;
            }
        }

        // -------------------------------------------------------------------------------------------------------------------

        // special cases
        // Transfer to Canada
        if (!array_key_exists('Transfer_to_Canada__c', $request_post_data)) {
            // default value is study
            $request_post_data['Transfer_to_Canada__c'] = 1;
        } else {

            if (stripos($request_post_data['Transfer_to_Canada__c'], 'study') !== false) {
                $request_post_data['Transfer_to_Canada__c'] = 1;
            } else {
                $request_post_data['Transfer_to_Canada__c'] = 0;
            }
        }

        // if campaign id is not set
        // check parameters
        $cid = \Drupal::request()->get('cid');

        if (!array_key_exists('Campaign_ID', $request_post_data)) {

            if ($cid != null) {
                $request_post_data['Campaign_ID'] =  $cid;
            }
        }

        $request_post_data['Campaign_ID'] = $cid != null ? $cid : $request_post_data['Campaign_ID'];

        // convert term
        if (array_key_exists('How_did_you_hear_about_us__c', $request_post_data)) {

            foreach ($request_post_data['How_did_you_hear_about_us__c'] as $k => $v) {
                $term = Term::load($v);
                if ($term)
                    $request_post_data['How_did_you_hear_about_us__c'][$k] =  $term->getName();
                else
                    $request_post_data['How_did_you_hear_about_us__c'][$k] =  $v;
            }
        }

        // -------------------------------------------------------------------------------------------------------------------

        // utm

        $utm_source = \Drupal::request()->get('utm_source');

        if (!empty($utm_source)) {
            $request_post_data['utm_source__c'] =  $utm_source;
        }



        // utm
        $utm_medium = \Drupal::request()->get('utm_medium');

        if (!empty($utm_medium)) {
            $request_post_data['utm_medium__c'] =  $utm_medium;
        }

        // utm
        $utm_campaign = \Drupal::request()->get('utm_campaign');

        if (!empty($utm_campaign)) {
            $request_post_data['utm_campaign__c'] =  $utm_campaign;
        }

        // utm

        $utm_term = \Drupal::request()->get('utm_term');

        if (!empty($utm_term)) {
            $request_post_data['utm_term__c'] =  $utm_term;
        }

        // utm

        $utm_content = \Drupal::request()->get('utm_content');

        if (!empty($utm_content)) {
            $request_post_data['utm_content__c'] =  $utm_content;
        }

        // utm

        $utm_source_type = \Drupal::request()->get('utm_source_type');

        if (!empty($utm_source_type)) {
            $request_post_data['utm_source_type__c'] =  $utm_source_type;
        }

        // utm

        $utm_lead_source = \Drupal::request()->get('utm_lead_source');

        if (!empty($utm_lead_source)) {
            $request_post_data['utm_lead_source__c'] =  $utm_lead_source;
        }

        // utm

        $utm_marketing_channel = \Drupal::request()->get('utm_marketing_channel');

        if (!empty($utm_marketing_channel)) {
            $request_post_data['utm_marketing_channel__c'] =  $utm_marketing_channel;
        }

        // utm

        $utm_communication_channel = \Drupal::request()->get('utm_communication_channel');

        if (!empty($utm_communication_channel)) {
            $request_post_data['utm_communication_channel__c'] =  $utm_communication_channel;
        }

        // utm
        $utm_campaign_name = \Drupal::request()->get('utm_campaign_name');

        if (!empty($utm_campaign_name)) {
            $request_post_data['utm_campaign_name__c'] =  $utm_campaign_name;
        }

        return $request_post_data;
    }

    /**
     * remoteGetLead
     * Get the Salesforce Lead
     *
     * @param null $email
     *  Email field value of Lead during submission process
     *
     * @return bool|mixed|string
     *  Lead object or false
     */
    public function remoteGetLead($email = null)
    {

        if (!$this->access) {
            return false;
        }

        $result = \Drupal::httpClient()->get($this->access->instance_url . "/services/data/v40.0/query?q=SELECT+Id,Email+from+Lead+Where+Email='$email'", [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->access->access_token,
            ],
        ])->getBody()->getContents();

        return $result;
    }

    /**
     * getLeadMembership
     * Membership of the Lead in Salesforce CampaignMember object
     *
     * @param null $lead_id
     *  Lead ID of the Salesforce Lead object
     *
     * @return bool|mixed|string
     *  CampaignMember object or false
     */
    public function getLeadMembership($lead_id = null, $campaign_id = null)
    {

        if (!$this->access) {
            return false;
        }

        $result = \Drupal::httpClient()->get($this->access->instance_url . "/services/data/v40.0/query?q=SELECT+LeadId,CampaignId+from+CampaignMember+Where+LeadId='" . $lead_id . "'+AND+CampaignId='" . $campaign_id . "'", [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->access->access_token,
            ],
        ])->getBody()->getContents();

        return $result;
    }


    /**
     * remoteAddUpdateLead
     * Updates the Salesforce object in relation to the mapped submitted key=>value pairs data string
     *
     * @param null $data_string
     *  key => value pair; using the API REST / SOAP object and endpoints
     *
     * @param null $object_url
     *  the endpoint (minus the instance url) for the CampaignMember object
     *
     * @param null $operation
     *  standard operation string, such as "update"
     *
     * @return bool|mixed|string
     *  Successful salesforce json formatted response object or false
     *
     */
    public function remoteAddUpdateLead($data_string = null, $object_url = null, $operation = null)
    {

        if (!$this->access) {
            return false;
        }

        $http_header = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'Authorization: Bearer ' . $this->access->access_token
        );

        $result = $this->getCurlWithHeaderPostfields($this->access->instance_url . $object_url, 0, $http_header, $data_string, $operation);

        return $result;
    }


    /**
     * remoteAddUpdateLeadMembership
     * Updates the Salesforce CampaignMember object
     *
     * @param null $lead_id
     *  ID of the Lead object
     *
     * @param null $campaign_id
     *  ID of the Campaign object
     *
     * @return bool|mixed|string
     *  Successful salesforce json formatted response object or false
     *
     */
    public function remoteAddUpdateLeadMembership($lead_id = null, $campaign_id = null)
    {

        if (!$this->access) {
            return false;
        }

        $data_string = json_encode(
            array(
                'LeadId' => $lead_id,
                'CampaignId' => $campaign_id,
            )
        );

        $http_header = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'Authorization: Bearer ' . $this->access->access_token
        );

        $url = $this->access->instance_url . "/services/data/v40.0/sobjects/CampaignMember";

        // update
        $result = $this->getCurlWithHeaderPostfields($url, 1, $http_header, $data_string, null);

        return $result;
    }

    /**
     * remotePost
     * Execute a remote post.
     *
     * @param string $operation
     *   The type of webform submission operation to be posted. Can be 'insert',
     *   'update', or 'delete'.
     *
     * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
     *   The webform submission to be posted.
     *
     * @return array|bool
     */
    protected function remotePost($operation, WebformSubmissionInterface $webform_submission)
    {

        if (!$this->remoteAuthorize()) {
            return false;
        }

        // get webform submissions
        $request_post_data = $this->getPostData($operation, $webform_submission);

        // re-align with api fields
        $request_post_data = $this->remoteFinalMapWebToApi($request_post_data);

        // semicolon sv array values
        // salesforce dislikes array/json
        // on passing thru api
        foreach ($request_post_data as $k => $v) {

            if (is_array($v)) {
                $request_post_data[$k] = implode(';', $v);
            }
        }

        \Drupal::logger('SalesforceWebToApiPostWebformHandler')->notice("request posted data: " . serialize($request_post_data));

        // get lead
        if (array_key_exists('Email', $request_post_data)) {

            $campaign_ids = null;

            if (array_key_exists('Campaign_ID', $request_post_data)) {

                $campaign_ids = $request_post_data["Campaign_ID"];

                // remove the campaign
                // and don't push it along with data
                unset($request_post_data["Campaign_ID"]);
            }

            $lead = $this->remoteGetLead($request_post_data['Email']);

            if (json_decode($lead)->totalSize > 0) {

                $data_string = json_encode($request_post_data);

                try {

                    $this->remoteAddUpdateLead($data_string, json_decode($lead)->records[0]->attributes->url, "Update");
                } catch (RequestException $request_exception) {

                    \Drupal::logger('SalesforceWebToApiPostWebformHandler')->notice("ERROR: UPDATE Lead: " . serialize($request_exception));
                }
            } else {

                $request_post_data["Status"] = "New";

                $data_string = json_encode($request_post_data);

                try {

                    //TODO://
                    // use the settings of cud
                    // but for now the entire url is included including the host/domain of salesforce
                    // can't remove because of might be dependencies
                    // USE soon =>  $this->sales_force_lead_endpoint_url
                    $result = $this->remoteAddUpdateLead($data_string, "/services/data/v20.0/sobjects/Lead/", "New");

                    \Drupal::logger('SalesforceWebToApiPostWebformHandler')->notice("NEW Lead Created: " . serialize($result));
                } catch (RequestException $request_exception) {

                    \Drupal::logger('SalesforceWebToApiPostWebformHandler')->notice("ERROR: NEW Lead Creation: " . serialize($request_exception));
                }
            }

            // try getting the lead
            // again
            $lead = $this->remoteGetLead($request_post_data['Email']);

            if ($lead && $campaign_ids && (json_decode($lead)->totalSize > 0)) {

                $lead_id = json_decode($lead)->records[0]->Id;

                $arr_campaign_id = explode(";", $campaign_ids);

                foreach ($arr_campaign_id as $c) {

                    $member = $this->getLeadMembership($lead_id, $c);

                    if ($member) {

                        if (json_decode($member)->totalSize > 0) {
                            // don't do anything
                            // already a member
                            continue;
                        }

                        $member = $this->remoteAddUpdateLeadMembership($lead_id, $c);

                        // \Drupal::logger('SalesforceWebToApiPostWebformHandler')->notice("Member: " . serialize($member));
                    }
                }
            }
        }


        \Drupal::logger('SalesforceWebToApiPostWebformHandler')->notice("final request post data: " . serialize($request_post_data));


        return true;
    }

    /**
     * getPostData
     * Get a webform submission's post data.
     *
     * @param string $operation
     *   The type of webform submission operation to be posted. Can be 'insert',
     *   'update', or 'delete'.
     * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
     *   The webform submission to be posted.
     *
     * @return array
     *   A webform submission converted to an associative array.
     */
    protected function getPostData($operation, WebformSubmissionInterface $webform_submission)
    {

        $data = $webform_submission->toArray(TRUE);
        $data = $data['data'] + $data;
        unset($data['data']);
        // Get data from parent.
        // Well the idea is that only mapped data and custom data are passed to salesforce.
        // Also curation is best handled only at salesforce_mapping.
        // Unable to make use of parent getPostData logic.
        // $data = parent::getPostData($operation, $webform_submission); .
        // Get Salesforce field mappings.

        $salesforce_mapping = $this->configuration['salesforce_mapping'];

        // 07.07.2017
        // modified from original ++g++
        // using mapping to populate data instead of the submitted webform keys
        foreach ($salesforce_mapping as $key => $value) {
            if (!empty($value) && !empty($data[$key])) {
                $salesforce_data[$value] = $data[$key];
            }
        }

        // Append custom data.
        if (!empty($this->configuration['custom_data'])) {
            $salesforce_data = Yaml::decode($this->configuration['custom_data']) + $salesforce_data;
        }

        // Append operation data.
        if (!empty($this->configuration[$operation . '_custom_data'])) {
            $salesforce_data = Yaml::decode($this->configuration[$operation . '_custom_data']) + $salesforce_data;
        }

        // Replace tokens.
        $salesforce_data = $this->tokenManager->replace($salesforce_data, $webform_submission);

        // Allow modification of data by other modules.
        \Drupal::moduleHandler()->alter('webform_cud_posted_data', $salesforce_data, $this->webform, $webform_submission);
        return $salesforce_data;
    }


    // helpers

    /**
     * remoteAuthorize
     * Get Salesforce authorization to be able the 
     * Api connectivity
     *
     * @return bool|mixed|void
     */
    protected function remoteAuthorize()
    {

        $config = \Drupal::config('cud.settings');

        $salesforce_token_url = $config->get('sales_force_token_url');
        $salesforce_client_id = $config->get('client_id');
        $salesforce_grant_type = "password";
        $salesforce_redirect_url = $config->get('redirect_url');
        $salesforce_client_secret = $config->get('client_secret');
        $salesforce_client_username =  $config->get('username');
        $salesforce_client_password = $config->get('password');

        if ($this->configuration['salesforce_use_defaults'] != TRUE) {
            $salesforce_token_url = $this->configuration['salesforce_token_url'];
            $salesforce_client_id = $this->configuration['salesforce_client_id'];
            $salesforce_grant_type = $this->configuration['salesforce_grant_type'];
            $salesforce_redirect_url = $this->configuration['salesforce_redirect_url'];
            $salesforce_client_secret = $this->configuration['salesforce_client_secret'];
            $salesforce_client_username = $this->configuration['salesforce_client_username'];
            $salesforce_client_password = $this->configuration['salesforce_client_password'];
        }

        if (empty($salesforce_token_url)) {
            return;
        }

        if ($salesforce_redirect_url != "") {
            $oauthUrl = "$salesforce_token_url?grant_type=$salesforce_grant_type&client_id=$salesforce_client_id&redirect_uri=$salesforce_redirect_url&client_secret=$salesforce_client_secret&username=$salesforce_client_username&password=$salesforce_client_password";
        } else {
            $oauthUrl = "$salesforce_token_url?grant_type=$salesforce_grant_type&client_id=$salesforce_client_id&client_secret=$salesforce_client_secret&username=$salesforce_client_username&password=$salesforce_client_password";
        }

        if ($access = $this->getCurlWithHeaderPostfields($oauthUrl, 1)) {
            if (isset(json_decode($access)->access_token)) {
                $this->access = json_decode($access);
                return $this->access;
            }
        }

        return false;
    }

    /**
     * getCurlWithHeaderPostfields
     * Custom Curl Posting with tweaks for properly submitting
     * to Salesforce endponts
     * 
     * 
     * @param $url
     *
     * @param int $post_type
     *
     * @param null $http_header
     *
     * @param null $post_fields
     *
     * @param null $operation
     *
     * @return bool|mixed|string
     */
    function getCurlWithHeaderPostfields($url, $post_type = 0, $http_header = null, $post_fields = null, $operation = null)
    {
        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            /* get 0, post 1 */
            curl_setopt($ch, CURLOPT_POST, $post_type);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            if ($operation == "Update") {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
            }

            if ($http_header != null) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
            }

            if ($post_fields != null) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
            }

            $result = curl_exec($ch);

            $error = curl_error($ch);

            if (!$error) {
                // \Drupal::logger("CudHelperSalesforce")->notice("Successful using curl_init");
                return $result;
            } else {
                \Drupal::logger("CudHelperSalesforce")->notice("Error using curl_init: " . serialize($error));
                return false;
            }
        } else {
            // \Drupal::logger("CudHelperSalesforce")->notice("Using file_get_contents");

            return file_get_contents($url);
        }
    }

    protected function getFileUri($fid = null)
    {

        if ($fid == null) {
            return null;
        }

        $service = \Drupal::service('entity.manager')->getStorage('file');
        $file = $service->load($fid);

        if ($file) {
            return file_create_url($file->getFileUri());
        }

        return null;
    }
}
