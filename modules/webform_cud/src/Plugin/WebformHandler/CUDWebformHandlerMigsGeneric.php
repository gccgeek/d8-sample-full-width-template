<?php

namespace Drupal\webform_cud\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;

use Drupal\webform_cud\lib\RijndaelOpenSSL;

/**
 * Payment Generic HOP Migs
 *
 * @WebformHandler(
 *   id = "cud_payment_migs_generic",
 *   label = @Translation("CUD Migs Payment Generic / Direct integration"),
 *   category = @Translation("CUD Payment"),
 *   description = @Translation("Generic payment gateway direct payment for MIGS."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class CUDWebformHandlerMigsGeneric extends WebformHandlerBase
{
     protected $custom_local_use_encryption_key = "117781CUD117781";

    /**
     * {@inheritdoc}
     */
    public function alterElements(array &$elements, WebformInterface $webform)
    {
        $form_id = $webform->id();
    }

    public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {}

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {}

    /**
     * {@inheritdoc}
     */
    public function postSave(WebformSubmissionInterface $webform_submission, $update = true)
    {
    }

      /**
     * {@inheritdoc}
     */
    public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {
        if ($webform_submission->id()) {

            $rijndael =  new RijndaelOpenSSL();

            $str_id = (string)$webform_submission->id();

            $hash = $rijndael->encrypt($str_id, $this->custom_local_use_encryption_key);

            $url = "/payment";

            $encoded_hash = urlencode($hash);

            $encoded_hash = str_replace(array('%'), array('PPPPP'), urlencode($hash));

            $response = new LocalRedirectResponse("$url/$encoded_hash");

            $response->send(); 

        }
    }
}
