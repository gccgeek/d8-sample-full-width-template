<?php

namespace Drupal\webform_cud\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;

use Drupal\webform_cud\lib\RijndaelOpenSSL;

/**
 * Payment HOP Migs
 *
 * @WebformHandler(
 *   id = "cud_payment_migs",
 *   label = @Translation("CUD Migs Payment Direct integration"),
 *   category = @Translation("CUD Payment"),
 *   description = @Translation("Payment gateway direct payment for MIGS."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class CUDWebformHandlerMigs extends WebformHandlerBase
{
    /**
     * {@inheritdoc}
     */
    public function alterElements(array &$elements, WebformInterface $webform)
    {
        $form_id = $webform->id();
    }

    public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {

        /* $form_id = $webform_submission->getWebform()->getOriginalId();

        $current_state = $webform_submission->getState();

        if ($form_state->isSubmitted()) {
        $triggering_element = $form_state->getTriggeringElement();
        if ($triggering_element["#value"] === "Register and Pay") {
        // $form_state->setResponse( new TrustedRedirectResponse('/test/payment/?formid='. $form_id) );

        $url="/international-summer-school/pay";

        $form_state->getValue("birthdate");

        $hash = urlencode(base64_encode(1));

        $response = new LocalRedirectResponse("$url/$hash");

        $form_state->setResponse($response);
        }
        } */
    }

    /**
     * {@inheritdoc}
     */
    public function postSave(WebformSubmissionInterface $webform_submission, $update = true)
    {
        /* $url = "/international-summer-school/pay";

        // $form_state->getValue("birthdate");

        $hash = urlencode(base64_encode(1));

        $response = new LocalRedirectResponse("$url/$hash");

        $response->send(); */
    }

      /**
     * {@inheritdoc}
     */
    public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {
        if ($webform_submission->id()) {

            $rijndael =  new RijndaelOpenSSL();

            $str_id = (string)$webform_submission->id();

            $hash = $rijndael->encrypt($str_id, "117781CUD117781");

            $url = "/international-summer-school/registration/pay";

            $encoded_hash = urlencode($hash);

            $encoded_hash = str_replace(array('%'), array('PPPPP'), urlencode($hash));

            $response = new LocalRedirectResponse("$url/$encoded_hash");

            $response->send(); 

        }
    }


}
