<?php

namespace Drupal\webform_cud\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Salesforce Unsubscribe
 *
 * @WebformHandler(
 *   id = "cud_salesforce_unsubscribe",
 *   label = @Translation("CUD Salesforce Unsubscribe"),
 *   category = @Translation("CUD Salesforce Unsubscribe"),
 *   description = @Translation("Integration: Moves the respondent's email on Salesforce to the appropriate campaign. Check 'Do not call'/'email opt-out' Salesforce fields."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class CUDWebformHandlerSalesforceUnsubscribe extends WebformHandlerBase
{
    /**
     * {@inheritdoc}
     */
    public function alterElements(array &$elements, WebformInterface $webform)
    {
        $form_id = $webform->id();
    }

    public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {}

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {}

    /**
     * {@inheritdoc}
     */
    public function postSave(WebformSubmissionInterface $webform_submission, $update = true)
    {}

      /**
     * {@inheritdoc}
     */
    public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {
        if ($id = $webform_submission->id()) {

            $webform_submitted_data = $this->getWebformSubmissionData($webform_submission->id());

            $url = "/unsubscribe";

            $encoded_email = urlencode($webform_submitted_data['email_with_confirm']);
            
            $response = new LocalRedirectResponse("$url/$id/$encoded_email");

            $response->send(); 
        }
    }

    /* webform */
    public function getWebformSubmissionData($sid = 0)
    {

        // Load submission using sid.
        /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
        $webform_submission = \Drupal\webform\Entity\WebformSubmission::load($sid);

        if ($webform_submission) {
            // Get submission data.
            $data = $webform_submission->getData();

            if ($data) {
                return $data;
            }

            return false;
        }

        return false;
    }

}
