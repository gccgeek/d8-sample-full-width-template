<?php

namespace Drupal\webform_cud\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\webform\Element\WebformOtherBase;
use Drupal\webform\Plugin\WebformHandler\RemotePostWebformHandler;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use GuzzleHttp\Exception\RequestException;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Webform submission remote post handler using API.
 *
 * @WebformHandler(
 *   id = "sfweb2api_active_campaign_post",
 *   label = @Translation("CUD ActiveCampaign Web-to-API post"),
 *   category = @Translation("CUD Api to ActiveCampaign"),
 *   description = @Translation("Posts webform submissions to a ActiveCampaign.com API."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class ActiveCampaignPostWebformHandler extends RemotePostWebformHandler
{

    /**
     * Typical active campaign fields
     * Used for available list of campaign fields.
     *
     * @var array
     */
    protected $activeCampaignFields = [
        'email' => 'email',
        'first_name' => 'firstName',
        'last_name' => 'lastName',
        'gender' => 'GENDER',
        'phone' => 'phone',
        'program' => 'PROGRAM_OF_INTEREST',
        'how_did_you_hear' => 'HOW_DID_YOU_HEAR_ABOUT_CUD',
        'country' => 'COUNTRY_OF_RESIDENCE',
        'nationality' => 'NATIONALITY',
        'date_of_birth' => 'DATE_OF_BIRTH',
        'utm_source' => 'utm_source',
        'utm_medium' => 'utm_medium',
        'utm_campaign' => 'utm_campaign',
        'utm_term' => 'utm_term',
        'utm_content' =>'utm_content',
        'utm_source_type' => 'SOURCE_TYPE',
        'utm_lead_source' => 'LEAD_SOURCE',
        'utm_marketing_channel' => 'MARTKETING_CHANNEL',
        'utm_communication_channel' => 'INCOMING_INQUIRYCOMMUNICATION_CHANNEL',
        'utm_campaign_name' => 'CAMPAIGN_NAME'
    ];

    protected $access = false;

    protected $active_campaign_api_url = null;

    protected $active_campaign_secret_key = null;

    protected $active_campaign_contact_endpoint = null;

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration()
    {

        return [
            'active_campaign_use_defaults' => TRUE,
            'active_campaign_api_url' => '',
            'active_campaign_secret_key' => '',
            'active_campaign_contact_endpoint' => '',
            'active_campaign_mapping' => [],
            'excluded_data' => [],
            'custom_data' => '',
            'debug' => FALSE,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $config = \Drupal::config('cud.settings.control');

        if (!$config->get('active_campaign_active')) {

            \Drupal::logger('ActiveCampaignWebToApiPostWebformHandler')->notice("CUD config settings MUST be checked to allow submission to Active Campaign CRM.");

            return;
        }

        $webform = $this->getWebform();

        $map_sources = [];

        $elements = $this->webform->getElementsDecoded();

        foreach ($elements as $key => $element) {

            if (strpos($key, '#') === 0 || empty($element['#title'])) {
                if (in_array($element["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container'))) {
                    // proceed to adding children
                } else
                    continue;
            }

            $map_sources[$key] = in_array($element["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container')) ? $key : $element['#title'];

            // 2nd depth // get fieldset children
            if (in_array($element["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container'))) {
                $sub_elements = $element;

                foreach ($sub_elements as $k_2 => $e_2) {

                    if (strpos($k_2, '#') === 0 || empty($e_2['#title'])) {
                        if (in_array($e_2["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container'))) {
                            // proceed to adding children
                        } else
                            continue;
                    }

                    $map_sources[$k_2] = in_array($e_2["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container')) ?  '&nbsp;&nbsp;&nbsp;' . $k_2 : '&nbsp;&nbsp;&nbsp;&#8627;' . $e_2['#title'];

                    // 3rd depth // get fieldset children
                    if (in_array($e_2["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container'))) {
                        $sub_e = $e_2;

                        foreach ($sub_e as $k_3 => $e_3) {


                            if (strpos($k_3, '#') === 0 || empty($e_3['#title'])) {
                                if (in_array($e_3["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container'))) {
                                    // proceed to adding children
                                } else
                                    continue;
                            }

                            $map_sources[$k_3] = in_array($e_3["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container')) ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $k_3 : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8627;' . $e_3['#title'];

                            // 4th depth // get fieldset children
                            if (in_array($e_3["#type"], array('webform_wizard_page', 'fieldset', 'webform_flexbox', 'container'))) {
                                $sub_e_3 = $e_3;

                                foreach ($sub_e_3 as $k_4 => $e_4) {

                                    if (strpos($k_4, '#') === 0 || empty($e_4['#title'])) {
                                        continue;
                                    }

                                    $map_sources[$k_4] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8627;' . $e_4['#title'];

                                }
                            }
                        }
                    }
                }
            }
        }


        /** @var \Drupal\webform\WebformSubmissionStorageInterface $submission_storage */
        $submission_storage = \Drupal::entityTypeManager()->getStorage('webform_submission');
        $field_definitions = $submission_storage->getFieldDefinitions();
        $field_definitions = $submission_storage->checkFieldDefinitionAccess($webform, $field_definitions);

        foreach ($field_definitions as $key => $field_definition) {
            $map_sources[$key] = $field_definition['title'] . ' (type : ' . $field_definition['type'] . ')';
        }

        $form['active_campaign_use_defaults'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Use system active defaults'),
            '#description' => $this->t('If checked, global settings for active will be used.'),
            '#return_value' => TRUE,
            '#default_value' => $this->configuration['active_campaign_use_defaults'],
        ];

        $form['active_campaign'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Custom'),
            '#states' => array(
                'visible' => array(
                    ':input[name="settings[active_campaign_use_defaults]"]' => array('checked' => FALSE),
                ),
            ),
        ];

        $form['active_campaign']['active_campaign_api_url'] = [
            '#type' => 'url',
            '#title' => t('Api Url'),
            '#description' => t('<span>See</span> <i>cud.settings.control main configuration</i>'),
            '#states' => array(
                'required' => array(
                    ':input[name="settings[active_use_defaults]"]' => array('checked' => FALSE),
                ),
            ),
            '#default_value' => !empty($this->configuration['active_campaign_api_url']) ? $this->configuration['active_campaign_api_url'] : ''
        ];



        $form['active_campaign']['active_campaign_secret_key'] = [
            '#type' => 'textfield',
            '#title' => t('Key'),
            '#description' => t('<span>See</span> <i>cud.settings.control main configuration</i>'),
            '#states' => array(
                'required' => array(
                    ':input[name="settings[active_use_defaults]"]' => array('checked' => FALSE),
                ),
            ),
            '#default_value' => !empty($this->configuration['active_campaign_secret_key']) ? $this->configuration['active_campaign_secret_key'] : ''
        ];

        $form['active_campaign']['active_campaign_contact_endpoint'] = [
            '#type' => 'textfield',
            '#title' => t('Contact Endpoint'),
            '#description' => t('<span>See</span> <i>cud.settings.control main configuration</i>'),
            '#states' => array(
                'required' => array(
                    ':input[name="settings[active_use_defaults]"]' => array('checked' => FALSE),
                ),
            ),
            '#default_value' => !empty($this->configuration['active_campaign_contact_endpoint']) ? $this->configuration['active_campaign_contact_endpoint'] : ''
        ];

        ksort($this->activeCampaignFields);

        $form['active_campaign_mapping'] = [
            '#type' => 'webform_mapping',
            '#title' => $this->t('Webform to active mapping'),
            '#description' => $this->t('Only Maps with specified "active Web-to-Contact Campaign Field" will be submitted to active.'),
            '#source__title' => t('Webform Submitted Data'),
            '#destination__title' => t('active Web-to-Contact Campaign Field'),
            '#source' => $map_sources,
            '#destination__type' => 'webform_select_other',
            '#destination' =>$this->activeCampaignFields,
            '#default_value' => $this->configuration['active_campaign_mapping'],
        ];

        $form['custom_data'] = [
            '#type' => 'details',
            '#title' => $this->t('Custom data'),
            '#description' => $this->t('Custom data will take precedence over submission data. You may use tokens.'),
        ];

        $form['custom_data']['custom_data'] = [
            '#type' => 'webform_codemirror',
            '#mode' => 'yaml',
            '#title' => $this->t('Insert data'),
            '#description' => $this->t("Enter custom data that will be included when a webform submission is saved."),
            '#parents' => ['settings', 'custom_data'],
            '#default_value' => $this->configuration['custom_data'],
        ];

        $form['custom_data']['token_tree_link'] = $this->tokenManager->buildTreeLink();

        $form['debug'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Enable debugging'),
            '#description' => $this->t('If checked, posted submissions will be displayed onscreen to all users.'),
            '#return_value' => TRUE,
            '#default_value' => $this->configuration['debug'],
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        $config = \Drupal::config('cud.settings.control');

        if ($form_state->getValue('active_campaign_use_defaults') == TRUE) {
            $this->configuration['active_campaign_use_defaults'] = true;
            $this->configuration['active_campaign_api_url'] = $config->get('active_campaign_api_url');
            $this->configuration['active_campaign_secret_key'] = $config->get('active_campaign_secret_key');
            $this->configuration['active_campaign_contact_endpoint'] = $config->get('active_campaign_contact_endpoint');
        } else {
            $this->configuration['active_campaign_use_defaults'] = false;
            $this->configuration['active_campaign_api_url'] = $form_state->getValue('active_campaign')['active_campaign_api_url'];
            $this->configuration['active_campaign_secret_key'] = $form_state->getValue('active_campaign')['active_campaign_secret_key'];
            $this->configuration['active_campaign_contact_endpoint'] = $form_state->getValue('active_campaign')['active_campaign_contact_endpoint'];
        }

        return $form;
    }

    /**
     * remoteFinalMapWebToApi
     * Map the data for active api posting
     *
     * @param array $request_post_data
     * @return array
     */
    protected function remoteFinalMapWebToApi(array $request_post_data)
    {
        \Drupal::logger('ActiveCampaignWebToApiPostWebformHandler')->notice("request posted data - RAW: " . serialize($request_post_data));

        // utms
        $utm_source = \Drupal::request()->get('utm_source');

        if (!empty($utm_source)) {
            $request_post_data['utm_source'] =  $utm_source;
        }

        $utm_medium = \Drupal::request()->get('utm_medium');

        if (!empty($utm_medium)) {
            $request_post_data['utm_medium'] =  $utm_medium;
        }

        $utm_campaign = \Drupal::request()->get('utm_campaign');

        if (!empty($utm_campaign)) {
            $request_post_data['utm_campaign'] =  $utm_campaign;
        }

        $utm_term = \Drupal::request()->get('utm_term');

        if (!empty($utm_term)) {
            $request_post_data['utm_term'] =  $utm_term;
        }

        $utm_content = \Drupal::request()->get('utm_content');

        if (!empty($utm_content)) {
            $request_post_data['utm_content'] =  $utm_content;
        }

        $utm_source_type = \Drupal::request()->get('utm_source_type');

        if (!empty($utm_source_type)) {
            $request_post_data['utm_source_type'] =  $utm_source_type;
        }

        $utm_lead_source = \Drupal::request()->get('utm_lead_source');

        if (!empty($utm_lead_source)) {
            $request_post_data['utm_lead_source'] =  $utm_lead_source;
        }

        $utm_marketing_channel = \Drupal::request()->get('utm_marketing_channel');

        if (!empty($utm_marketing_channel)) {
            $request_post_data['utm_marketing_channel'] =  $utm_marketing_channel;
        }

        $utm_communication_channel = \Drupal::request()->get('utm_communication_channel');

        if (!empty($utm_communication_channel)) {
            $request_post_data['utm_communication_channel'] =  $utm_communication_channel;
        }

        $utm_campaign_name = \Drupal::request()->get('utm_campaign_name');

        if (!empty($utm_campaign_name)) {
            $request_post_data['utm_campaign_name'] =  $utm_campaign_name;
        }

        // map active campaign proper fields with the submitted webform keys -> values
        foreach ($this->activeCampaignFields as $k => $v) {
            if (array_key_exists($k, $request_post_data)) {
                // add the campaign field map name and assign the current value of webform
                $request_post_data[$v] = $request_post_data[$k];

                // remove only if the key is not equal to the value
                if ($k !== $v)
                    unset($request_post_data[$k]);
            } else {
                unset($request_post_data[$k]);
            }
        }

        return $request_post_data;
    }

    /**
     * remoteGetContact
     * Get the active contact
     *
     * @param null $email
     *  Email field value of Lead during submission process
     *
     * @return bool|mixed|string
     *  Lead object or false
     */
    public function remoteGetContact($email = null)
    {

        if ( !$this->remoteAuthorize() ) {
            return false;
        }

        // clean up the url
        $url = rtrim($this->active_campaign_api_url, '/ ');

        // define a final API request - GET
        $api = $url . $this->active_campaign_contact_endpoint;

        $params = array(
            'email'   => $email,
        );

        $query = "";
        foreach ($params as $key => $value) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        // define a final API request - GET
        $api .= "?" . $query;

        $result = \Drupal::httpClient()->get($api, [
            'headers' => [
                'Api-Token' => $this->active_campaign_secret_key
            ],
        ])->getBody()->getContents();

        if ( count(json_decode($result)->contacts) > 0 ) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * remotePost
     * Execute a remote post.
     *
     * @param string $operation
     *   The type of webform submission operation to be posted. Can be 'insert',
     *   'update', or 'delete'.
     *
     * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
     *   The webform submission to be posted.
     *
     * @return array|bool
     */
    protected function remotePost($operation, WebformSubmissionInterface $webform_submission)
    {

       if (!$this->remoteAuthorize()) {
            return false;
        }

        // get webform submissions
        $request_post_data = $this->getPostData($operation, $webform_submission);

        // re-align with api fields
        $request_post_data = $this->remoteFinalMapWebToApi($request_post_data);

        // semicolon sv array values
        // active dislikes array/json
        // on passing thru api
        foreach ($request_post_data as $k => $v) {

            if (is_array($v)) {
                $request_post_data[$k] = implode(';', $v);
            }
        }

        \Drupal::logger('ActiveCampaignWebToApiPostWebformHandler')->notice("request posted data: " . serialize($request_post_data));

        // get contact
        if (array_key_exists('email', $request_post_data)) {

            $this->remoteGetContact($request_post_data['email']);


            // clean up the url
            $url = rtrim($this->active_campaign_api_url, '/ ');

            // define a final API request - GET
            $api = $url . "/api/3/fields?limit=150";

            $result = \Drupal::httpClient()->get($api, [
                'headers' => [
                    'Api-Token' => $this->active_campaign_secret_key
                ],
            ])->getBody()->getContents();

            $field_values = array();

            if ( count(json_decode($result)->fields) > 0 ) {
                foreach ( json_decode($result)->fields as $key => $value ) {

                    if (array_key_exists($value->perstag, $request_post_data)) {
                        array_push( $field_values,
                            array(
                                'field' => $value->id,
                                'value' => $request_post_data[$value->perstag]
                            )
                        );
                    }

                }
            }

            if ( count($field_values) >0 ) {
                $request_post_data['fieldValues'] = $field_values;
            }

            $form_params = json_encode(
                array(
                    'contact' => $request_post_data
                )
            );

            // post to contact
            $api = $url . "/api/3/contact/sync";

            $result = \Drupal::httpClient()->post($api, [
                'body' => $form_params,
                'headers' => [
                    'Api-Token' => $this->active_campaign_secret_key
                ],
            ])->getBody()->getContents();

        }


        \Drupal::logger('activeWebToApiPostWebformHandler')->notice("final request post data: " . serialize($request_post_data));


        return true;
    }

    /**
     * getPostData
     * Get a webform submission's post data.
     *
     * @param string $operation
     *   The type of webform submission operation to be posted. Can be 'insert',
     *   'update', or 'delete'.
     * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
     *   The webform submission to be posted.
     *
     * @return array
     *   A webform submission converted to an associative array.
     */
    protected function getPostData($operation, WebformSubmissionInterface $webform_submission)
    {

        $data = $webform_submission->toArray(TRUE);
        $data = $data['data'] + $data;
        unset($data['data']);
        // Get data from parent.
        // Well the idea is that only mapped data and custom data are passed to active.
        // Also curation is best handled only at active_mapping.
        // Unable to make use of parent getPostData logic.
        // $data = parent::getPostData($operation, $webform_submission); .
        // Get active field mappings.

        $active_mapping = $this->configuration['active_campaign_mapping'];

        // 07.07.2017
        // modified from original ++g++
        // using mapping to populate data instead of the submitted webform keys
        foreach ($active_mapping as $key => $value) {
            if (!empty($value) && !empty($data[$key])) {
                $active_data[$value] = $data[$key];
            }
        }

        // Append custom data.
        if (!empty($this->configuration['custom_data'])) {
            $active_data = Yaml::decode($this->configuration['custom_data']) + $active_data;
        }

        // Append operation data.
        if (!empty($this->configuration[$operation . '_custom_data'])) {
            $active_data = Yaml::decode($this->configuration[$operation . '_custom_data']) + $active_data;
        }

        // Replace tokens.
        $active_data = $this->tokenManager->replace($active_data, $webform_submission);

        // Allow modification of data by other modules.
        \Drupal::moduleHandler()->alter('webform_cud_posted_data', $active_data, $this->webform, $webform_submission);
        return $active_data;
    }


    // helpers

    /**
     * remoteAuthorize
     * Get active authorization to be able the
     * Api connectivity
     *
     * @return bool|mixed|void
     */
    protected function remoteAuthorize()
    {

        $config = \Drupal::config('cud.settings.control');

        $active_campaign_api_url = $config->get('active_campaign_api_url');
        $active_campaign_secret_key = $config->get('active_campaign_secret_key');
        $active_campaign_contact_endpoint = $config->get('active_campaign_contact_endpoint');

        if (empty($active_campaign_api_url)) {
            return false;
        }

        // clean up the url
        $url = rtrim($active_campaign_api_url, '/ ');

        // define a final API request - GET
        $api = $url . '/api/3/accounts';

        $result = \Drupal::httpClient()->get($api, [
            'headers' => [
                'Api-Token' => $active_campaign_secret_key
            ],
        ])->getBody()->getContents();


        if (count(json_decode($result)) > 0) {

            $this->active_campaign_api_url = $active_campaign_api_url;

            $this->active_campaign_secret_key = $active_campaign_secret_key;

            $this->active_campaign_contact_endpoint = $active_campaign_contact_endpoint;

            $this->access = $result;

            return true;
        }

        return false;
    }

    /**
     * getCurlWithHeaderPostfields
     * Custom Curl Posting with tweaks for properly submitting
     * to active endponts
     *
     *
     * @param $url
     *
     * @param int $post_type
     *
     * @param null $http_header
     *
     * @param null $post_fields
     *
     * @param null $operation
     *
     * @return bool|mixed|string
     */
    function getCurlWithHeaderPostfields($url, $post_type = 0, $http_header = null, $post_fields = null)
    {
        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            /* get 0, post 1 */
            curl_setopt($ch, CURLOPT_POST, $post_type);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            if ($http_header != null) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
            }

            if ($post_fields != null) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
            }

            $result = (string)curl_exec($ch);

            curl_close($ch);

            if ($result) {
                return unserialize($result);
            } else {
                \Drupal::logger("ActiveCampaignWebToApiPostWebformHandler")->notice("Nothing was returned. Do you have a connection to Email Marketing server?");
                return false;
                // die('Nothing was returned. Do you have a connection to Email Marketing server?');
            }

        } else {
            return file_get_contents($url);
        }
    }

    protected function getFileUri($fid = null)
    {

        if ($fid == null) {
            return null;
        }

        $service = \Drupal::service('entity.manager')->getStorage('file');
        $file = $service->load($fid);

        if ($file) {
            return file_create_url($file->getFileUri());
        }

        return null;
    }
}
