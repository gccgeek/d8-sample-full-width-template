<?php

namespace Drupal\webform_cud\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Core\Url;
use Drupal\webform_cud\lib\RijndaelOpenSSL;

/**
 * Payment Generic HOP Migs (For Arabic)
 *
 * @WebformHandler(
 *   id = "cud_payment_migs_generic_arabic",
 *   label = @Translation("CUD Migs Payment Generic (For Arabic) / Direct integration"),
 *   category = @Translation("CUD Payment"),
 *   description = @Translation("Generic payment gateway direct payment for MIGS."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class CUDWebformHandlerMigsGenericArabic extends WebformHandlerBase
{
     protected $custom_local_use_encryption_key = "117781CUD117781";

    /**
     * {@inheritdoc}
     */
    public function alterElements(array &$elements, WebformInterface $webform)
    {}

    public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {}

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {}

    /**
     * {@inheritdoc}
     */
    public function postSave(WebformSubmissionInterface $webform_submission, $update = true)
    {}

      /**
     * {@inheritdoc}
     */
    public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {
        // $webform = $this->getWebform();
        // $wid = $webform->id();

        $language = \Drupal::service('language_manager')->getCurrentLanguage();
        
        $url = Url::fromUri("internal:/payment/form/arabic", ['language' => $language]);
        
        if ($webform_submission->id()) {

            $rijndael =  new RijndaelOpenSSL();

            $str_id = (string)$webform_submission->id();

            $hash = $rijndael->encrypt($str_id, $this->custom_local_use_encryption_key);

            $encoded_hash = urlencode($hash);

            $encoded_hash = str_replace(array('%'), array('PPPPP'), urlencode($hash));

            // go to payment
            // with language
            $url = Url::fromRoute("webform_cud.generic_payment_gateway_redirect_migs", ['str_id' => $encoded_hash], ['language' => $language]);
        }

        $response = new LocalRedirectResponse($url->toString());
            
        $response->send(); 
        
    }
}
