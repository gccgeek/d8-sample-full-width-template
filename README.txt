WHAT IS THIS CUSTOM
-------------------

This is a custom drupal installation showcasing ONLY the basic custom layout with full responsiveness features using bootstrap.
This is an template staging from the original main branch.


ABOUT DRUPAL
------------

Drupal is an open source content management platform supporting a variety of
websites ranging from personal weblogs to large community-driven websites. For
more information, see the Drupal website at https://www.drupal.org, and join
the Drupal community at https://www.drupal.org/community.

Legal information about Drupal:
 * Know your rights when using Drupal:
   See LICENSE.txt in the "core" directory.
 * Learn about the Drupal trademark and logo policy:
   https://www.drupal.com/trademark


